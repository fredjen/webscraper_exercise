from woolcrawler_src.dbio.db_model import get_db


class TestDBIO:
    def __init__(self, storage, storage_path=None, autocommit=True, pool_size=0, migrate=True, fake_migrate=False,
                 check_reserved=('sqlite',)):
        self._storage = storage
        self._storage_path = storage_path
        self._autocommit = autocommit
        self._db = get_db(self._storage,
                          folder=self._storage_path,
                          pool_size=pool_size,
                          migrate=migrate,
                          fake_migrate=fake_migrate,
                          check_reserved=check_reserved,
                          )

    # ------------------- #
    # commit and rollback #
    # ------------------- #

    def commit(self):
        self._db.commit()

    def rollback(self):
        self._db.rollback()

    # ---------------- #
    # close connection #
    # ---------------- #

    def close(self):
        self._db.close()

    # ------------ #
    # test actions #
    # ------------ #

    def insert_wool_case(self, brand, name, website):
        db = self._db
        wool_case_id = db.wool_case.insert(
            brand=brand,
            name=name,
            website=website,
        )
        db.commit()
        return wool_case_id

    def get_wool_data_points(self):
        db = self._db
        return db(db.wool_data_point).select()

    def get_wool_data_points_with_wool_case_data(self):
        db = self._db
        rows = db(db.wool_data_point.wool_case_id == db.wool_case.id).select()
        results = list()
        for row in rows:
            result = dict(
                brand=row.wool_case.brand,
                name=row.wool_case.name,
                website=row.wool_case.website,
                crawl_date_id=row.wool_data_point.crawl_date_id,
                availability=row.wool_data_point.availability,
                price=row.wool_data_point.price,
                needle_strength=row.wool_data_point.needle_strength,
                composition=row.wool_data_point.composition,
                scraped_on=row.wool_data_point.scraped_on,
            )
            results.append(result)
        return results
