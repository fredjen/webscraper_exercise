import datetime
import pytest
from decimal import Decimal
from woolcrawler_tests.util_dbio import TestDBIO
from woolcrawler_src.dbio import DBIO
from woolcrawler_src.business_core import WoolCase
from woolcrawler_src.utils import Queue


@pytest.fixture
def setup_db(tmp_path):
    test_dbio = TestDBIO(
        'sqlite://storage.sqlite',
        storage_path=tmp_path,
    )
    return test_dbio, tmp_path


@pytest.fixture
def insert_dummy_data(setup_db):
    test_dbio, tmp_path = setup_db
    dummy_wool_cases = [
        dict(brand='Ramba1', name='Zamba XL 1', website='www.wollplatz.de'),
        dict(brand='Ramba2', name='Zamba XL 2', website='www.wollplatz.de'),
        dict(brand='Ramba3', name='Zamba XL 3', website='www.wollplatz.de'),
        dict(brand='Ramba4', name='Zamba XL 4', website='www.wollplatz.de'),
        dict(brand='Ramba5', name='Zamba XL 5', website='www.wollplatz.de'),
    ]
    for wool_case in dummy_wool_cases:
        test_dbio.insert_wool_case(**wool_case)
    return dummy_wool_cases, tmp_path, test_dbio


@pytest.fixture
def get_dbio(insert_dummy_data):
    dummy_wool_cases, tmp_path, test_dbio = insert_dummy_data
    dbio = DBIO('sqlite://storage.sqlite', tmp_path)
    return dbio, dummy_wool_cases, test_dbio


def test_check_if_exists(get_dbio):
    dbio, *_ = get_dbio
    today = datetime.date.today()
    assert not dbio._check_if_exists(today)


def test_get_crawl_date_id(get_dbio):
    dbio, *_ = get_dbio
    today = datetime.date.today()
    crawl_date_id = dbio.get_crawl_date_id(today)
    assert dbio._check_if_exists(today)
    assert crawl_date_id == 1  # no other date sould habe been inserted at this time


def test_get_wool_cases(get_dbio):
    dbio, dummy_wool_cases, *_ = get_dbio
    wool_cases_queue = dbio.get_wool_cases(limit=(2, 4))  # note limit! if it works with limit, it works without
    assert isinstance(wool_cases_queue, Queue)
    woolcase = wool_cases_queue.dequeue()
    assert isinstance(woolcase, WoolCase)
    first_woolcase = dummy_wool_cases[2]  # should be first one fetched
    assert (woolcase.brand, woolcase.name, woolcase.website) == \
           (first_woolcase['brand'], first_woolcase['name'], first_woolcase['website'])
    last_woolcase = dummy_wool_cases[3]  # should be last one fetched
    woolcase = wool_cases_queue.dequeue()
    assert (woolcase.brand, woolcase.name, woolcase.website) == \
           (last_woolcase['brand'], last_woolcase['name'], last_woolcase['website'])
    assert wool_cases_queue.dequeue() is None


def test_bulk_insert_wool_data_points(get_dbio):
    dbio, _, test_dbio = get_dbio
    today = datetime.date.today()
    scraped_on = datetime.datetime.now()
    crawl_date_id = dbio.get_crawl_date_id(today)
    all_wool_cases = dbio.get_wool_cases()
    new_all_wool_cases = Queue()
    i = 1
    prices = list()
    while woolcase := all_wool_cases.dequeue():
        woolcase.crawl_date = crawl_date_id
        woolcase.scraped_on = scraped_on
        woolcase.price = Decimal(f'{str(i)}.{str(i)}{str(i)}')
        prices.append(woolcase.price)
        new_all_wool_cases.enqueue(woolcase)
        i += 1
    dbio.bulk_insert_wool_data_points(new_all_wool_cases)
    all_data_points = test_dbio.get_wool_data_points()
    for data_point_row, price in zip(all_data_points, prices):
        assert isinstance(price, Decimal)
        assert isinstance(data_point_row.price, Decimal)
        assert data_point_row.price == price
