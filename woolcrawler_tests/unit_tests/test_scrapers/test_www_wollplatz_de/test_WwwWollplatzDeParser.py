from decimal import Decimal
import pytest
from .dummy_website import DUMMY_WEBSITE
from woolcrawler_src.scraper.website_scrapers.www_wollplatz_de import WwwWollplatzDeParser


@pytest.fixture
def get_parser():
    parser = WwwWollplatzDeParser(DUMMY_WEBSITE)
    return parser


def test_parse(get_parser):
    parser = get_parser
    parser.parse()
    assert parser.results['availability'] == 'Lieferbar'
    assert parser.results['price'] == Decimal('3.10')
    assert parser.results['needle_strength'] == '4 mm'
    assert parser.results['composition'] == '100% Acryl'
