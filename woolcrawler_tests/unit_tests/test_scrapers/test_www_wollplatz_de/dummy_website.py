DUMMY_WEBSITE = """
<!doctype html>
<!--#####################################################################-->
<!--#   Deze webshop draait op het Next PRO platform van Logic4.        #-->
<!--#   Meer informatie? www.logic4.nl/oplossingen/webshop-software/    #-->
<!--#   All rights reserved.                                            #-->
<!--#####################################################################-->


<!--[if lt IE 7 ]> <html class="ie ie6" lang="nl"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7" lang="nl"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8" lang="nl"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9" lang="nl"> <![endif]-->
<!--[if gt IE 9]><!-->
<html id="MasterHtml" itemscope="" itemtype="http://schema.org/WebPage" lang="de">
<!--<![endif]-->

<head prefix="og: http://ogp.me/ns#"><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta id="viewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /><meta name="format-detection" content="telephone=no" /><meta name="HandheldFriendly" content="true" /><meta name="MobileOptimized" content="320" />
    <!--[if IEMobile]><meta http-equiv="cleartype" content="on" /><![endif]-->

    <link rel="preload" as="image" href="https://www.wollplatz.de/resize/bwstspecialdk_1001_1_16313763206784.jpg/500/500/True/stylecraft-special-dk-1001-white.jpg"><meta itemprop='name' content='Stylecraft Special dk 1001 White | Wollplatz.de' />
<meta property='og:title' content='Stylecraft Special dk 1001 White | Wollplatz.de' />
<meta itemprop='description' content='Stylecraft Special dk 1001 White - ein anschmiegsames Garn in vielen wunderschönen Farben. Stylecraft und andere Markengarne bestellen Sie einfach und schnell online bei Wollplatz.de' />
<meta property='og:description' content='Stylecraft Special dk 1001 White - ein anschmiegsames Garn in vielen wunderschönen Farben. Stylecraft und andere Markengarne bestellen Sie einfach und schnell online bei Wollplatz.de' />
<link rel='canonical' href='https://www.wollplatz.de/artikel/18099/stylecraft-special-dk-1001-white.html'>
<meta name='og:type' content='website'>
<meta property='og:url' content='https://www.wollplatz.de/wolle/stylecraft/stylecraft-special-dk' />
<meta itemprop='url' content='https://www.wollplatz.de/wolle/stylecraft/stylecraft-special-dk' />
<meta property='og:image' content='https://www.wollplatz.de/resize/bwstspecialdk_1001_1_16313763206784.jpg/300/200/True/stylecraft-special-dk-1001-white.jpg' />
<meta itemprop='image' content='https://www.wollplatz.de/resize/bwstspecialdk_1001_1_16313763206784.jpg/300/200/True/stylecraft-special-dk-1001-white.jpg' />
<link rel='alternate' href='https://www.wollplatz.de/artikel/18099/stylecraft-special-dk-1001-white.html' hreflang='de'/>


    <!-- Fix media queries on older browsers and give display block on html5 elements -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>  
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if gte IE 9]>
        <style type="text/css">.gemengd,.goud,.metaal,.zilver {filter: none;}</style>
    <![endif]-->

    <link rel='apple-touch-icon' sizes='180x180' href='/styles/clients/wolplein-wollplatz/img/favicon/apple-touch-icon.png'>
<link rel='icon' type='image/png' href='/styles/clients/wolplein-wollplatz/img/favicon/favicon-32x32.png' sizes='32x32'>
<link rel='icon' type='image/png' href='/styles/clients/wolplein-wollplatz/img/favicon/favicon-16x16.png' sizes='16x16'>
<link rel='manifest' href='/styles/clients/wolplein-wollplatz/img/favicon/site.webmanifest'>
<link rel='mask-icon' href='/styles/clients/wolplein-wollplatz/img/favicon/safari-pinned-tab.svg'>
<link rel='shortcut icon' href='/styles/clients/wolplein-wollplatz/img/favicon/favicon.ico'>
<meta name='msapplication-config' content='/styles/clients/wolplein-wollplatz/img/favicon/browserconfig.xml'>
<meta name='theme-color' content='#333333'>
<link href='/grid/css/z-bundle.1.3.min.css?v=190923132912' rel='stylesheet'/><link href='/styles/clients/wolplein-wollplatz/less/main.css?v=190923132912' rel='stylesheet'/>
    <script type="text/javascript">
        //document.onreadystatechange = function () {var bum = document.getElementsByClassName('bumbli-wrap');if (bum != null) {for (var i = 0; i < bum.length; i++) {bum[i].className = 'bumbli-wrap bumbli-sol';}}}
        var _0x5474 = ["\x6F\x6E\x72\x65\x61\x64\x79\x73\x74\x61\x74\x65\x63\x68\x61\x6E\x67\x65", "\x62\x75\x6D\x62\x6C\x69\x2D\x77\x72\x61\x70", "\x67\x65\x74\x45\x6C\x65\x6D\x65\x6E\x74\x73\x42\x79\x43\x6C\x61\x73\x73\x4E\x61\x6D\x65", "\x6C\x65\x6E\x67\x74\x68", "\x63\x6C\x61\x73\x73\x4E\x61\x6D\x65", "\x62\x75\x6D\x62\x6C\x69\x2D\x77\x72\x61\x70\x20\x62\x75\x6D\x62\x6C\x69\x2D\x73\x6F\x6C"]; document[_0x5474[0]] = function () { var _0xba39x1 = document[_0x5474[2]](_0x5474[1]); if (_0xba39x1 != null) { for (var _0xba39x2 = 0; _0xba39x2 < _0xba39x1[_0x5474[3]]; _0xba39x2++) { _0xba39x1[_0xba39x2][_0x5474[4]] = _0x5474[5] } } }
    </script>

    <script>dataLayer = window.dataLayer = window.dataLayer || [];</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j = d.createElement(s),dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src ='https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f); })
(window,document,'script','dataLayer','GTM-KVXHBMV');
dataLayer.push({ 'event': 'detail' ,'ecommerce': {'detail':
{"products":[{"id":"bwstspecialdk_1","name":"Stylecraft Special dk 1001 White","price":"2.60","brand":"Stylecraft","category":"","quantity":1}]}
}});

</script>
<!-- End Google Tag Manager -->
<title>
	Stylecraft Special dk 1001 White | Wollplatz.de
</title><meta name="description" content="Stylecraft Special dk 1001 White - ein anschmiegsames Garn in vielen wunderschönen Farben. Stylecraft und andere Markengarne bestellen Sie einfach und schnell online bei Wollplatz.de" /></head>
<body id="MasterBody" class="preload de page-product cat-wolle-garne cat-herstellers cat-stylecraft product-stylecraft-special-dk-1001-white user-is-not-logged-in">
    
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KVXHBMV" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    
    
    
    <div class="shopholder">

        <!-- REGEL -1-->
        <div class='wrapper100 wrapper100-regel--1'>
<div class='blockcontainer wrapper-content wrapper-regel--1'>
<div id="websiteblok594" class="box pcw100 pcs0020 tabw100 tabs0020 mobw100 mobs0020 pagecfg key-next-regel--1">
<div id="websitecontentblok671" class="box pcw100 pcs0020 tabw100 tabs0020 mobw100 mobs0020 actiebalk element">
<div id="cmscontentblok308" class="box pcw100 pcs0010 tabw100 tabs0010 mobw100 mobs0010 cmscfg">

<div id="actie-bar-1" class="actie-bar-inner">
<p><a href="/wolle/drops?ft5=SALE">30% Rabatt auf ausgewählte Drops Wollgarne!</a></p></div>
<div id="actie-bar-2" class="actie-bar-inner">
<p><a href="/sale/sonderangebot">Jetzt 15% Rabatt auf ausgewählte Yarn and Colors Baumwollgarne + Pakete!</a></p></div>
<div id="actie-bar-3" class="actie-bar-inner">
<p><a href="/pakete/wollplatz-sommerpaket-2023">Bestellen Sie jetzt Ihre Wollplatz Sommerpaket 2023!</a></p></div>
<div id="actie-bar-4" class="actie-bar-inner">
<p><a href="/wolle/stylecraft/stylecraft-special-dk">10% Rabatt auf Stylecraft Special DK!</a></p></div>
<div id="actie-bar-5" class="actie-bar-inner">
<p><a href="/">KOSTENLOSES Faltbare Schere wenn Sie für €25,- oder mehr einkaufen (exkl. Versandkosten)! Vergessen Sie nicht, das Geschenk in Ihren Einkaufswagen zu legen.</a></p></div>
<div id="actie-bar-6" class="actie-bar-inner">
<p><a href="/pakete?ft10=Tassen&limit=filterft10">Jetzt 10% Rabatt auf alle Taschenpakete!</a></p></div>
<div id="actie-bar-7" class="actie-bar-inner">
<p><a href="/wolle/drops?ft5=SALE">30% Rabatt auf ausgewählte Drops Baumwollgarne!</a></p></div>
<script>
    var actions = [];

    // Let op!
    // Maand telt vanaf de 0 => Januari = 0, December = 11

    actions.push({
        id: "actie-bar-1",
        startDate: new Date(2023, 8, 12), // Start datum actie
        endDate: new Date(2023, 8, 30), // De dag NA de actie
    })

    actions.push({
        id: "actie-bar-2",
        startDate: new Date(2023, 6, 31), // Start datum actie
        endDate: new Date(2023, 7, 4), // De dag NA de actie
    })

    actions.push({
        id: "actie-bar-3",
        startDate: new Date(2023, 5, 23), // Start datum actie
        endDate: new Date(2023, 5, 26), // De dag NA de actie
    })

    actions.push({
        id: "actie-bar-4",
        startDate: new Date(2023, 6, 14), // Start datum actie
        endDate: new Date(2023, 6, 17), // De dag NA de actie
    })

    actions.push({
        id: "actie-bar-5",
        startDate: new Date(2023, 6, 21), // Start datum actie
        endDate: new Date(2023, 6, 24), // De dag NA de actie
    })

    actions.push({
        id: "actie-bar-6",
        startDate: new Date(2023, 6, 3), // Start datum actie
        endDate: new Date(2023, 6, 7), // De dag NA de actie
    })

    actions.push({
        id: "actie-bar-7",
        startDate: new Date(2023, 6, 17), // Start datum actie
        endDate: new Date(2023, 6, 18), // De dag NA de actie
    })


    for(var i=0,count=actions.length;i<count;i++)setAction(actions[i].id,actions[i].startDate,actions[i].endDate);function setAction(t,i,n){var a=new Date,e=document.getElementById(t);i<=a&&a<=n&&e.classList.add("active")}
</script>

<style>.actie-bar-inner{display:none}.actie-bar-inner.active{display:block}.actie-bar-inner a{display:block;text-decoration:none}</style>
</div>
</div>
</div>
</div>
</div>


        
            <div class="wrapper100 wrapper100-topbar">
                <div class="wrapper-content wrapper-topbar">
                    <div class="btn topbar-button menubutton"><i class="fa fa-bars"></i><span class="btn-txt">Menü </span></div>

                    <form name='search'
class='search'>
<input 
required='required' 
maxlength='150' 
id='searchSooqrTop' 
type='text' 
class='input-txt' 
autocomplete='off' 
placeholder='Suchen nach...'/>
<button data-searchpage='suche.html' aria-label='Suchen nach...' class='button topbar-searchbutton searchbutton'><i class='fa fa-search'></i></button>
</form>


                    <div class="topbar-logo">
                        <a href='/' class='shoplogo' title='Wollplatz'>
<img src='/styles/clients/wolplein-wollplatz/img/moblogo.svg' alt='Wollplatz'/>
</a>

                    </div>

                    <div class="cartuserbutton-holder">
                        <div class="btn topbar-button userbutton userlogedin" id="btnUserMenu" data-reloadcardonclick="true">
                            <i class='fa fa-user'>
</i><span class='btn-txt'>Login</span>

                        </div>
                        
                        <div id="maincartbutton" class="btn topbar-button cartbutton" onclick="LoadShoppingCartSlideMenu(this);"><i class="fa fa-basket"></i><span class="btn-txt">Warenkorb </span></div>
                        <div id="maincartbutton-franco-message" style="display: none;"></div>
                        
                    </div>


                    
                </div>
            </div>
        

        


        <!-- REGEL 0-->
        <div class='wrapper100 wrapper100-regel0'>
<div class='blockcontainer wrapper-content wrapper-regel0'>
<div id="websiteblok44" class="box pcw100 pcs0010 tabw100 tabs0010 mobhide pagecfg key-next-regel0">
<div id="websitecontentblok101" class="box pcw100 pcs0010 tabw100 tabs0010 mobhide menu-bar element">
<nav id="menubar101" class="box pcw100 pcs0010 tabw100 tabs0010 mobhide menubarmenu-bar" ><ul class="menubar-holder2-v1 "><li class='nav-main '><a class='nav-main' href='/'>Home</a></li><li class='nav-main '><a class='nav-main' href='https://www.wollplatz.de/wolle'>Wolle & Garne</a><div class='nav-subholder'><ul class='nav-sub1 '><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/wolle/herstellers'>Hersteller</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/budgetyarn'>Budgetyarn</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/botter-ijsselmuiden'>Botter IJsselmuiden</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/dmc'>DMC</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/drops'>Drops</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/durable'>Durable</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/gutermann'>Gütermann</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/katia'>Katia</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/lana-grossa'>Lana Grossa</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/lang-yarns'>Lang Yarns</a></li></ul></li><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/wolle/herstellers'><p style="opacity: 0;">_</p></a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/lopi'>Lopi</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/phildar'>Phildar</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/pink-label'>Pink Label</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/rico'>Rico</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/scheepjes'>Scheepjes</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/smc-schachenmayr'>SMC / Schachenmayr</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/stylecraft'>Stylecraft</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/trimits'>Trimits</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/yarn-and-colors'>Yarn and Colors</a></li></ul></li><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/wolle/materialien'>Materialien</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/acryl'>Acryl</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/alpaka'>Alpaka</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/bambus'>Bambus</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/baumwolle'>Baumwolle</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/hanf'>Hanf</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/jute'>Jute</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/kamel'>Kamel</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/kaschmir'>Kaschmir</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/lammwolle'>Lammwolle</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/leinen'>Leinen</a></li></ul></li><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/wolle/materialien'><p style="opacity: 0;">_</p></a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/merino'>Merino</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/metallic'>Metallic</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/mohair'>Mohair</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/polyamid'>Polyamid</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/polyester'>Polyester</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/seide'>Seide</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/tencel-lyocell'>Tencel / Lyocell</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/viskose'>Viskose</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/wolle'>Wolle</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/wolle/yak'>Yak</a></li></ul></li></ul></div></li><li class='nav-main '><a class='nav-main' href='https://www.wollplatz.de/nadeln'>Nadeln</a><div class='nav-subholder'><ul class='nav-sub1 '><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/nadeln/kategorien'>Kategorien</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/haekelnadeln'>Häkelnadeln</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/nadelspiele'>Nadelspiele</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/punch-needle'>Punch Needle</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/rundstricknadeln'>Rundstricknadeln</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/stricknadeln'>Stricknadeln</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/weitere-nadeln'>Weitere Nadeln</a></li></ul></li><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/kategorien/1740/herstellers.html'>Hersteller</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/herstellers'>Addi</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/clover'>Clover</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/drops'>Drops</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/knitpro'>Knitpro</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/lana-grossa'>Lana Grossa</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/milward'>Milward</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/prym'>Prym</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/rico'>Rico</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/tulip'>Tulip</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/wollplatz'>Wollplatz</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/nadeln/yarn-and-colors'>Yarn and Colors</a></li></ul></li></ul></div></li><li class='nav-main '><a class='nav-main' href='https://www.wollplatz.de/pakete'>Pakete</a><div class='nav-subholder'><ul class='nav-sub1 '><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/pakete/kategorien'>Kategorien</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/garnpakete'>Garnpakete</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/haekelpakete'>Häkelpakete</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/punch-needle-pakete'>Punch Needle Pakete</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/rabattpakete'>Rabattpakete</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/stickpakete'>Stickpakete</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/strickpakete'>Strickpakete</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/weitere-pakete'>Weitere Pakete</a></li></ul></li><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/pakete/projekte'>Projekte</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/accessoires'>Accessoires</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/baby'>Baby</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/damenkleidung'>Damenkleidung</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/kinderkleidung'>Kinderkleidung</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/kuscheltiere-spielzeug'>Kuscheltiere & Spielzeug</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/ostern'>Ostern</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/weihnachten'>Weihnachten</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/wohnaccessoires'>Wohnaccessoires</a></li></ul></li></ul></div></li><li class='nav-main '><a class='nav-main' href='https://www.wollplatz.de/zubehor'>Zubehör</a><div class='nav-subholder'><ul class='nav-sub1 '><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/zubehor/accessoires'>Accessoires</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/clips-beissringe'>Clips & Beißringe</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/amigurumi/fuellung'>Füllmaterial & Kissen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/garnschalen-halter'>Garnschalen & -halter</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/kleber'>Kleber</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/kumihimo'>Kumihimo</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/lampen'>Lampen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/maschenmarkierer'>Maschenmarkierer</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/maschenraffer'>Maschenraffer</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/papeterie-geschenke'>Papeterie & Geschenke</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/amigurumi/perlen'>Perlen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/ringe'>Ringe</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/rundenzahler'>Rundenzähler</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/spannen-pflege'>Spannen & Pflege</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/amigurumi/spieluhren-klaenge'>Spieluhren & -geräusche</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/spielzeug'>Spielzeug</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/spitzenschoner'>Spitzenschoner</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/stickringe-rahmen'>Stickringe & -rahmen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/strickringe'>Strickringe</a></li></ul></li><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/aufbewahrung'>Aufbewahrung</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/aufbewahrungsboxen'>Aufbewahrungsboxen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/etuis'>Etuis</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/projekttaschen'>Handarbeitstaschen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/hobbytaschen'>Hobbytaschen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/nahkorbe'>Nähkörbe</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/wollspender'>Wollspender</a></li></ul></li><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/zubehor/kurzwaren'>Utensilien</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/leder-labels'>Leder Labels</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/broschen'>Broschen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/gummibander-seile'>Gummibänder & Seile</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/knopfe'>Knöpfe</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/nadeln'>Nadeln</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/naehzubehor'>Nähzubehör</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/pompons-quasten'>Pompons & Quasten</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/scheren-fadenabschneider'>Scheren & Fadenadschneider</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/amigurumi/sicherheitsaugen'>Sicherheitsaugen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/amigurumi/sicherheitsnasen'>Sicherheitsnasen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/stecknadeeln-nadelkissen'>Stecknadeln & Nadelkissen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/stick-nahgarn'>Stick- & Nähgarn</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/stoffe'>Stoffe</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/taschengurte-boden'>Taschengurte & -böden</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/zubehor/verschlusse'>Verschlüsse</a></li></ul></li></ul></div></li><li class='nav-main '><a class='nav-main' href='https://www.wollplatz.de/anleitungen'>Anleitungen</a><div class='nav-subholder'><ul class='nav-sub1 '><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/anleitungen/kategorien'>Kategorien</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/andere-anleitungen'>Andere Anleitungen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/haekelanleitungen'>Häkelanleitungen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/makramee-anleitungen'>Makramee-Anleitungen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/stickanleitungen'>Stickanleitungen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/strickanleitungen'>Strickanleitungen</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/punch-needle-anleitungen'>Punch Needle Anleitungen</a></li></ul></li><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/anleitungen/projekte'>Projekte</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/accesoires'>Accessoires</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/baby'>Baby</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/damenkleidung'>Damenkleidung</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/pakete/herrenkleidung'>Herrenkleidung</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/kinderkleidung'>Kinderkleidung</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/kuscheltiere-spielzeug'>Kuscheltiere & Spielzeug</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/nikolaus'>Nikolaus</a></li><li class='nav-sub2 '></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/ostern'>Ostern</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/weihnachten'>Weihnachten</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/anleitungen/wohnaccessoires'>Wohnaccessoires</a></li></ul></li></ul></div></li><li class='nav-main '><a class='nav-main' href='https://www.wollplatz.de/bucher-zeitschriften'>Bücher & Zeitschriften</a><div class='nav-subholder'><ul class='nav-sub1 '><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/bucher-zeitschriften/kategorien'>Kategorien</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/hobbybucher'>Hobbybücher</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/zeitschriften'>Zeitschriften</a></li></ul></li><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/bucher-zeitschriften/herstellers'>Hersteller</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/dmc'>DMC</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/hoooked'>Hoooked</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/katia'>Katia</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/lana-grossa'>Lana Grossa</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/lang-yarns'>Lang Yarns</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/phildar'>Phildar</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/rico'>Rico</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/scheepjes'>Scheepjes</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/smc-schachenmayr'>SMC / Schachenmayr</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/bucher-zeitschriften/wollplatz'>Wollplatz</a></li></ul></li></ul></div></li><li class='nav-main '><a class='nav-main' href='https://www.wollplatz.de/sale-wollplatz'>Sale</a><div class='nav-subholder'><ul class='nav-sub1 '><li class='nav-sub1 '><a class='nav-sub1' href='https://www.wollplatz.de/sale/kategorien'>Kategorien</a><ul class='nav-sub2 '><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/sale/wolle-garne'>Wolle & Garne</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/sale/nadeln'>Nadeln</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/sale/pakete'>Pakete</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/sale/zubehor'>Zubehör</a></li><li class='nav-sub2 '><a class='nav-sub2' href='https://www.wollplatz.de/sale/buecher-zeitschriften'>Bücher & Zeitschriften</a></li></ul></li></ul></div></li></ul></nav>
</div>
</div>
<div id="websiteblok46" class="box pcw100 pcs0030 tabw100 tabs0030 mobw100 mobs0030 pagecfg key-next-regel0">
<div id="websitecontentblok96" class="box pcw100 pcs0020 tabw100 tabs0020 mobw100 mobs0020 usp-bar element">
<div id="cmscontentblok294" class="box pcw100 pcs0010 tabw100 tabs0010 mobw100 mobs0010 cmscfg">

<div><i class="fa fa-check"></i>Gratis Versand ab 75€</div>
<div><i class="fa fa-check"></i>SOFORT Überweisung, PayPal oder GiroPay</div>
<div><i class="fa fa-check"></i>14 Tage Widerrufsrecht</div>
<div><i class="fa fa-check"></i>Garne immer aus dem selben Farbbad</div>
</div>
</div>
</div>
</div>
</div>


        
            <div class="wrapper100 wrapper100-logobar">
                <div class="wrapper-content wrapper-logobar">
                    <div class="logoholder">
                        <a href='/' class='shoplogo' title='Wollplatz'>
<img src='/styles/clients/wolplein-wollplatz/img/logo.svg' alt='Wollplatz'/>
</a>

                    </div>
                    
                </div>
            </div>
        

        

        <!-- REGEL 1-->
        

        <!-- REGEL 2-->
        

        <!-- REGEL 3-->
        

        <!-- REGEL 4-->
        

        <!-- REGEL 5-->
        

        <div class="wrapper100 wrapper100-page">
            <div id="pnlWrapperConent" class="wrapper-content wrapper-page" itemscope="" itemtype="http://schema.org/Product">
	

                <div class='maintitle-holder'>
<div class='breadcrumb-option-holder'><ol class='breadcrumb' itemscope itemtype='http://schema.org/BreadcrumbList'>
<li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem'>
<a itemprop='item' href='/'><span itemprop='name'>Home</span></a>
<meta itemprop='position' content='1' />
</li>
<li><i class='fa fa-chevron-right'></i></li>
<li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem'>
<a itemprop='item' href='https://www.wollplatz.de/wolle'><span itemprop='name'>Wolle & Garne</span></a>
<meta itemprop='position' content='2' />
</li>
<li><i class='fa fa-chevron-right'></i></li>
<li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem'>
<a itemprop='item' href='https://www.wollplatz.de/wolle/herstellers'><span itemprop='name'>Herstellers</span></a>
<meta itemprop='position' content='3' />
</li>
<li><i class='fa fa-chevron-right'></i></li>
<li itemprop='itemListElement' itemscope itemtype='http://schema.org/ListItem'>
<a itemprop='item' href='https://www.wollplatz.de/wolle/stylecraft'><span itemprop='name'>Stylecraft</span></a>
<meta itemprop='position' content='4' />
</li>
</ol>

</div>
<link itemprop='url' href='https://www.wollplatz.de/artikel/18099/stylecraft-special-dk-1001-white.html'>
<h1 id='pageheadertitle'>Stylecraft Special dk 1001 White</h1>
</div>


                
    <meta itemprop='name' content='Stylecraft Special dk 1001 White'/>

    <input type="hidden" id="ContentPlaceHolder1_hdnAlertTextTooShort" value="Der Name der Bestellliste ist zu kurz, er muss mindestens 3 Zeichen enthalten." />

    <form method="post" action="../../Product-detail.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="form1">
	<div class="aspNetHidden">
	<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
	<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
	<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+9gKDRX4VS+N7+NHChgHXeMxArRt4EeOFb93T8UshIxolv1ukrdu+tfSoBkWNr2kuNeGn9WjhNkzR2XjmzH4jMZS33N+1I9gN9m33eE3aCo2BkfSMKmDZc6TgZENkxBYtdWGQ6efIUdwahYECIeB/EGqvyan3iWBlBxdjAHbtgNOmVz+DS0odD0fWJVe2yIRSuEfiJszGSYgKYRHUJB/tMeWO9QsAfU8KubAsJPJzEvRuQcZyKC/NrbTvSN59sFrq2+TxGGDP42bAx70pZiycQZTKPfWIogQtMpKiLrFN9GB6QMKuBu4oOaM/aK2n3fwaZ/bo0j5KZVD3XA34qoRdmSSfyDZ99nN/hYBrzWD9+IdbDCAsM8OaAAoXH9Yf6dpRlgWr+Vn+qyZKk1re29y+xMkuIvC4PnS3e9y8wJaS/OAoRSW1E/m4BnsA+4Bq2v25DFLtd5GjnBgfdsE15X+Orvy6bQmHXeVzuzNsUYBQ95GWnXSYmP5ScVJ9Y2Ev07FS/v68IFks/53aGub+ZsKY1l3gS6kiur4DLIApRv2rYGEJDIH3/m3/9Zsh7wUXx2vpEt+wG6jNdVJ2aEYgHDkWwGss4rmebP1D/e6aSIZKb4ymCecHh0TVvgPs10FKwzxYGlSbJaHsEIv51+YNR86jgr80pgciRoSzSfsGr1fobe+xJCIcJcA7+r+XTnGARuIT+soeSvmfa2tbpmTON3j8/GyEm8Zv3v3K+l1Q7jc11GgdYgITYVKtLpDuCfmNNMdZsxvpdLXURSJpEJkUPlqqKItnD8kOMhgHMA099Awp7IZdV+VAmjFTPeYuLGBRpWudiT/awBrXuYcGF0Y1bvEOevw0fqxfaMPi0IfyW5lCd4pm5+Vy8Wyg1F8sfTaqV2qE3/srHlIYRUlBruSF6A6vkZwjcFzzE6tYqckVoHJccUwOqdmXlHOf90TFl1mXeXXTFow2si0IUGE8yt7JwJRO+0lU1KQqYlOD0sJ+DZquK9mYHGi0Fwm6Pum92Lko+8Szipzp3z/cyUVKpXQDYk8YPCZdp0JNeSHMgKqp70NvQPqv4FhwAFweO9J/lb2NCCJhT0Oqal5cXAxNAnYOgbPa/ZbTXE2vUoQGI3WpoTPs77vgevWEnomijvDL77H4vmq4ldks7C1d3dwwLY5sGPjCLUlD5HkyBCeE+ungJEZHPBLPItDSa8SiKeI7jaVShb4VnOXGFcrBrg7SMx/PBXtcenaEcUpGVYGssMEyoWENLuLx9OJEqKKNCULfdRhTUn8lKTAb4quQmc+vUlnII/1VKCdLnMsdaiTrk1GsfBLbfnocjzWJNYIn5x0pIaoufgfBcw+YozFPxrsqcOjI9wzLPbr9NE0f6W/Ix9BleNsnUmGPbNFsJfxQQM0iim2e+YE9cKFSmByrovlQn5gXx0T+ctVFBuLJJslWdsG1JCkaIedm7PoDOTN4gcTBrsi4kkpF2B+QpMIUcm79Lfyt4VyBaAZ4GUdcLF8AUgOGdVh8SJcquCxKerTCQ+F4tgXX/U7Rr+tRthYCPEpndgmAsvfVo8u2cRYYCoQBonUDys3AN4H7CewoBUFBxxEMF9Fzem/4SpYjb8j5JQqswX7SKKRl7yC/OXk1/uaHbmvwkiGNew570vCKcYyX6XURYuDJyUhUp3S6woxT7zzYH6X98vJvKkd3e6KZp+CrSjkCAA18/pFLzotFN6tXHplYJEP5w+C3Zb/ZR93GCXYz5Y09S4wvjOKEtkl+nJsPfYPbCopWxzcENRnriUM+9qHU6QJpLwPbNiLS4p9Nzln/LHqH//QxFqFz30RD+9FrzCLX5IPG0w4pwYoMZDh1vmQD2x0wA3j+a3tctwZwCbIEIDk4fPY50IFisKfxLTaeiJ3V0IfLyNuATkALKfCUqNyizJp/9nOyKqdTDtYtKmkjzOJz9HACM81/BDTk4K/OZwkVqY6h7caO14zCs2koklPqoXe2q43/h7AZUMKpLtESWgIEWQPIaR7OisloXAEhGOIlychvWgHLgRrN5yFNd1wTUINRytaOsb2vAFs07MRwkvlELXw0BO7iIn4fC7FEf51CxjPs23FLaTaF9cTRlC5pDADkhW8kJRjZB0C/6j7lMyYQggch94zxNZ/WXx7FJRmzqFioIK3IyPKjhhw6+SrIlmPq9Ab4Pt5bHQX2TAh765r+wvyngdEC8xUPEiVyfEtNVS+34d7dpP7N6folYi+4QzGznec23FvvzwS0jnjqVFAnz5SImGUPIHV10Setacg6Ws0lSF9vYtcSLxGSS4G1pQBFamLzAdIHM5VnpPgww11QNwmvsepG3VvC8y3CcaevQdANWy6DG8BEpOw7uySf4TgUdNWB5MpBtyKMlpJSYe1HsRdIRcXBoh/c2emJ2kkOuO+XXKlCapt0W68qs63NGNxhmKTSo9nyRpjewwTAmhSb/TFRWlwqhBI1eo0BzONF+dxDv7HbGTgRQYeQz2X3dSvdwV7gj2plsgNYbd+fF4uyCcqTHow6BB0jO/ncO7l+p6TefvEJwML/nSx7azrKGBbGDW0AFFL2PJoxkmptD3qJgYjGftDYe8elvJuhiIXT7JbXduMdcO05ijkqHhl0727F5Gr81VfJrjb3kKiIgb/OeP91HseOvl4MaxFPJH/50M8tldmzIIAhBwsy9m0y9lSfZxkf20tK5LJmF4ah0/0w/8w8V7zt0IGmmHNJcu7RJ6EcVz4lGS6BGZX+sL8c8AW/ok86h7G0vXFYS+akzQJTtcE49IJZAw47sS4+RYS47os5D/NK5iwJ0pSiHKnXVr6Ax0rM/pwTVoL7gxvqSFzvzyVOlzAy2PKOSHXPmiFc87FO6ybqCScDJdgo3PjebHFumI1gH6hakHeUSp+7g5mZLf1kOl78ek21Np4hT0/OK9eG0gtCoANAVsAYYYZVJOEMrz/7EgMEf9uri+f7lFPO3AT9KIHlKcAps/gdsuuFkimhPxedcdhTIO1CKbujJHPd4FDPkKb17bfNTDCSEdrMEfVX6dx2Ep0fIogAH5B7efbxgAIbndxCJzB4fyYnV3d/QAMEhZcEL9Mz/Xhn5VT/k/uSYz6TfhCKVke8OWRKelPTxIfXhI+4EQ6vqyFQafk/3ZsnsTGHUNlYuMSaaBzo1s1y7IzTTqpC4rVou2DkS9UydQbd6sh6GdFFQyakb91TpE1+hmv/K4swc/jjggSbh90bo0NBNX5MmmYjC8KOpMZSZ7OPcZJCisB7ybe2gmRCcxwdeuJ0vn0Of7Nh0qmI2UqadnE3eghRALARv8YxSSjfxSBgaboYm0roNqQ17AYEJSau0MGohM3AOnlcGHgwLta9aJouCxMWBygJoyVxF0hpfOTes0pbu7gt6arnhVzQ40NKZnnOyXtUOc1RJTbdq+V2T3Aui3w9KmhNTqLgngvEACKKmX433MKyhD0P9Zhj8hVmwLKRLWLArB3cCnP9h1Bo2goX+x/vr1cGvme6yHFRmzaGavzXWnhm348P/Fc2hxCBM9Ste7S5nYhaQVlOg5rxm/ihbR38gGzY4W2jnU8Da2STIS6hzjuO3PALjE/I0GQC7GcWeqUs8h5p53kTPsSRoaB64jnLx7M7ikx3MMNYahLtIZ38cpfRLU7oERHitGnKjMkrh9TQbT2mVtWiP6PjT9SrFKPhR5/SFZW5M2GrNngcir33MVH0/rER2W/SWB0yCHIJjzDuiNyb5FTgRXV/49LDznXb6tR0bfASCpMHVTFlKusDJ1DMT7uBgiPy0WEydLG0RBo8Jkxx0stNsigUz0S17wR4BvlGO+gqPLVnqeDvayysVncehMWPQoBfaiKKgTLkyMRsOe5qHgmbmVNXzMZL2nwY4V0BcJVvvjFneJy35vi7zbWBYA+a8IL7fSauP1stMPYnFO5jQ3PMZ58DKJCXBs2WZ/l9xJbfiV49C80nKqc89hkA/krHgoR51KSHlPsZ20OMZmZ5PPq1dSWlgq7e/jaCpvLG+oOF8d0GkS3iUIjyjHNWiMD+RVcgEjDIP++1aXAjScASTh30uc0hmoK6xvSitY+vLHH/UqUXKmObWOS3c71hE9bVIVHey/CVg50z/a4rWN8GqhbO0CEBegjPFjIFmw+VBmFgK70W72JkQ8ivqud9hJBip0tGkClCazNNpUYChac0QJM+pFux2B1Odd9gQQoYACBK6kf+8/UIXIUitMk3kDvWY2EEJSg8bI/AShxk0oUYP27Iu0Y7WqvjI/ogvlxqu7va9Kc+wZ+P7vUKqoSrbaOMbATcREAxGBWDMHodGMW/lGX7c7ef/emLmn+5b16SbNClXduHIy0zB7uucMfqf9GqtpnzVH8fBYziIfubGg+XA54Iisk3Z+7dR6zvcQ5Oy4v7bSHyA2JToRYYvYo72fICESqQLI6bIK2vsklso122j3Y8B9ak73dg505kqf8uqOo9GHF8wWqSWUHjSznblg1/Bw4x0h11C6xg3EhqLBhz2TL3xM8jrl/85SKYCpl9bwuukGbifQw8682g3NG7YU91w1Y73e5PMFFVADUx1zaNeHfXrn86/4pWYEsHbQg1SD0nlq+f+vteSg9/fclrEOOAliKjs/ipal96VeUEyQMKocP4Pmr4VRsG34rlGR2RLh6B89zgC8A5qNios+ENhKHBEN8R8HPAtBWGjMxN2YQVuWsbCWo9Gx4VHDLplygfqFSGCtz7OLN1fm/fIblsfD5qIZbQ0MjraEIjhhVlTKacHyRR3r659Ue80uA6XiMS/bK+ffE6ybxN6FEeAmtghYyEkf8LWDDzMLiWxRPepWDVLUAotTjDIC602/Hxs4hsADLzxU20niGILg5oZAdq8hlw9oRQfrEQE2/IF3oVLvWnfuJFT1ZrCknPFqusqDhlT1DcLE/RM+ykiWT4yNFZ0zkNhPSZy6YR48GcUU0Pp0TH+sAtkCh/wZuqiMWVfvAQyi/zVCuE1kp6/AZAt5domIz1+tI2P4LPYgdst9j3VerfCmB15iN6zH3c9OG1n20xeFBvhs2GceJ0Gk9jqY1aDh0bbwq1OfWay0NYeOlSIu4KgJV5mrI5d0ph4BffWeaImRmpCsaMk04fiEharnlxYE5nRiTDg4oAx8azzDwHbJmsZ63/9JiF2QVjvg0ZaP5qhTme8F87GEuxjJEyYFrdrZuTop7wHuwQuhA9wsVs378jGr3FXho9L9dffj5fmVIchqp2HKPOX7zV/epq6Zvn9JuilFDO64bRj96ixh0oHzch9MRjezvWrlnwc5lE3FMp99PFBdL6u3bYjzFtghqz2tSEVKz9qCIyGcC/aKEwDwn/z/fpw4GxBJ8zppx2TSJtrPmdtUGTvJ8V3zC5QZEVjMMsrpb5C2H+z1LwsnMxNiGUZ90ZiuACPqhaGwyRMnw+EL8Y6iGSt/b6ntm5wQ98/nT8O+RAspbP9hULPIBtm6U9lV2BL5xjprIOQzVl3RGCNCXiETZyH8AnVS3CXn2LZLEf2BvT83nu53y3ndG1o0WAE6P8SLzv3yVOA/A6124vC6llXz0ugWdsJcGs+RaxhDjzQvvNCFDqhPgdlGxq69Gqgry8U0bdndZFYh3wXetPqQAPFgajgvCJ2HykHF2tlEZTFqODXKnwtc39NzZBzBl+NmAr25bGXyFqRO734L8eliHco7VJyri31OAcLkLyD9Ulo+TWkpLBHjUqvlHCbZvVsRY8ttAyZf77u4mXnenRnw+T0CvAwBEn1B6qkjvUR28b9pBbFWm1R7b0/A87wUJo+mYoZsotvCeIZvG1LGz8wBGnQiVYSQRWMFAd9yG+vaDn2eH04hvkQiUAIB/6d7I0iibPyMzzWTAo6WVWJ8890j5PNFAi9F7NBSdIENquTu5jK4iq0wUcZ9PvEoRGPUmDlsyP5Y0fzs20JCYNi1GXfgQTyTG6q4HTjSJfqV3HNVbzBCp4+LNDz6cX6//54QKEpIWFNYaxawk7H5jeIUyJ6SmMqyVId0miraZW12Dn1ST6inGCpizBZ9LR4M9P+hy1WXelz9Dny2UjrG+sLTSkVH8hMqNFpFDqJ3t6cmrEyz5hAarD3eP6nO7s/eSsnOCE5Zsr89tGGFvdScm1ay/+nOFL0bmjkY5c5UcumBWGYAp3H3V/OBvMF3GmFwW0RjPu7jT4WljvJBFy0O/lr76ZDjjhBGZRr3yi6i1KhKjdK5D+hO5e64k0h7KNf/p66RpiL/QFlvG3uY9JdWtEDWyQ7HouMuqRK2zYnjMfXxZyCA9ncSCWxZ2bzkjaWwp+fRTqmrv5VfYUINvVWaWR1K0xbdba8Ql6pjmJHKrX/XGUyltj+KYVxNdtTAoMrcZxB2EypspTat+/a2oDdWOc8j2eF5m9KOHWgQNuclDimu6My/ee9oUemn43impvy/i9EcQvb+KASCz1+fnHVHEl2JHHgT6HeQ7KL8MohzhUaK7Jt8HdaBKVdACrvJIxDZFj0lLnvkZMOgFdfWsSfKCEdXVywCzF2grLysNtNR0SMfvLPAzaxgQ/XMjd/ugrlIZThUrc4p72AWeR7LJCV+0eVlV3oxh/wsrhox5na8JzBcm/eT4+uQL6+rRwAFcI/rCl2f1d2zuHL7SP3Fjymj4dC7YXV/KSYJaP216gQ/w4+e6UliMBO4digh4ltvkk/qB9O0JgXLYJinzDxvQZ9CGLQ+L/ZJ5yHmFscJ6dZxIg1lPtEFzMNUIOP+WJDpx5QrcMjrUAocBnitXL37aFQe0W+P+o+5IL78ZX0uDS1bJLEDfN1oawUhn26FSUjDsM8oo3PmaPRV8IWFPXJex83LDNwwNx/US8wtRRKOjwTdrF07XirDcHRZA2IvFn5+nnjLvTowY9otsyWq9ocxnc1D9weXX5GufHTuke/TCRbwUFovC7ls8qG6ZWPpEvSaj/MlWlN5KQcUjhWMx1LJKNSMSITe2PLJ1Sb7wGUOMJSCvGUlyHtq+2LyaLth7dloZrIeQ1/evhFollPa9/wugsdHJWScA9DvY1q1rRIjvvZTh1GtJS82HnQzkpgxibzkhPVXtoinK9xzbQs5Shp0ATSnnQ49UqW6ErecAjSyYFdtYQRSzik66TYdMffc03bxbN+KArSCwG376eHLauRS6vNYFW3StgsK1r393dIpZx2h6ukbTQ4sCMh5tp+Otc2RGfPH9gcDjdmeQ9HrGCrqGtoCEIoMRmFugE/WSnOPLaWjUWRShHssQR0yJWIJlabEXtlEPJ51pXdTKRmBoRLTNnoH3Njhw+Q6KmQwXL1WeNWPzBkgkb2IXN93h5T3CkiREFJP8+y0rI7xPWRR5YID/S1haaJheUEHA1DULFYTi1buXKksegyPYamV5ZzTsTKG5iRj6j1Ev839GP30//AMmVJI3ZR30l6SHrGqN5YmX+kTVI5ilnR+WZmU46IQ0kHFX+GlxkE74wtKx6+Cva/oY9NYHjB8+1M45OhU7m9XgWIwrdXqgqmoceZbP0lM3pmVSf6PxpS9Bw6t7RDlovyjYe2uhWCZbwnGGrFhUCpIvktVeoq7b3ag0+bhXiNQOnal1+9oO6JELOYFI6YyR5mz4EusRaQ1Fgyiv/leJcvfFtBKTgN+feD3QZnHrdDKtPpy7rUNsU+7LxTV4PML2f5MNh4wygqw+mNakPihZtrY/53RrEaFpbBd8vaerMQZUsXgLdzxvM6PfuuUQ3L199BlyAOd3Sp8CvsAxVibGoY1/DV4KED2bQ2KlNlkg2tfdBAnVnKk4radoC7fJtobm7EntmY3YrwrZzq51izT2e/hbvH+E+H7ulupUkP8vnW3V4zAFYlfEFSFCEiNtv7FmZJkaGMUUpTl0LSCTnW+Fhv5VyERg740P4mg2qanh+AyEClK8nmBknCnOqsTyB3Xjxob0V16O2U0gZNUJPOBjjTuD6zKfbr5UtKPZ7Gu9XAhLsKYcrIIzy923CWeGzBdriP8/+EBMM7AKUypy2On4PrNKW4Zj2iR/PaOCLS/WrNBi0dt6fPjH2UIae/WRqf5mxjtonUo37gDbywHr7oHeMnN8xR6Z3gEpL0oz2micKFJ/N+rcDegQ+WggzzzDeYfeABcnBMntIQBNgGUhz6yZD/4q6MPqF/nJo201KrnxaCFYdFuxgRkNac1UuHT3IMC2Ht01PVOaLuWfp1U5Mcbnggt60SuLz0s+USFfpLxEAGGFtxkwlGFQ7a8O7D6LtGinqiK1H5xzzzzlHL0ps55AIeTfjcxlpaZPEGahhuVCnWibGB7Hh3+cO9jSPCRgnsuk/C/0RKupcJkOLEsGEacpF+dTLmFYB1ZG+xHmwVb6uGU2++JNq6jVzsO1LYSEC5uhBCyRQUjm18YsUPxt9V2fjYA4+ulyHJtvtbcCG1vqfv5uDg4jnpoaTR51vA4MeBqYKq8DLX+9G24bCH3FPiiBxKYbofXDriuoRgZoVOAjn8IHmux9d5cU9a1LgFlRygkY6wuMM0PZMdrzBD+qEjgF4YCU7OB+SctEltsew2qfsnW+HgG0GDgMbEgld8AmWU2VG/22zaDDb5nUQaTmGekUi0SqPbsOM8yvEmSNcqqRo9bKzs1eyjdGFn3TsBW12SWbLWnCGpLLbEWhHEZlGe04ZsCA/cRJN3n1hAOJrrcr7SA1aD9Y8+QHbkKzakfb9xd9boNGoyNaNCPDe1JaqHAbtR4BrxNdTjefEntEGlOwPUKI0kLI7wWV69Te2r4eppWPD0nO7hRlPS+AHbcm/dddO3NQ78qNtslQOG7de1HQF3cs2/CkaQV2Um3MgujUKluV8X9a4bbrET2zxTWyk7ftC/gQghdnlDQU/mYFzu+CJ022lLZ6WpCJSIbikVmoWsRGhuprIQ2Iw1do2uwUzIMiCnhqevlEtB3u1cFTEg2NELyrrDM2lwJsP3qwSMBBshCBNsxYCf6M6pEsd7YzE4h1KN1ADtFbkds44MmfqpLn/5wKGNKc7IyTT/cRFNlaZTMwBwtJ8erVlyH+zFmxYTH61/8y1DznvZ2JxrDo+p1n/uk1bzDBX6KVr3hD1bDTxHVl16mlLiaOVa8QGAfk9F7DAAuMtFnMwCDUoEJvGn4dQTzWw8leiJ+jiVhdV78wVp7c78F/Jzu1/8e3PzPgGyZaOvY4sVxVTmZOX03XdIficsmPOY2jx+d9RRvSnDI9A+2ZXbSoIxQkIx/JJJF2ZaJHfUoC8lA9c53CvlPPyWiuKcvkUqFuy0nv5U21ax4x2jgDbA2FOWZgf8nX03hyRwjBQWRKPpWxFcWdadmGMov/mgfTGlWRsw0b6jj1ki/5+ZxSLNXaoKbvwcmONcQA/MCU9g5klB698AmY0qAzaVj3pg5VVDTF5R1sVM9ztz8bN7C3Kbb02XF7gqDcFdK0Dhu5uUGIMH0HEcm0IDlJ26ZozXkTzKSsJsh3OI2alqpv9m4kjRah/5ng1j3G+NqbN0B3jnAXY123skS3hQFy8zyKSBQIsei884HtmN1fzZgbvmC6qM96tPM3sfmuI6nQsOSkIbhOfl7HLdoYVWWQcD4TKN8KfBZuVkKMnhpdO1RAx1eNTpvkAihFj+uIwkDG5LhaSxjufQHLAZotTR+t8D9aqpsNxYprI+Lt46zuSjTn/QFvqdl/lMrKUB/AftNiXR17FqVmRcledAqb7zCjq43YuTwKGvVAlZzHQg9f5G0GdZDrPcD2ik2ZfZqsUjeSvagVlAiAKQW/ufqOLFxJyq7S3fOwmF+3XWPoy9moELCNauPMtB/bZCAT+AQHcH5Vu+xI8sFL1GmwJ4c7qkPP3ZX34yU5y1u7ajLn8vkZ6yLyPHVf6KETaCEfg9QJqpvbaeYhMsL4i0Xtt5d+Z+X7Km9tVgRglWPK/1xvEclnciFZ2u1p4boOGBHbUjeUAvqpWDUYgvfbl4KAt1ourd26kDKHnAtyxozS/A+U2RV18K58tF9/Shzv5MaVUnky6xZ5MbPdMpjFYl7zykoe2nybWK2eE5WJuV1L/CxjzTrxSHI0N/lwCQ/7S4kHVkUiIxtJqrit0HmmvJZB4SdPquzqJdhIACCiXm9kHggEVnc7U8cHPaRwWyj/tZwpczPShnOoPDyNp6YssRDKSJuT7MjdE/fgmgwSzahtVO2+9yczBQhfnctGDc9Yrv+sfpiiXqW7ozJ/yNnftVfmpG8twH7QCZKMZ0OVDwzkmhqzS7Kdb+FAwoKnlaixq4ATnkQNgpAXWhZZ5XJ70o95vfKHX6LMza8g2l52h/mMbBovUanVKXtf20+ljMNP00K5mt4phTmH+a0gxaaNdxKd4PEaBHY82YJzhGS662ArswiL0b3L1nLEqeWlxkzxZAaKKfQDUZUQ7PpGAOoXDAQR+yL0Ulyj6lpzyD2jQJP8uhl99QOad4W9ucOB6wIY7yosMFUj5QtSMPFPgNXEz8niH5cC3wm/GxgF+04qSwJ+8xQHmVNlCsUlnninOzfLFyAwF27uxbkbyO0ZIB4zgQRb5BssD29WFl0seftBwtX+OCVH39PIyA+dxzayCF0MkCyajJmVe8GMWHlThUcoyXvJANrVLfWCJQ8P9eCfjWE/eaKQj9qeEUVPN9NxVlAAybhIdXVe9CAstz32qmX35VTWn80ynfYUseXZsZrltURY9P5V5zoRb+/uMK4sbEEUos1faLNCBwpj0xLcRoGvajJFqLqZhgQs8gKYZfzvDaNIOAzKfMqC2OVuhbCjCKjm9Po30ybA33P5/ulH821e0nj8m9yr8Fc/6DGwnrj1JtI/rRXl439U8u01qdFKIEuDvglClIgtcZ6R1I2BqW2dSLazOLOqzb7H+CofKHMVwY5KqubNUCusYDjA1jBe9FQK2CFEpG6VwIHm1qTt+LQKdxq7bTeXzK30DDOgXanGAi7shJM875xZ5gHka4xbTld7rW8qnAMF6va/2KLXMyjwc3pCm1OLsUDAA1IyxZhxWIdBbJmB+C9j93chCt8eNp9OOalI8kjZ+/pluR6dAw6U/m0EumgJXb82ney3eG2+JH+0zniuvmqOAqGTOD7yQ5JzJewLqmXlDHHVJWXISdrrKTOtzJ9P4AmxFLQP/On3Nyz1eX7fAPARXskpyWAOaLPI4bDI+cOipq8yUpvRm9CHlp/7EEG658Ui4ZvUBm9UF6cEAQSTAddLjXgelnYe8S4CXuOCA2hNWjuciuk44k/uajs9sY7vCbMnBPod3me9I1Tqmt4DqWnKu2EI/owNX0CO/2Es/SVs8++eWHitJnP6d6qOgoheOL4hIcL5W4ql+YetI0IQVXxyGlapK3cPAYFQ8GUz1/d1wjUnb2nhwRZVDpGD94qvxgss7U1Tu7uMoCmmgJGClPWIhXVMCX4N2PpCWe5evxhNKwdkr6tO6zhIXQE9El+M56K4X0Q1nzghGCJQFnMjRC6NFCOrRxTY1NvsmMCMhOqfk5uBR4mGM/CZSceCioPKfaFTcYqtAgMs+CYKXrMd1NiidSXSmZFDfS4FOVB3yTXjq9UICfmjiNcS3SXgRjjWteJhp2+16ZNAIhQtuAF8dpR1NUvktt/0fcXeP4mL0y8sJAjmyaf/HTEaPZzica6U/0rcMJwkDZ/p7NQOEQ5+6b48h2pO7HKm7FlOl2sYt0p2CpCwYVfSVV5nzmhYVqZ/NYeytgCo+7+/e2pbHTyrpf4hH1qXOzPZWFiaiqF6HHogQjYWrP7FchgelWprJ2K1PBeZCQkX1A+QNdak2s8ge7vHhdliFs1T82Zbh/hXecPLLCx154whR/GX1zjIw4vN1QMBTuPH9DNJ1UvtuXWgOQxS+AK0vTbVAEENQ536BC+xzIHckU6dy1hzAgLdniFhuIiutOWD/V0k0PitWYvTohHZSbJgXayZNfkCE8CR9DLJN+4s3bkqV2LdUsqP8HfAyalLgwpkdkbd+HpcKzKl5JzioJizsx/JhJXRM2uCE/a8om1bO3es3Ri3PWCuYiWDjiu6BDLGglgFWGzRGNi1MAwR4Ymgrn/+ILwZJequNb8MgeMf1M91fv/ESykNoGvDckcaX2XmXxUCJgBG8Qux0PnzBF5VJlr0vv3dyBv/IdRglUtGa7FA1g3zfxs1V3jWxQlWHNylFZYB7sFnZq8cTGopvSmWRSIOQDRSDSnSywNVoF4UEkx/A5/vPdQG0zrb0V8MbSk2Np2hgdicx4Z4ZK2wJ2TneGqBn7vXkLIt/jFAI7gkOIbvbDsr7rJyYi/rlYz1lEMz5SqlRv94GJznP5+eXcv8uo/G8kHtQiT5+ywBV4FaScw6Y/wXgvaOv8FBNShnKyCqrMi9pvM2xWOAsUILOzsJq1+trdMaQzcO29R2+fpQ2HeS9+Tk7+ousHhcvXw8Y7J/cum+x1gj668C1H+iYYFy/svUEQ27pFQ/m6tfLaSZrBTWQMoJYJX6bk5eAUcQFhlLwoFz1DIbOC5PuSkNYksxyv3ZeThpbuPpGx/5E1FaVFFYJxkwUo1Q91ztW6NqLmjltd3c6AEexAatGwfuoXsqGVHh673HE0R5hWyqo8Vxy5l35gNRkQSdSisPXBHsRIKzx7X8ZPeZZPYZj8wyTbhAxraTjOx7Zb8ZwnOSyIgf3AsxS/k3bmfMGY62rnRHj7869J9wpHPeukmw3e7rCRuOZIQ67fAzt6ExP+91VZWCE3PHxKuoMHeFRqixdKdQc6k0lZ56h+rbG0Bqdb3Z8WHrQ2ndujiO687BQy8m9MGQlHZ4fT5yeIfKFjZWRBmGF92NhXrhbWj00kpvNnAPH1nwH5UkKkgsqRqMEF50AfK3jvHJD772u+jXAsCAqerwjmdr+z5yAEd+1Q07GeO0/r+wdpNIiQCRUIAaOt4V3xRVsY+MZWB5ZfaysrXFlJAiUoHtn7qVM5GMe0oL68J6UVHP46KMTiwg8javaD3dgxeCFanpU6C5+0V9h/bpTCUYu4fLK4xw1VP7dE8RIUOHXicfZ+g47cgKVdxEXuyrXFBw4ZMvmb5mxiENW9HGZRnxOwAtWBc6dyPTZwdL5tfSZJROr+vcTEwD3tpngHM5ATEHSEbduNnzK8O7wmuJ5y2zibKm1jCUDE17n6EmQ8srGojn++qNLw6ky/P8Ie9h2Q6Bkle0YLRPtuiZUajDFzSXQw/08MfQw49hxGBdYuvipb54wB7BeHM6av9V8Jj5xKKI37xxMG/t14hNwyNo279EoyFA8BdX8zvNVBjJ8C2thBFqY/K4zXyG0zyARafj6XUC/dKNQ2deXxPezSunoQcXTAmGw4zKscCezgtZfnT2avcW82c9bp0Dx2A+CT5Ha3IIY8vWMQ5c+Zm9HBtt9MVtSAWTiHp+hc58k9Yfs2wWAw3eoofZFFNKWdYzvuZMqhJtcrj6g99cCF/fqS0pXZ2BsZVRdvmbM2BYpDFkjKlVcS5SZBj+p3+3N+VJ9uw244zuheAkQUftSfhb4vIIXGivpEjaz3r3vKNXY8xHFZgKqLSuwttuZg6SRqc5rPLef0VtmSo4jDgbws4JeOhW9G5Mc/zEdDHAArx2pCnB/DNsPGiHBqhHTKKm2w/vb12fY6djcPqcpTw4+62Tl97ibc3uEEO+p9PKFBqG5JztVZnetV8WCzQ71I6c8GUKwrwM0NsPnKFXG0RI5qiAgJPD1RIdbbwzlQ5c14PBH7/dwL1de38+yc+OlPIXRb8agfmSqeIFVjPE9JUB41dEmDy74kNW4V4QeknQLObwODZ7aAf0gX0/mj7B5JYefAmyTeuynUt3QQLaWytXlZwpKeC5dr6IxgfbrP6OSUhoTrLiquBTKmjcIkrylxuz++PuK9yzVhS0nv6Am+ximH70Evq60/6fW2sBgR7HGaD+itZ4j2s1Oa9U8+rjmrnbLJpR2XodR/pkxW3VJLkk73uQNrWNcCRRG9MFodtGTJccGtoMWQGNcCzqM4IF+9lLWPwNXcpTMQi5WYJ2XuQRw7X7EqaAGa8Pt7gtUS1OqbYdB7bS3WPRpuNF4dSlpk5mcjpria0Dxid+KbhK54CyyHrLtb2XPhnqLWjL6KZi0hmNHDoNFh21vmlICFesZ7vkj3kZuAK9Nq6RMVYTzr2wKkwnJCO5DXO5w6r0ppqb1X7C1CM9CHpdhkgPwnLVNKneV2pJvijPLWHr5T0zj670f2Iz55hJf5Odg5JVIC0YyrjyFa31yLBWsvA4gmp7858x+7cdpR/yacHpKnkeRCNbVt2k+jiHr93vqBlyvje5MZi3yCZf6MODxLk+9QokWpp/vDuu4ase+NPmgEbRZFfdBeRTR97tK9oEHiXmXOWzC8gQftVEdBA0ujgZelb18U3KIGWKfvq3OvODMM1UqDccHpbfUuwC/TtQWPrvAehAoTnCbWE3RJ12nl/BQejZrEa8A7rh+QM8D9QL8Z93Z3qBmGjufRFEMzGLE8+6+E/ilB+g1tz6vJe/72NjsOE70Y3K7izsZTtOjWWe/1DP0JOZslnhObQYv4w3NF4qDyqYQAH6s1H5EOUONyC6zIOot95M6vzGrZTU5paY4sLVrR9+EamewW7XfrxuJoyKIdf0dVmXlG4+HOpT4AbguO/5yAer+yWECkxaKID9/o244DmRNpToOAXcSzqxVXYWoYlaf7XBuQUEkUn/gcD5LdbR7KZi/C1XccbGMk/HkHAW/+BRr1+93luXbMmaEpEoomA6NsHr2HgxqNijDoNDqi1jHvonVScOn1eXoP2r3Q9ptlsra8I+ds2ARevf1oRwc5kN3B0nSJLfj4fPzU+JKrmz0y/6OkkSlwVaZNblGT8cZiphkVfWxbBlPQnCyYv0wjUehDeZdzXY2P4+Hrp8NYVtrI7316aTNYX1yK6grMF9tcLx63Rb/8SMjvUiSfdKBmtHMobFZgUccz5T4L5js+xCokj45hTapJL002e03AtwVkXUpglyt71pf3mthPm35uaY1qt2FPR3R9fNWoQvy1FdIINDpYDVczSWKNerymKDfFZ0LhM9Cp/veHYKYQ/uWTSGcki5cdPfNAgEFGSMjTmAH0t0+OSxAFy4gcH0wG//WVlQZoPGi0hj72Euuw2AyS5vFxpBhDsGq2sBffIz1MWOx9hQjVN7kKsfPlgvvSv6ontTSaUehHqzUItShCCYaMMyh6N+1tjtArgtMhMP5OkkNOUS+IWkAa2Ql9a0xTdAz/yYsQoMlCoNPjKR+v72zGV7vKPkoL8rRZxSPHRob3P7ek4VOdQyhguxkipCxuYO0X/tntVAElaSrcU9Aa1mc1iARXl933ueDAAQbLcCw+XnndUkD5LK5A5yUVlHywpOm4JZIrgx+oiiVEcjnnGCkf0xmiVH1iTDnV7+tn7KMp8E6Wo2VsB1F0Ra2ndE1K/JuzJDdizUJYOqBU6044vNhXjeInKePw6C7SSfw6m2rGSuNpEnutuRmhV9qNNg6hcN9dXo6BXT3rhcCccKbim5Zh9S4QBNi6ggcfmPL+/r62kZ9pgiyVgLXVjRJCt4b21hXqjjmoQdKhbh7E7f26tjtPv/bIUN3ad3jyo8tYjxfcdeWzjEEo86tdxiRws0StuwKF3N3WybGXPQ6nydbImLlDSNTiGLeVNGDPfQ0LrbzXBXiO9SLdfCDr/F65+SSlk1q40ciOAYd+ozG46rgABjBdjpfTjiCCPK2UXsWcqrNxbJ/gs3nTwWw1AEPmHyYK6C6chcaYrumR4XWG1Fe53+qG9HPdsQslL+/I5nfBc9ToCAPOyit0uUu1jd0QAlc2n2RH6Qx4XYzGyOJjM9h0Ri+COfJ2RpqJUVrlCLL6x3hAqjViHK3QHODe01FcHp+LlUJTwnD9BEhHSNduja7MZQB1Wui6utv6W797zJbQ6wV7qTB+ia//kQhBw4MNoaQcP3G+HkESsBLsm4jEs8/hJm0U9sVeNzoCtrJ5CynA//tRYfgl2kSXEm/TQjVownxNV4XGL6eLLYT9Dg2wyOOia/rJKE41hnFOelcEKys3qG+zg+29vL6ARxukK43kRA+UHFMfgz6UdqK5hJ/i1aWJy8NoB1VY4ybHH54lw+y4B2YL8pEIkI3q3Bi0A7wgEEd0P/R+KCPnuJooGbK5haSE+x8lU+8xvGfnz77nGnxZSb2v8Ihswxej7Y6LvPxBDkxLx3P1PxGr/WpjhAeegg6DiSXPf3E3tDS6/I9yfX5eF/Wyc4Qrt+sw4TBDUgztk03Ona5u6JSpra7Z+Frlo/FY3B0e0l08zaOjJBCQLuRY6kjc2B7C0FIvLYTntUQO6ypmPzIcLn9aiLk4dgqaaBiH+JnKF0/d00kQmG/PPUHqq+yvEW923LrKJtef+1gGPJlRCXw4UFD+FYkRBPUmyhDBG4VEXQ0zx3fFolEhIkcdhzW1WGgTgXBtpwzNZNKsd97fWdwljz70ROEUuFxu/V0H0ddgjDxlJeulqOgIJMsqam7Spd8lAaTX8Y+6QB6E6O4wSGYinuAabp6tUoNGjgE/YSefwTlTP0MWt9+IBITuBkCPLv0eS4lwPaDR64HayOfl0XNtEvQ6Pe7WL72G0h72HtnqK8YP4qx+l0xy+FsDuxb08XWtbWQldcWTvv3tfUju9XjLXxXTn2Izx2MC0u2xAoiteTUCPJR7QLIER25NZlGDSUuMIzEsjZ5CsU/vUxSyU8yGKKcHWFy7syllr9qP/lFd6rnmFPhFb0Wbvj5QUgs7OruhSek2YRZWQ9zxYIG1SMbctxYZBvislQ6zb5PQr1jl7+W54oqAlmPzR7kVOfSD+xayM8MHGXVeA9Z8uv0npjVsy6g5BUd47DiMRA6DLL0i+ag0Cj+urYGxQSUqHQupA6mSoJ61EBY19xHbqPBT4DosN9OJzZend5s0PqIjlHOfmiZ1TtTOTl4MUlDvZqHseXbuEg4rthKrtWGaS+2wMfUk/0o7YoBujHu2c1htjTz9GDgatDZ/maxpkLxFTkQhPFBSXKo+Iy/QnipIJyVmhiATjVUTSKvfNOrSc/ujDmf62CzK2nIuOn7+v/5Y747vlRGGkL1zqglIzrX+9BdgzN1iIefMsAWE7VuAdoBzADt6JHl6cocPMkDh4PemvIG35xxYm7TsleIq/zx5X9yzD49yvFmMh1bLGbNfSE/pGXEvdg6HgmcTwpFkpMOEvE3A8FeZlfQE818j3H/gZF9/vbOLoQKA/fZw6SkVNP4gQwi+LxGPO9CAI8oi26VlhfvAmOENs4Ln3CCDhAw58FheJE5pgvi9LoUV9qn1vMrfh2/02jKkZcfaKxr/Vbg9bdg6Qki9MHv/quM/tdin05G7m2rGlC9kF1Ccdob3E3R47R4A3n23DVtr87RYL1+rqm22qeLwDpZwkzTNC8b2Sy/pZMpBvPNY1kxsdflrpcU0QxajKMyzc2kNgZWKRzQOOMkDNXlJHiRROQLQsWG4DVpqffR8oIkQA3E26eDIkYBGlcWbM+qJA8+yEyy6TC0WQKFhf5/7Q3yuzidXm4E7t9Lwhsr0UU1GqHENnvLhoQu8+32i4wL5erIom53Jvv09QteL3Q8+uyw1GBw7osgeRLdOWsMInlz85Pv79DPLbuHKt6jr4oNQqn94H7kinI+AB1XzYXycwMMwGXpEpBQx1HmVyedWQLHGOyfc42E19Ot3wnGpKa8uG0yC0TrLnFmjGqINwPJTw3s6Mgbe2HqLxWIXYoopT4tStJhIlN7ePV17W4U2PiYUsJYMYCjDneFihzTYdDxJL5+1K3cQ54ND5xvPJf6zdbFil5sEsK0IcPF47LPheY9kR+HV+51zOeaQ1KpijAvETPscxio0UqnhRnBFqcp4+/1wc6QT0lvNK78OlWytllfEOUx/9OQwV3P3kIyMtUnTQnlVz+Edn3WoC9CIzEL8Ds5DZO7xFISyI8NA+qb/XTR60/nT4oqVCt17NoPB6gKxiuVyYsXPm8us+kNyRx5FMv8L+m9QH+/UAEqgDWGhSyN8TyWaBvG1yhpnTcXQpNEIP83jRChNSsf6eGn8Xkwfc3hadLf6qxUJm1MWnldMuzYNYJgk6hqKXQiwvoiwL7+Op6nEYcjnQyNuiwXU/DZHjFQPlhZ+kajNsTFFLoFkd11F87JNIl57ctFCodnV3s3pHmY8TZhlCcdd74NM8smHgqmNIG37tSv0JryJIy4RXGdxQtqfNca4fgUoaORZKX6+rGEr4b+o3bWNbg80D9/e0cAJIju+FWUBqcEwgiSUueIi27RdL9iVZ08FHUzw/YIsCln2ZMc/IHNK8kkH6r+yZbpBxiVIpqaa5FN8NRSfBBz71SADxNG9xqW6AZO5164Fj75xDHyD9DOqIoIqygsm/p7KMnW+Qt/QP5f7KR8uSCE069AD/Pyr3JLqY07xzDp6+J/bOcNjptlmhD+ray/b+ep0fHrp/krfaiWEMvZh35gwMkHJ81m77AwuSpdki02kmIwVSDvWs/BVKB0vL6KNa5mz23mmsgXSKIyQGfgXcFQzd0aub+gLPxrVz9Dyyt8EQ+odZs3FGZEAfi3d8RUgpKToOaygoOQJp/DjI2TWsT2F6KIBtdxH3eegwvIrSpC77FGOePPKdWE2VMHbYfSElngRkeFg+j3SMc8NqQSvmTvxoHbPee4mlWzfEu39+Aqgx+TXjA79eTo+CTkzbtKOegqLm4AV2QP2Pou83D8y2rFRYJ+3jUeSs/d5MPqZok5hbtrz/yTxJUWVwfPVdmFJ7+Yj1iz2NWRBLpowpRaGb+6xZ4md3UN2OazakVq1AZrJC+o5IRQZlQ7zdpooFdU7uA6ub0b33STEvF8WGUFfyehqNMcuaOLcWtEifURE4ARAs8qg+xV9rNuR3Zw0iBgdkgRqhKAChz3509YB+lMtxnYEQwmjjH5ANvPRWyE4KS1sML9aL3YCALwo9g0UCbBuAnoYLJJsM7TJCQS58goJeMH2qXh1OGwHduah6qGhixTQ86bXG0OKwOnYgkg25TPMu467D71d4hIrpVHiizVhyL4FYnQRhrZ0xOOFjqLa9kigwhC7ol9Mmq5VrZXL8lv2QG++WTlanGkuTt9I9UWZs1d+ngwetfkzZwTQjpkdm4ZxIST36rHnvOzThznZyKLIYDsnVc05wl7CHHNKw686MdePgHr8zyFgQoSpdIwmws9fkEK4oTD8stXlay37ByWHiY709PMtC2bF2aA8fMWMnF4GDihfKzsXdu20ZwU36KUh0uhD5YKoNzjxEb0GNq0FUPazins4xjANYP2kqdIdx6hgtN3Hi2iPuMJsHeDMrKlAaHeqTZrXW1TpLR5lDdOhwtxGvDkD6vUp8oBwhGe40Ho5nHiWxXYohED36dW1JWZfRvArbtqLsAeFZwm1nrUbCEoo4V2Y0TRHHZV4S8j7If+3TwvxizUt7QoNSmE+PUFuexA6pFon7BcPgj76mzkcaxSUJiSCacRzfnQQJxn7VVp0wewRljWgbyd1a+LDMgfbaMe3Zx0pg8mSFWPw7TLo8tZ7/XTiydXeOkzstHyvDUIG47brClldGJNY6OGeXiKJSoEdrDWj40VfnbmRUgV/FJykwqoFHjmtQ+b8QEVrJhgbcYEyJw8X89RThrPtXrOAL1c49PScgL7sjnhfz+UQ5b6kTMUEJO3R4+7UC2QSG2jZz5NCh5NzyR1EmAKDgYNTeHjS99pWKtpTGLUP5/2jiWvWto2fUBZmmEVqDu9UAM7+RG/sDaGJk/l/5kvDRYb/OL+HCtLxre3ntrumAMyZckHFuMK1aoYzqIGhefEtrugsusGavrAi1ODnl8p1Hqa3PI9MYvGG11HzFwzlPS6CpRXSx8eCeEhqrANwt5AdkB6CIxsw6cENLRg5svvoWu8MUKnc8xFWaxZTkdXCT6mc0yj5CpnAjqc+XIXbg9wo4bqST+r1z4jwCThMPutftZrUOqcsAM/itXpMBNbNA/IdmiIsLxo7GqX3G66fA4nOYHQPr0a9XM+lqXe0l3FU3bYQN1QhP2kH7m7fu8OQ7WyyePjhUpX5NiuJsZFFkPWRzFWM43KMqmr0tK8v9OyioNM1YOQuZY64wJhDRZrcwvi3GW2nrepfw4yIxqY7M89tTLU710XNpSLcrmtpc43ugk5mmmVHgfEyh8YFn2v3l7zU/J9HxfgQcUerw6w9Mk2oF0B1P+zAPtlC6GNIF6/XPYeIrNf92KVCOmrsV6L6LkM9YImoPo75JbvnviLwoc0UcoPrkQcHC1Hsyza3jMINu73KdVj7+TDoxUJZbnEMhM5UEFgv2FJG+bgOm16TpaVcsz9P8ePsOO2Pq5RpJwpkgV0gRKYaavT80hAfgFDtTH7q4Y53mzy9zWkmz+7byVFJd0UjsJxlt+uLOCwgOC0mF8as1H618BTa7kzVfZB4Jwlz8UYG3Ey6B2EMC9eQxy64jvXb/uYJM6eNb5LAIjJuii9IfKK92cPaqjKiY9NFrfZ++Nbb+f+5xKrYwBUfO8vMIzDsNscXWOs6yUv95ZRtiHEeo7iuSlVrEVnNYgLZ/WgH0/b2sAAnAd4pu6NmgM9bk1c/rPCzBQEg2R25h8a+MCarYtFFMDdsBzeHZsupnQCeCO50WayMx/h7pMwrIt83VtEeJBFyXsrQ05elUBfjZ/KA9UPp+eh0qNSO2WjxH/SWmyk8PYf9A8XhFduxNXmAzyw7RYmnLJaf3Nvjx05RiOTKQR9evEOAJLwoBcWCxt/bw2Z2f4cNapAFm57/6gNHlwDK8QMflJFfQEy/yRYKl9xJekhE2J19dLFjfiTOWuZLpfCO9Wed2PWlIGJqnL3Yf1Pm3BZRneBMhTQATFFeGdeRJ18ftS1bZnRdPEvCiiBYOuo10s2gSHFl7yaNNc0n6vBa5d/zinu2xhZNYsaN3JeyLbs3yAxhsFTcfQBeQ+1FReSCvAr1olrFkfcUnTLQNKyQ4cEEdUlgc8K7Fxm6a7VbyalkBxgj+FD3x67dZBA879mHpGriuPBXht7qsxPofxuftEDMqD9NDNl8QNgJ/roGpu2GayI2Y/4VsRTmJY1N4sUHaA+hyJXIm8Z7LGPy2NYGIsVfbvfOGhvAGv0wK7pKGFXzT8yHd2GyG+Lt9hE8mCCTdsyR5EZmQ7z7NVSgT1oA6bJsm1td/Kg3USCPY4eIgoY6SGE+3jNU+YxbayW9NhIGh1XAfeqwET2aVc9fUuVjMNvQmv73cxUL675yt0+9EExBxeS1uoiawYOG378gbi4IAhlT6sxfjDmRT1yCnH++ffsLhdhrj66kswvziYl70a9RCltNF2ixitctb4d9qfXO6PWqXLyDTu/vElzrquJGWrZ/LsnESL/leQf8quG7fj/VFl0iMJYN8+SP3tMTPvjwzIvPVOaGArVHY0v7UxWrJdtXxbuNtPqvNeFLgNo1JWCV/HK3Uyw2OemHiAypZ+dwbT25mlVIwf68CQIHHgtDuoxFKGIyuG7hZGdd5HkrZIHxuqCoPUQz00ENbV4gvgFxL4hJaszWSs2vEXupwiWtdE1npVCtLOuyvGwbO34CjcMUMy3zU4G4X0vdhcuzf9PhnRS4AmMcsid9FNqCaqzyJzyK1GZy78RfZ9lGqnGAgRjzmbg1HOWmqvrIbTjEJ56Ovt1F+HIHM1n++YT2PvzF9flGmoor5+eso7gbBt9nCmtPcnSgCGs4u4xU1iEx8N0ZOvSpjsqrbLMzZEjZUVGQIM48q76pTXjO9SkiSxXVrK9D6s+6ZxuBf4YWh617axkCFteOtUVBX/pFKbSYs+akM0DIkCNhGyeU7zMqi7UAKn0fmxdarv9FZaB8qC/NVhKIkTCCt6xVmbmCndLLuSuKdAlA04//TxlJswoDWsx/K85TlDTY7WXvJSPjzG54strOfKW0PVe6l1tG8KEp0kZSgB1gOytkV3FQNvnKgSxdChR0zAdBupOGr1FQd6bcO3GaQ7GBYNHBteVNt1jzSPEQILXkrJWdIHQNvE1k0HnBXN89qxMHvH2q7C3WzViVW4qnTp/AHPi0leEy7HDNgP2MF02LfJVjguZHb+4A7hB/Q1x5vFmPFoiWkKeOkogNH5JpT7y/NTT6ymGafTdiBA0RqT852CIL7g93ZY4Q07I9p57YguSkApuPMA3hYt7G3eNeIwMTICyIVEXu4A/wA2mUco3rSOyFGWuFuzncsiUmwhKZMnJAiVKt9eYn4pI0r4CmaIAfuXPHC9MN59dgEJEODIZlzlxKqbJ9S+9MJkQxep5X87f10kfAa4x97KfZolp+3Xe5JMKPtf6RfaOYIjYK/tfJWIbbtnZJui1ER6XSQxh9+jx56eOyGxgn5SDKmBPB1ONSLl13DDmyRJE+Q6cRA9AJVASMwxKb++B9ufpAfNFO9Joh4ZJqP/pI40KGxJmssOikbbIZehGp9GfNy8ugtqd4GN2FzlgfvLlTpfX0CSINipuRjfErnGhMHo843UJveho1tuysgGNI+k/Mlny3U6K0899rCsHXi4On1Pxl6BgBNZTOgHJSqd5E6TAO6nDGetYmmBe8Kly4tBwnhcd3qnhiftm4HDHGF1ZbsN1wKWatSXJvj3LxaZ9WUD/MwggLQcZyjCkF2RDSTDXnEFP5wYcZPiiO6bHIEY4ca005Y5WsagaRts9/nRaMfPxwjfB7T29b78Yl/zs6KXdZFfwRLPTPTwHYMGhpOhD89n0hskNY4X2xfTbC8hSNbJ/qyrghiBQUjguht0QN4UBSfoiVq5Q+zMEYOrV0RoBqglou+2nj6tGdqlepqf/cGQkCfXawrI5g+IHM+qPh33PBtFBYpmHO5WboEV3Anua9nN58eMQFxpeYAI/90okmQEiA3uuUzvBDzxJNMZrhsje5uVo19aZ6spcaWOTQDT6FitT70hsqIlVPC5nfw6CwL5ZfJ8as86rgvySuu6RT77WixWpUbIC2RYZihwr26shXW2vbLsGVmWRO9SsBXqBl5vd3mDa+Gj26L+1CXcbJ7A5GJzBeudJG3rEPz/0+sTueWlJ2QIomlN2Efb8qb2b2DIs58XwR/HcrmIai45swtEJPrqyglPRGE7nQQ1zn/V3ceCJ1MRYJv8n8SU8K5TWkmyUqlqFQ1OsX1ec9f6nWFYcczp1Ohq+7zGaysgPC2vHqFdOM8ZC7fqrQTmBeKD3n+HMNJlDTT2Wf7kUaiWja8M9L0Kvf8Vgx2oYR1oQZDiye3Tc7ysoV+efLRhviD2Yr+Ai9i84/Vbl3l8BHE5O6zY7Jqo3rJwmdEGjGtKS0QqFbo9Xl2UECEn2VzPBVzlxunucOVlV2de1y8a3ns7+DhW+oEGllhW54QGVKqm3K1/W/Tz5tOhagWPuU0p2hMTKUPuRF9pE7s1blK15hk97orkco8lszGnr9NTYW0JRu32GeUFfi7qaLLwD0vARMILEI6T6GGd1SAtTCXIbJfODLu+Ul9QNUMeuFiBKqL++pAgx6DVet0KUEbOlD7ITGsQF3fYtRIu97J+iOgS0jR9SZ/2CeptZjfprFC+sfJSWexEEUnnkeO0VxZYk2wlpTRl1YVc9ZkSB3We7HAAM3NhO69fK9FKLhpoKbA2bcyeuPkEKZNvWFjvd97NRFIAbm/mUm4NqQppECYa9XQtJSL39W759plDtzkvXf5el6JcqWYuBcM+g9Hb7/jYuJHvTJsiACDTUSmiBbOw1KGxHsL91tL9j14U53pGO5V8sSPKfE55ojOyw+lhr7X0+FrzoSQLt3+oGt/Whi60l/KP+FUsqlepZnVs1VbVPoF1tcFD8WPWfGzCKrDOmywlNM4l70bswLUWgeQHS10F/sD23PjfTGnpLeeZj3nzfr3NsWTQEK0TkXCkbXWQ2aTQ0NGGXEBA0gtLQAEWqWmRG8/Ec/kCNaa4goFIvo/wR73DhezkqvOtCqVWivJx8FuygkgNda2qqr2UpfuVK+ZqXZAM70UuU7DU58v40SMPBt32GlFiGbfEIPMQISlJwwrv5zxBA17qnfF67SFbyIA3Dz+N7f2M6jMMPW96ddMTOgGHLibQ/mXW7rrur1u5C9S/laTrKxn3utmi1+PhXM2UKAS6wv2m9UPykcRvRMlVNCZ+O1NQuJTw7HrJSvtBEdSD8mhtxfmtoyT5bFlYV4h9l+1VQUdIiPGdEZysTnl2E4i0i8f8a2F2lnNCGBHnkZLrWG35ZXDZ5DdAMfuVRm7pqFlM34vVQWgb+ZgwqwT1PRahX+Yw2Rwh7Uog4pmOcDArW8eqyJTioyXMzzNSfrEkAcTyZs+ptrAvCP+XOLE5PN9ERyknTmIxD0T6Nai5qxrw2wxWf/4yeZhRQatb0kDnifs2lYnGyJTLPJKzBbAE5AI69ZLQ+axGNs3NN8qTad/6ZE4KqgecXagEn4p33quUrjqyaIOwVJrHZGXotxQDSZ1ZLudx8M/pg4bovbQtV/94Q6Hnzi255SyK59YkGzdoTJMwuma/AwDRfW1XQtER6znVV1LQfee7ZsJHTJsT0/+6W9eNFPOY9+YNdNCXkfJizy8H3K9tZZVVn4ivaEPKbbSAFEbegDVvX37dRuMUHWFuE053tW1geYlicAmMbBd5mD9X2/XvsQtSHHvEbElFagck6KUjPyTVhHekUZW3Z5Gw0839WLF1xOJS8PjHszWCrtbffe+NurwnuV3c0exrStqgTQhEzUFPbR9+0Yle05lgQGiEli6aRcappyRikKWbGtQhzK0poXOmdE3j7II308YyI7wAgojbOHlE1AjYWVfeP8k7DTiGEkG7cx+4nEkm49bUDaPy6YYG9fWYxSsu39lKxWruF8nHRyEZtL9UJZnXV0DFo9QsVaR4EcXbyBDK7Wu0f+xoyr4zk9gM6lg4apRdQVg0Bh3ZKsN1PjhMlmtr2umDkA1S4VtRMwWjNUOkRnk0cH84UeheayKSwEfjUMcZsk/Vik8n/CJwye9SZUz0qhh+w7RuVxpgzuBhzTTSsEDi+T6SINeZAT2HrZS0bBpcVckGhHFzPzF/NWuyhSDf1wpddAnp1QQ6NZ2ceGZsNezE2dNbeF54137qHoWYI7e5l5VmuvHDaY8aYNXkbwNYYPMFZL5ym+t2DQNocWw4NF3FE7fSPxuS7eyjudnqZFDMDnig7Ch1s56y/rWnzYMXXoAlCGEhZEZdsQgjo8rPZ5UJBe/J8UOWCHxRTI+S4+gRejd2ehODIRrF3+PKaTbkMvfojkjLDIkGZOo4LzhRwmCCWqw6wZYD+lXTQj/h1YZqt+SIzUliQHxESMWrXrEEQseB/lvf4U/+f4vLYhwj9L87w8otRVpQszHOdtyXyCfHcPAAKYBNNS8bB3pVaw8uj/Lu5N7ntin760UNAcmCUA5IFPD5qxKc+JTqD+3kHo7dZtv9EL7WjoNCE14nKbAv5AbKwv2DY+6QBKQi7BvQZZm8O24AZy8pz2mfODPmAYxWsJ4wdfdAiHcQJo0wHJa+akVu7YJrJ7xX2c/IvNxEqAhcttu4dJEGCBwcN+N0RzVwdvYXqX3TSJ9AtjxmuoW8v44bhuCVQUvatdU+H6G1GGBGcAUhQnObwusqkHRAePj6l+5mqSySWpLedQEowcY7im9Orshqa6+zZLuhCX9zNQZzaMmxSl5clGNGjmVzdfl95CZ6FBFa7lWWM5uIqGE4zct3t7NeIYmoRbtUjJzFnx6JGKuU6HfuPmXEggBZLUIAwqqkmKrG7YqoyEn+o8eeN7n1Y0ZvR9lL5Znrp2hMXo++dIqYB68d7rMN3kKII1+Tb+22mw+RbBpeSeORbqE/6dDDzEESUctqXWtf0dgzmAsYmnKA9rXlnby8OkH8uCfrupJPjdm/XjUNRYCeEkDCzrCk6skLN1do6jxlQcPd1LApAcJeknUwJorrqKRpjfrlM8ijkWr5kD1+kvzAeAKRsi9zda+Wp11s2xCtRCywQnhoWMN6yB5HyKeE/YNLCV+SkDfnjc5X3wevTfjXRqbNha9jTIaXGlKOfa3yBHffW0/UpSFyYJLizxUJz43Bndi33T8UmhqqZQoMuSXKbWPoFHpiXFIm0mJQNRuAcuSxNgGIceVR1blpJVEVelGlBN2JgSYkSc/XLaSyoO5iVQQ2dgb02VVK7/gHfq7qJ2Cq+U60AER8POIxhDGpjSiSzUC1FuzobeMGrieVyFH+5d0n0xtQT9wOeJIrvFvrXQ7qi0CzQfBAASbgTQf6WVL/4Qxt9NYJNZKFhNR+accnv60qtTWCviMVERWgQaaG0c/g2t6YoQL4pRnHXV4+xFS/64mFu4JASMHYgVl+xkIykNBeqjJdxIno2dcQKtYmPdiKEuMSF/x/PhlShiveGENfzX5RJgjnHg3w0wbH4YLn/a7+DkhcJ2Vk/IHeG3uhDy80Z8QKh/j0bvDEMHUXQzykQ4Dgc3yTCDFVPtUL0QKt/hZYYLOHREg9H0+Wsle06r9wy12RanDFT5Qfz6Ov8vPA2akqELQqzwgqoQE77QoLqrSZRtqng0oWRt5Fvrt2GOKG2veulKksMrtsBsm04bPRz80UXhHLWve/DwtNC6Ne72m3Pm0dQDjYfSqw08bOjfNFEcRVlHJjIMs3dVt5NVONKQ/UuuJerap/HC3SuRHPR0ll/Vv7ZxnKx6n0JThoZJg9spE75CzZAoFpl6lP8xGPFl6aOuepmIx2kU2lVa1d7Q4PwYbvic7uxtD1pweDiV1Dh3/+9Nw3uX1h7sUCliCjsQ9YVbQz1w7kscdQbLjmVUolUJPoZZiLY2tSDHPvz93l0SH2zlg0zkDaZPT+aFyEZaGVi67emv8aUL0x8bhASTXfnqbDQojzu/qyhg+1FdTdzrr58j8IaWkigWQOegHh1nTurhQz6YNpiihLW9Oys4vLy4JmRwNGq+ElxLATT+gN/vupugqa1ed9ZCTdtycGfAG+sDKc4xm4NuwzYLbkkPsu6apeGJI/aI9R882O7ZQmzhrwgHd8hO5CUDsP1GguS2aDJelAruJX7bLBCFn0+mVNGQcUSr/lkR9ietFHGzSAfWNEUs7mAD4BiXNDrrWC1yfEm3dOBvgTUZ6QA+A08TTkrgg9rQDNOfdJUuaq0DfghMxM/R3g2UGJijwAy3SKqrwqgrBYilIKjIiZU2NWxITS5IOjKnDaUJM5nsLGUgOs4DjUBUEXrph9WXGm9R5Bm/DJUrqfql1oWp5leUQDhDJZLS4NxmxB485kCKwCq/DDzIZB0X16GTC5JvVIIp3xOX/4q7JvvDAXTNaYX9RXZwlnseG2aTgVAH1ZRJn7vCtugH3YBAOHwvXi1q+nah/eHQnLhT++9Lm5khnsXM+w7GgoGm+Fx5KFIqYGttmNucHyrwKki8NfUXLUnmxKeSL7f9TMKjvZ7KxZ+emqXG/72nxgK66aNISl34WQgP9uAk/sXNsQNU9fSsY3SggS1JCv10Gi8JX77prG7l6NYdp2+1zN/JMy0z72hw5L4vknZjR//7uwe1cBxTexyCoFeY+67hSCytzhBRwqG+KXwcTeGB5B8BvK/pCNcpiXoCjrVfapEV7RlQ+dcVaHy0HseIuXB57P9IjgyvuKyh13mMePH8hkXCooH7vK2SIqhZNEeUHplrN3qzKJjq8jaWJepbcEHyBtuyt4+FXpDoe0BBu32nTeqU1peyik8Po9NzF00dmGDJfWVYU+yxQ2BklpuZVwfdbG0wsdVZfKZYSoGTwfSUA1C8jk5X6N+xsYwqdUrDoLkTJuTVYZ/dnwFLvadHkMtlzzV9zrK4G+y2YPOkzOUHPdS/w4h+p8J1aJ0GsTydYK4yj2Dl/1EyXzgjZMO65yO/wcFcGtnvwyfRnfD9hW3h9VjpFNWCvqNBabZkVwN2oHSRGn81XTGMDqNvy3vUnELp7F68cGjajl5mXkrn79qQI2aDo/DubgVvQjLwf8b6f1TcLWlm0zDJn1rfk65fA9zrf2EFAy1UNPoZYTp+BcRp5pRaTJobSBkkt8Hjfmcboa3Hl49aV5jVSiVBTcKAMCJ6erg0J2XYWBwjmpxU8yKe398pGmAaBbQjy1GxbEkzueJeedwmNmatJ2aSmMhNbPlz3+fY17lI9J7PmZpZsIuo6f8txZjImL1U7EYXzgnDvbCYfXP1/eCQnSl08WpNNklbFpZJLgZtaaJkJmWu2alWXx/PwL80XaVJLYI5neHXfwBgqy4BsVbdIJkZISosbRPthxWEVS0pkJWg4Nr1L9kPfGA0qOBkfNxSVFgveYpCLLpIMOQwptvqFLF3LjhKQ3oFIGTzMSqp4Bkj3CrERjSi4XmaJ0EYU+dts0PDk33++ntAoN+qWa5vih4HrYhpaOd8dTfIgYD2b4d6oEZFSlQ0zIiz187XugjxFSwMEVpZq+YGIgu2BsP1jjuqoF6CTZiSdUCnjxNGDSYhQESZkZWcQAOhwyZC/jO4P5etW/R7pOW37OHdCEsvgFMYiF6SmeqGrHsm5ZeddygnT77kISGFNlqxsQhR59IHw0A5mEJS8ZE9gD4UNiSKbNyP5/3gpqryuNfC2fUOEAlXG8Z/6WkEklYs4roxThDARzP5wAS68wVb4Nrn/TOWF3Sl5XRzqVpkL1N3ykG1YbjqClex5ePuiYLrHuOpIx1WJkK1jjNz/C7kkg6zWYYt52I9taTml76eKlZ5prHNbFXL7xNsifZqI2jKoJcmx2kUVxH0y4NsFBsGoRyheil1njWqsjs8mOoNRLRgYB30jRrgb2+kQXYpjcM7nK9sRd5qvi2mOJ8bPH2fsDEI5bvqjEB78fPHgWDgOI8/D3K0jfYDDsqr0hAVagitGPHxFJuwde+cH1h2eogTfKlUaU6jRVVYx9sy1+ZwmHTMXyfgZGe+MT1ecHyM1bXC6CnRdVuAxQxwc24KaSL7Jz5u7623NSXkaic9CFw3Nh+5R4cHlP62fW0r8vuBBvUEYxcwYvl+mP4hAL4dWa6IE1Ww+tpwKv+hTbIuAwOVeYC0mM/Aq1a+cWaMopdbhYsBBmoAxJ3ZE+ZrTdRD5CkvtV0Re1v0oENjOzDtZHfW3S3Q275SNhnmnV3F9+0BOiiI0F1EnJ/vGaFQ/qCiKnSqOcTW1Moxzjlk26cMeH6NLmOcgp5F3cnmVS7Oscx5i0z1y9D5rmmUhvyeG70UnGNskQFdYoQrgwuLS8NM6xuLA01DSLEHChNUfqHmt3H8J27vne790G2DzwXeuAiw94mOfrQ2lgy2ujcOjW5+Z5Svxl8N7Ir/osT3XCPIszxyaJBMlrZphWBwhKJR6JmCyIlKjcpqvtkUK5dVlgTPpvhmwLIzrvbxkkgHa+nZNl13QTXC14pHhIFu5qyzzvYp2IPHcaabGdjFYNq9LW0GJ8/w82sv3PGaPWgtq2ps3KRdW9TOZtzA9AGXisubaSueLA80UB+O/WtjPxvcaajV604FvRAufXbjxsAN7N+uVV27l4qW3xhDxjILBOTxXoFBMWENvKeZu1Ay6Rj5WCf6n384+F0uflc1V9wyQUwHaPD4a7O4BxtoSXelKeJFF2s60ucEQuRUHViNBd7pHFrZCB+ECcmJwaA8VX/+X3R5M9MkuWb+dXRz7vt9Eg2yrqYSpVLoaeBpVdfSiUTzho8w6+8dpqbRAiRKnT9irx6HfK1AOhZk1s4cF9NG0lkoVYtXP713dytvmtfVPwSnFlD6HUz/993aTpcwnJsWT73Bf6cRpDb32sMsDGUuAsjvyJrVQlrvUk2x4Zl5VGkOD0DGDiiY9nXVMxI3MkaGI9cB29K19CJyM5Jje6jFXnb3gO1OSxD7upPZLx2CRemLTvLRxW5RF8ILNUV6xidxkAhrN6/CSgpO908buzP4lIfhsRXZ3Q58cLNp5XV6qpWfAAWQ+0aqU6cfTAi5HUS1D6qEnhyX7tuRtteky9S7uuwjh0PMX9sJqDZOX3TtSHM9l9vA8S0WmBg/i1HM/CQ+sFlv3VCEigRcLylntiOZ2lN6H60sEiijNSkYIV0gBYwybufwaEbxeLTMCD7ZJHFrlEpO3nJMUD7Vj+7LoPMHmWegO93Id8ZZHoEpbhuNaJESdUmRUOJUamRm4WrBPCYHtoM5F9rcpatnuxnxhBLyLR18gjc3fFDM2mUDrXmx43IzPUyHcJBpiZ11GmuAE0pARLTPl2taJr+sSLBTEc8P0u0DYlIMRkEMjQfGDMDVKnTCFh9JW8r8wlQdgx8NamZ2T2nCvnNw5/G8qzvp/HvRVQAHQQ3kbKa6QTSysmAsTRhygKIRZjgLzDgHIaiRZCMWJ1wmuMbxPMrMPdb5o/MY1O99wfKMN/3PBYLANlM+j/13+cWMpAEbBhP91DnCjZiaCoCiGpWgeQYnTymJ1M3GPl0x9opx5bnJrCfeHKxv1yPQ9vAGFVGSK46lBxP819b7bdXgJpgCPPgnBR/ZZxDe9aLQmO/pLs24ETgqveTr+zHN5SOAhDW7IR19CfuAW0tWsaSdj9wIyOng3V/+zCJ+fxo7HXJshpmxOQ/+S7H+aCf3W+uodHmhfg4BuyDbfh357XJd4pNQKsaOCAwJNrSWYbzne6ZapNcmOyYxsu4hHQ6XoqbhDRb61exTSHY6kJ7K0W+poA+lCzsMMRKdqIboLBR+OlfxDHBt4MLcRbC8asitqWBYKA/SD5ckccW3gtDrw32v+Y93coja+exR4OBZokHjVs+mmCn9aDAn2Ukf8IcDKsATgbwEaHd4+DO3rmcXTRXbA/Lp6WWBGDPjA95M7s+eYes7TCpB5f4whgVVAYUxG97/oihWag28HQsSeCkSCSN4StEh1uXi+bld0gBA9bNW184vrjISYOdwrpRhPaiwNFz+IvjIdj0VyMM/4jINklWIKPXmOqp3X4yXZ1FYKYINIO0qZAVG8Y505A8fj1lMnVMFpW0cmzZKSsqGzm5fIqiKkZeDHm7RUYhGyugzFaGcTaj2Og+i2ndbppyqcglY5heJHNQT8aDVbjVUYWdYVkQSohO5iQq3UCS/7wtmtKiVEmf74yaFIJvXcNk4ML+It3N4wxv7m+BuO4R5WUpS4qiljFRjvt2SG3/jKlNv+w7WUwZbbtUaCIiydyBUBLZ16uxg2xY+rqh7D66jmA7ye/Mzhd5nQqlaWslH5kwD2oBg3gXCdlBLKrQ57jefC/iE+MJKoYGKBr2vjJiSJp61iOA+1iY/RYhZ/p/duRRWs3dGJMZquaNpP8M2n9CYNxp+kZ5aQvOmWx0ncrFVFk/uhaaxHMYGR8MKkKt2V21BW43BpiCFM0g30NzSiOY29ntOSbx7aIzJpxFMD8qAi1jimPO5mgToO5QyPGB0fqKpQWVXRIwokHKF3O2EmAq98VQakxkdcab0djaJCrZZh0FHVm/sz5PR2sCk6oPlBef85CGX9ID0screfc1uibrpKBVqljzbKhu19PFUZvOB1sJQiHamcOrcyLjiL+mc3OV4A09P/eCwf4z9ahhZIYmIAvXMNt/Id9++1psT5y1vxv44/Pal0kznGDI+TJB9cmUHceSf3YYHpBvM/dJXU+YItC9f0sOaAwHFZHiAkZyYt8hRfHQp/5PCxgP+V99/rL9I3X+znSeE6z/284e5/AEdfBB6E2R6bS/+/vcHxj6/n8Vc47Te1YWyNbxqHRYUwrD7UG+UNyJgulrr0LjZ/WrpeIZzRbt+YJaPNPSocbMejKCoKhfIfJXdpfs/Jr3qJ4lx8hwHjxi6Qj5tQPhOcbkcgk5UfL0JM3GG54HXnuzIIbBqUOD4PdYmkJzJjMMNz6bD4lxM/QuiWAEz9MJFfTLgIdvsIdY6WSvuBLh10cIw60IcKbdD6fZGi6NOZkhjWKkJor90QRiyNQ3wvjWstqQ1l+rWKKHCljmXV+dIg6CbLHU99n2RiZRrxLrkHIQ0ikejW93eiBNPVHk0FDy86GZec7JH/H2JcDkcY1pQzaiV8O0DkfUH8eOXRLW7Q/c0VzwdEoezh4xDbF3z+8CpV2g875bzxg/3hpcm1+Nm9OVLs7X7DUVmy76XyviBG2qtuNmUoMWFK2equukv52MkP01DZpnmPNI6eD0xyp78xLSg1MT/LxYavw2/UpkXJoU3Df+f+q7IvFokFn1ceNkNKb80opc0PPaQ78IAxe1mafekHW7kFKW9fDNdhV1SV+CizSDKVEcqEO5iWGtL3KJEvhqGKXICaBD2jZhMB7TAOiggFA5U6AApIGXz+ONzH3xubyn9Q7KPUGdJvIjenjwkamwkA0a0631yeBabbjINJmxrj3JhMH/hLBbuDyY68yQWaEvFxC0LhoAt4khM2Wy/W1hP/WgBON7BSQBba4EdVESEOWWpPyNC+dVyAvSxxJ7flLYT3t3N6cTwFLql5C5fzaX3g000cm/XSNC6V8wTuFtwvJG9UoeTYzO25TlUoxtq7oEs1zdQrNzLkQxG0DtsnTj0WKLA7WIwz9Q56wYwfEI+BQMUUefaihGMuYRQ7idOyJ9wQWpsaO5+t655wG8rL37cy1KQo4OWsG1NJrhv6Js7JHZ1X5F4keNsga+gT3tvR6nX1ZqBnD8NTrlOgtVsJJin8Xk4QEgzgknLyRYfInrqMb9D5EzVQEIdm966U2GRcD1rpwdKZbkVsKbaKrKRfCfFLBR36JcbaQk+0N+KybVmtnJH3XS8kNqiUmAgVZeDlISA+A2UgtB7ZHVMmfY4hij1pVydoQSXsHKVZQma0jcf0SarTRQYfMdDHXWBcUmDe9xaic1Xw03vgjsW0w81eqiuBZR2DxUOzb2UwVnN65AKgKhe/ZN90xqUuQYeizPofv81UyFlYuoPExnsa5VhvL6s7KOMmAUV9EJFyYszSj+dXEEDL46QxNXXB5WJZSwGqddbxMpZQd6LwJpSxYlYe6CmwYZnvAiqDv2bN+++P1m3ok9qW2gT5M+o2T/iZhWfr3n3FWfB/KLJjpcwvSU7phYPV86esgQVij1/Kk2AshxNx9fGebzfxO0ObiQbtMIt3gTmwJyIO2DsRojzcu0jnN1wHAjVTViwncRWC9+K+WNn0j+DgSZrDrBcy+/rboPQ9vXYXP0o1POvIpVDP5sa7cylgJLXJyUv+MnTBLegMZPIFaTv7VglRmkNHeTke2mV9fLPKPnuTWZoeK74FwThLsjepgbRc+Zzm6z/ABu9O5k2ok1w3+mnF7rgkOub64FKvjeJXLHqA86lfT5vKcA/N9Gvue6ippVzYGhQDzO/XqmMuCjmAhhAqLXMSLQPGHmY4jBNCbNrEb1J7u1gBzWHBsiVifkgtPdOV/L1kCwTJhT0byJk0/rwWnwNhBj2N5NVc8KowUWPJvIRKASOCDm2GmHA2Oel7cI1e70stIeFac2jme6S4rOsE4aYc/fi3SwuoTUOv+Gd6ssGXC/r/lNNvHUyi6p0ePVSx6RY/bincLs9OfdMdTkifd+wDxWnIQWtXBpCLgIv084RfwLDM9sxdUGMCZs09mKVZU+b1Ddvj+JnrSEFTfuyUlvxtPneU/bWMfjkJky3u4iFJXA9uTMTw4kzxlRFw7p7doVMCFlD/3WqSHYyM4XZY4HRXuWEw0wQLQmn8pjA1DOnzRHIhYs39AUZRRPc+tt8X5CUR454Klgrpnfbp66pebYlkTaONyOU0Z2VgV7wtuxVR63kOFX12oMVHHcE327pX/GyWYHqex5MLWejZix/mBhKx7M3TuoYadRxaqy0r75Cw8QyvwQAD/5RyzqEpwVV3nCmLnopfBoupH5r2xc2GCh4708w+qBKrwCepY2t3Vg2noqLvSUsqnwMOY9ifkNyDNQyOKV61mEo4blr/e1bZ1Cl1FQm5fcEDJMkZ2p56VgvBIkh4R+DdE1sXdkrgvfov/eVcnc9q3EmlPOW6LrgPGQnb6JFkp8VTf5w9rweP5cqE4BLAocujuwodEPWfnpCnQTYHKpSTpa97fg92VZiR23Om2aCTLHRGTemlEs5ZR6XHlo6utt++CyOjUzKQbA1kROKnUYmRys7affasxE8exHT/O0BNaLxWO+y5pDoz293K2Zt6pTaZGSk+VsWEKtfXvc6jEZgSv34sOX/J7bp5ulQNxztEWxN1uFGZ9MwcI40MAPlL0Mq5UBtxUoS2/BCFCDJCYFs3IxTJBjwnD2NAE6cbKLp89rM4WXG9JhPNwoYZRC4Tt7rGBYUbOA2AmqBSJUSjulNDUCcxxjy7gogm/D5noDmoPXDaFnrKB4txgrBwMvx9PYD8igeQSpGXvEgevnnFtFF5N8ML+J2JnklOOT6yd5KsGmaFdZl5AHKvv00DySkB3rJjv7Pw1sSsDF+G/wNe/96wUBRti3CR04+fROporB6QHBfQSThEKQM/22X1g+KQ6LpRNdPck5uZueDs95Sa20VamoHjlwM8kSywsYr5kqkcuYs+mFTCHoCKqCXRxF9zaTC8o2LoxcUNK0o4NFB7gXCX5qySHtxJornhmoMApVEI7dpp2JqDqIujiqR5c6NXCmMg5Hwk/3p/ui9yljyXnavM+ag7R9ax5lBRAefxQVTyT/4mpJkS4s/mLOKoZN6RYIWln6X3WnybCCFtrttNvOehb+geWFZ928BDNUILJwnYoaVL+BaggQorhZCJAtUQt07wH6B9LnaGqfbm+Us+qWJ7VBE9CYqxBhUe7CtmOJcCPWRllpOwWH0FFGt+VJqmvuvzTn4+wXwrXvUsCxcwNyi4NqZQugqMZMdCUyayOkhv7MfvbvbLZma2TUcA5BWmFJfCnXaa1WZpwcrVliuzkps7h4puY0yxxeR+ysWcssDCbU3bocZiyZt4THnGWqembu+e1+8lZ+QvI90bMjeFbmkO98z7Dt3Ry+A6y3bCyds78M3RnvSO89pOb+2b/nmoEGwH1TnGEKeaGfdDpXepQtdqGj6iNVwOwSVL9HWfJqbZqya2htU7Bbj/76zSc4PDt9Lrks7lMtbMx1L5nZQxbT75hBkNjWrgNl/kocLQBNuTyV4NaGJguBR0GEz7rO9zDciJa8B2DnZWNy/vo0qZSc8zB94I+ftSxWOzo7PLnbiv7sphEN59dakamFsWAyFRasP2hV9iY1/Lb2up5Lxw7/Sz1PZYcbzvW8KNVK/NrEFb1c3oJBVAEtZr5u6Wd2i60zGB8PP5wZlj+Ek3juR77FDkztQNl+4NRaryfL9LCTvOrJMiNRbj3aGMG7i0DBvFoaN0TYPx7KnDJy7786PNTuzgAoxyCcPSX/vZG6jKOC32Y4ija0OfCiCxYm0VkdDPodObdUVjbo02oWrJTYv9nYYgUuB0rufKl7fPvjkFx4cc3Hfx5eLWnKDPots4Un0PzsP6YDfxKXF4C07zumRO95lB+m1s/Ga75f4WIySSu0xntTru6FUnMWo9G8s7WeBHr+LsaBBhQYA6F0NXn53J0IYV0dJauf4YNDvPbHqcx6QYIf2ItRycgPr5z/kYvJlxHKN3iLiCXq5CxxfqlfUmEThILOY9+/ZekLjw2GdbYgkUJCfh/rT7jPY1PGuBESrumrP++I9lbzzvatq3JKN4n86CAAgQvqnAuf3lb6Ei9qtsNMre55cohmHQt/xfB0bz/jFysZ9b/hDXIYnzsFSJI0nwFl0DEEV98XGkA8kF8Y3MPnJy9kqIFzELRaAjmSWvW2JOpHoGGVOiBgiSVPVGH8/l8WAZpM3y42PZH5Gt9faSZVLG3HvCeoLIIYxHXk7EGAyb2NNkhso95elb4nTAN3L1vsO842JjqkHid3ZNvMXtk7smZ0FHIiMvGKeegYzta7Nq3hQBiiAupdDt/IF7Nt4LjRX+TDdS09KIPI3dYyFbn/yB/gJ9wc2O79iDArr+3tQCavPmYNRJFSRm0wSBrW6TkrYSpK37jWHX5O06W2hyqXoBLcA5vcALK5tC07Ze+vz/xWBPE2J6GYGmYxIWh4xQOVYWTGdOCG3wOn1uLrCKBZDB63B4fU1Vwgmu8LsO0wrUN83m/hkbBU9zFmyGFlB9WJVhXBEla7E5TplAnqb4+5k9X73mUKe/xL5fYt+c71y55TcDnYcIhAAhkU0tK4ZBgRn4uqq4dPC0zoz67XjcuI+CFbe984zp20Fs33QYl2G0dxZlWW9DQcWaJ1g+9g1PkBLF1fNfyuqpf6jQfBYzL9wAK/nyhXOrCmJd7vIXH8Wz9mGibnjFmimxS5utEWNCU5sMrC+tsUa2WD4QfCD/4H1MEbcCCybXG2FrvHNADALyHIkFbeuRu4NTB6dgE6BvBiwQ7ok8yd0bzUChoMI7NZItfa76DAY5XuN+E7eawp2OFgRTj2h3fcVLi+ZUtzgP7IJGFFKQPmafNwdzEgofWvoJGLxjFNvn0pJCsXMpbiQbml6Ap5QqAELYAnL4SwjMc5ynz4eyMbBvPoC/uJKHR5EHFVEFk6e+xbVccSAD0+GE3IobPvPhOsHoJigjzzuQbOhco1S0aDUUE65VUf42HBPk4JOqJrPe4270vOye+SbhLIrbWM1LKZU1ZD1tOVtAdc61o0+ieRV29Jn3+FbYf39DRqq4KJgV9TUeTB5vowVaEdKcbZMZ1RPywD7KLuttITpMg2ibRn829iZl21AJ6nh5PTTCOmAZ5h2JCPu1v71BGOsHEdG7Z5QLddE0IrpuPrRMR2YPTEWnUX7EpSJUNanQe6l2MAiADEJhTlJtBuTTVMA+TNXKgYi6+vtbSoJayJB/Rrsax+VXYyF4P2KBEo6sMG+fGfSOi1Js3Nh3fzJ6WB6qVrnLzXN6/sls8dI7OdqZYukuq/HyXA7t9Nr2xwenz4WvmZnQE++VocPKf3UzQKktcu1PO//L1R3K8jlG2vmvYJhy54oCyX+vLgN2tCN6DCbp+KWxMofISXWHAiM1SUG0A5rmxxpK4ZPdEbmsO6Uz7/O15830YpmcqKZI8nA7+9AQx3bjeePyoAMf/BusnjUeZn4YDVKehmaX4xcvBgb1Ipf+dJC0CRpoQ/igeWHsQZ6Nzm+OYhURapQIJP6r6IVQFtKkLUrHm1qu7Q1zIbsEYJCdYQcB39q9yhNZonQKnuvWleQ1bBXDEgChA9z1eOXZnMVLVRRFXBSHX1/fO4bcwf78qxBC6uXGh0x41rwYlVslpcCY17xykn3VWQ+LGe0G7nucR6ULXgIsSZTUW5vFLx7u6LmQAye78828Y8FrMUPGea2xfTadg/DWpt0BPNMu/aGmV9GYamO/2xP8RhX/GmMlCtcV3fKUg7RzlxEKqMJQAC2TQH/YoUNhv6Q3bu2KsyywdxU/+ZOkeZiC8mceCVCnZ+WlVi4STrTG3WhulQvkJc5U8Q+bPtmI/2irLxA7IiVA1rgNjSCw+94IK4pjhoaCWR0ryIxpkHTW1oZMj9h1d2z/jJ+/9OdoiArYTgj39XC7v33gQ/zamJviufjZ3da+7iAvdueXuyS2AEIT13rhJwwPlH4hraOeqb3Wtx0HfLGgbQGuQIZQ4+za+OLm4/M6sS1OfwTu2E7qDLtcU+Hg+rPzFYs+OG133OukcI9Sv2SboX50XSYWsuWOaMMf+skQp+nB9CUozXmrw1qegolQ50MLG2hJWQOKKqeNr0ZmzFxp1qQe54q4Ttw0bMxbAHMMIHXDGskFq4YWRsQk/vzflYEH2/w1EBM+lQoT0OIVjiy5F674DY2jbYe0xA6ExmuiVbQimNM0LuhVxmMSqhdXiRWoYWFJ0681PzH7ZEzH17kLosmy7hf0wiCJQM+70Njh+GNJS+0vPr6RRqYImQ5T+bZ+esaBrcfwK6lFpe5X6/sAOUf3+mtaj2UCUlh2QKmFnwL6E2QKLqPdYic1ZwN/4T4gyNV1O8bR6rNygKb29tuPcuWyj5tdxY4xG2cUwggM1PSFjEWY3bXobu2wLtLDJ1xVVbdVl+AECtFFDxxCWeDnCSUJtPqYfVovlQNPqJXpq1ctDqDV2uWy8/6xbZAE6MWRsp5fI1Od6NxWXNschOswh4/eJaJ9m1vLz6R5p7zcZczf1PMlVBscGA42QARKpZN1j1RqAYJ1Q4gCqR6VxJQyYz7C/YIW5oh1tna66yTZN2XRHIzuJg//ml4Hq2Ci5ir0TKOQNVYQ73+Q5LAJFlXGndKFErA9hXCAMR5BAJeEyU4QPPKMVTS4pLZCyIXAntDaP/MST0Oa2E2KVhidfQULz4cN5L+jyHOKRbCruD30QOcuLxxLT8iV5WZgUf3KvVlWJquLPXEUXx7EABBffAEMwL0PLT6j9wiShiafflD0ehjuiw4OjHRY1XYz09dfMx1DVBGCUi0UIqpOSCFNctXr0fLaBqZggzks9Y/NUnADdaaCL3luT27Nnh3viWQcd0DYYIW94Wigip5mTJxksrglygieFdS6e9Oho08teOPXfYsOrv8SxHRPwZ4OYxs2P/rwfjM+X2SDxrxqW2XPv9rKlLUYWTBIfv9rDC0wClajTZ4rUZVVC6g7isfb3YldFPGjPkP1DA7z/C+99WcKkidlN4203PcNvYb4rbDg9aSw6PfdQOtD05uJcQmGuHCsddKOOSIduMfsUrjUfOSVTCc7tFl7+ODyziXORGfCgcZpiYQnqIxjxs7kKrPB5wHrsCIJ/YRMgvWXFQqDNpXNhz2F3MQCNV0qfdeaFoiliGx9/C/kFVRmVcvQNQCn/p8iPnZ49/bazWAJ+Tbh0kAb03BYkddOo5BHwGeMbgLV0s3V81cElMZbA2MhZfDpm4FQJQ60ca3C3ztX8xrDm1k/b4qpDIsmufX2wtILN8EtEPOsILLdFedaeqND2SSc8yhN24tuk/8N2BJF0/KrYweu6fh+CX2DrFf1CPCfptD83AzlpyV93gXBeWtCSSsfDlasSn7nm91fvoD6jBUiKQY4ZSW4wiWjjuumrAR0jBlyoOHXgnw+423szH5j94NM3HWZlMw7rcvnDsABm3cfnJhDgwngRye+t3ys+KdaqSlP/kfDVolaG7gdA1gJ3KrBuPk+Yx84tGN+7h6EhiB7iPjcaEnX2B8F+KvqWNiCQvuHrC/BA6QgCCNV0qryQkqLrrc9U4hSe+lXZOPFQMHKVufAiaFrFAkM89Z6eQE5yalz3/AoSDRoijtZ+5Wrj67L0ntduuWKUWhxB3jsvBOm3bbn6q3fuN0rrzKVXtojUPgZt/ldqsQWcZB57go095mkVrSVKocdmOMafL+/7wDEzxOXL978Vs8R81l1uVU2GhcCVVs3g4elvijMgLgB375yKHWD0eQiBmW4yaMUWOdVBDup1WfkOZO4h7QvjiMlrWvF1bZyTWL3L4cWb70+SWidyEwdkWr0Tzki771vcbBH4vL6p92j7I/Jh0IVZWP+zTKrjpmbGOQ5kxqim9YRUahaCncpFx7rNDBnQFPb/TLNJ+ZzlWABp4VEV2ckMh3jZks1dGj6licNtQZN2TlZ50sieeMY9+iMbYifRfu5rHVhPAeXsmRcx11qrN3fBwZUk1CpAe52oOi5RqKEbkyWHMoXqqlmSBULN9p4BU8GaAtPY2intka03DzqrGlOmbCrz8DSvktjGPW/bLlJNuk5YuLdwnKj+3EljU4+Btu8CqYWl75sBEwhVlrfh+ok+3ARJbVcnfDdlyy/6x3zpcux5pIfkcVI6EWbagMWP7XT4nIVpLPMJ7D+5ypduhFOEPlACWlkR/5F9qN3snp7bOy2b93etBSufj88wMGnaI/x3bCm9tCjUGRcz7vbDbIaaGbLE81IIWVYiggFoYdJyn1Qd6t7F9lRd3qHqEWEz8dSp3RGqhTDwMpd5QjDf5G59uvAtwy8MXlryGUEp12ruS4FCxm9o6rnUkKVsYvkvnrAM/FQvbX/tN4CNIsw/IUMrv1n7MxEiPtFnPB5mLt5kCA+fvFKXCdvDM13Zeqhb9+qs7eoiQzPFp6wtYq7kWudst/0/orlArr2BDwRiwmznYs3yqm8yhmAvE7MVE00AAjXAZAf9XHtXBSY55JigfWvqGKvVegNLJIKYOiZWSJVqLBQh3qcVEhAJp2LNmoogCeZ9iMOGzbgNYrQCL8JenEeZEyyfLNHhZwHkKk7bHRf3hERaA1E8rvgJ2JS/24oyyf6B2eLW/ph6ssIvujv7Ct1zNtBuSntVmdW409Ad9vGm2kKH684FD58q7Qe5sksIFvC61RyoHaDLXdiqhOvNns7WvJ44xERY4OgGKje2uHtIalcsaoGk80qvDv6QvK/yMApLaNtnyAdLaMnYetiDkHuWmjZkRE97MLAqYNtfpZ9gfhfx1j3eUX30vOF40w3/is6u/Q9VtECdgyMf0p89eO/RPxzAUwz9acyf85y+ixOOLzJ3/XOD1UzAEMxTawgllVNi/glIB3G6VFC+Le/rfNaabH+EB3CuSKwRfjT4yUJEXhegY7Raql4qWY8v+TpUtQv5i0jUjBelRF13jpFccIhHkaXUz2pXELgah6QexIRIeCgvYuaMoF9X+3agdm3HVARdZecz4hz3HeEZUPKAlkqobtOUe5wxZg3Z3qt/aWgRSxXGjrvJafCWQffKDJGzfxGIx0dSU0/muSAk3rutb36gPDFkPc7dDvfn9PtkO9+u5PXRa6h0ebKVRU0V/l6oD2cYZSqhU3sYj4px4eM9sQ8yGxCCsA1lWv3INtmf/ddbBupkYE0ds2c8/rKwDXRTFs3sRYxe6E5qzhj9PdndosEY/t/6YrfkgNDfijlgvNz8Hi+drAMELBblNJt680cEXa4AM8qv4fDUWgISnv4j7z8G8Enb+ZIl4etNi2uf4d+O0ewNrewfedv4i79rVJcDp5qUIwYx9jyGQnnqHuNHgjDj4/pDvwC49scVx917Gs7xrGBv61TJD5ePTTJONnA7wL8jxun31dETowmhoROd0GOUICBKEBAAEK9gmrebULYemSaEmnSrZucRHl5MkFsk0txZTPENSOeZiBObzVvYvJ1Ey+TxYAYyLUuiMb8Bc0hlB3mufEZzJy5GgJR2HLovfjHXkiWPfLu19h92CGqhL5x8VaEpkEnwaYegS/upgKXel0NBOyOC5gpPK1mWafRX1gtuUe/efrnfp1yPRzXhLfQlmEDcQRdFHUYN2iFgbCVEp4SPUbmHxmKc110bTJWQ2Tqq+jrT04qXvRIe5YEV1AAujWaDgPkwNxUaXRytXPdAXd2tX0wr3FuIdLqMTdAuh1M3Eth2/QqOS/oeUV2oMoqoGapaJqJG7iPqklDcOWn8m6hA8zhU0Iqpe+QyAEg4EQbEMUYFCcwyE51AxoJbHQn9Q0SSydidg/Ls9dAkdYid4vG4X+/XwDhOrRmhqSdXxx5aBNT7SYA+wZzDYBepGPzbO2EAXNJc6bgXrOaQR+Cr7zGPmN2m7fq5K6nxIlfyZD4C1mTjqF7wyWaICHq8TVVSZiaFjXjeTkZOYwf5xmu67JQCZMYgg+ab0SxrD0mBlGa6bVViUI2Ufkf6G/oYgX/C59yGIA3r/l7kcWVQdZOKBxwA+xEc73q6Z8+pplcfh6zNZoLoqb9ZmLzrvIvBj+dEBag9u4bXI5bCWgED9gQtuPZp5ZI0AfRXVzQm71+W8DtuJ5n3koHNfOIRPA3NN2ZTQLggtBYm1Wmsxj7zwExF03oeSSD9guZIb7GpPKXs0XzDReRnRoKkL8XzWf7GnV2W5BXiyPoB64CjX5TELxe5yy6Jhn7zxvybtAzSIxsJ1/G9HA/kK6o5jldpZOaVBDjgzKJ7kKK27MVMD3/pEoHFiDwOoBeiGuLJO5s+AuqaXzU5uGCJ5NXtpKH4TG7jcNcgOBjYC9xRhAyLMsCbJkeYoOJoRlhcXV89pJeAFLqh7DJ4nP1iphG+Il2pD+VP+EaV4qAbOmN0vQyXUo35nfEpvSe3xLgAAPE91fwG4r/buNDJ/rlCTIjaRifc+Ppa8gEbKcBfPMO5VsHHyXI06tMfk2tzc+p4WimpoL3/YMged9rID5oFqUFPfLHyhBopcYDmNBSYOjvOcpP3j61iThin1x0otBgS4eYQ6bp0xePj5J+7pdc2cQYivDJjOT5fybKPg+U7x+JbrB0QKsjQx1W2gcNgJa1YJISLhuom+ft+oBsgKAgOytl5urw3g6lEZbTC/Dnd09egAgAGRI7yhQ3Vv5LhgWouCxmlh12jhIjDXr1XarC3DRxVsintOaBYzTjBmQ48+YdyquaUpmm/B/J9P4mR2pUWEkcxWv8lPAz/4UaCToCvi3/YZ273OhlxwZMJm2FpIr0KY+RaaLY6F9KMk9sRcUty9UbSnZ3eV4cyTVw+q5JOoByk9DhR4tHS+J4uDUuEFKtt7E2/r0cafHMrT+0udwJ0y4KC44+Y6JsKdUXGUiFy899MVISWYGrxCzDClbEP6hSe7koZXdtTNRj45901tyW1G/0IxBIQrPSdNJkMIxeASy+BayUWKL2b0LrOmsazOAS48k4AumbbcLxbWWyyVnxKSSfdZX2BkuuqDCm7keVbNWA8XK0s+gtIs7RDWoSqzZmGZN5EcE7auiSGxWaiE83LHFRIIuagDZb9G214OTE4SrZKhhrCUtfoQCEGCqgbCgwyZKCzoZLAjxqpz9EB/s3tRfZEo379PhJ6dbvWSmFeD4HogDUcsowadwbxAOjpRk56R86y032mmrojlX15lHJbZ9xxIokb60mirkHiuqdDklg0JmVu0FujTPZVFFyg9b4TNWPfz/n9TyY/b1DQEWqoomx3+AivlKTezzI3V/JZqwh723bV2kC5JJ2kyjeexaEGoSx7yARi4EDZn6j6bkVcbqDyZ/noRNPdR//Jb87RnhEsVxqCojoUGY2HjC4Cuqg8YfnoGqfAFznj1moAoztNHHO6vsrui/ZL+PXLwfV6H0KZcMdz340yX7p2Zx/6DnRn7sjjZK27947LRk8ZtwWp1qIAf3EavDkN0nLtKROtlitbp9h0znzkOsVkrri1QGkuJ+/WqNQXCt4xYyIKbmrgbWf17rlouCFPTdGtBVFNQhCVmNwjq+XqCGW+ymV/UlgrEWkwSstlwOPa8+oOMEIrIVCKCzAYudX4sVvVOyHY9RzYcpScLT/v3f7abL5Mr0n9vVvD3YOQhIyAX/GZ+5Nho8BlC7ew3zlxojQ2qcykyyYB75GoajIsBZB+jdBKPhd2pEx6XS/x79wK/ARIQzeW00x8d6F/fORvXDmJ5UdWL9aoXweD8X0H/Cd5+Bh4esD1HHmPb2P8Rr3VP4sNlVvZ16HVKb+f+kxAHLXEL/vdzz0zz9FJy+JUZFOCUAzK61mnFBnff68bFnTAcYb0pVJXUz0ntPCiWwUVSWFt5qffXPjFhbiH8BkVNWEWH5bxaQxexf0yCWOrMW3H3P8vjniBIKHlTZRqwNcC7torbzTZq0jrnAjlRcGAsW4Cma5DYAuMhiKydGJFdyFATzDbFlAoh5HJO/0eJp1Z3FozkPpsZVgJ7P9xAAG+kTEF/WtpGBFRRPi//nUItQbbm3NP+8wz004tiiinjZAYbOH02YLxaWQk2Ow+vliPF/al8KJE1bHL2gmEg74PsLD86kBKuF2UgOy9/Ye6Vc8MxeKPwTlFFdwBSnmRfBBYh61k3Bznm2J14uABkSoBmangPlfVtrC2hkilJCs0O9p+NC25B8ARP2AxqGhdlHGKrE42KuPW9VKF4GQVAm4Y+1Ra8SJs5J3AASUIf7bWNmEJ+UN0tPWv4OPCDihw74Yidj45/0dXpbkBx9BlOelaP6Yw7apS94OBZ3VjIzljkdzR06o/lsxnrN4ZKjVE0FI0EbcObEBE9UKkX4zCNENBfwAW4RHtwxb/RgH3f0BdRaPxZsL1IxSDkcJ8KBlDFx+IX/e9szxahszQSyMZmGL9+QP914vNg/CmZHWmiybkEkaEpnE9rdBD5drYBYh5a4KZx5YklC80qK117vE22rHubakmfaJNykN94gGm7iBb9WrDtSQKqzxR1ZbsMMiffkU2aNl9raVT1k5YoLf3cO3qL4nmvQ5+4FblboYIIV22uexWwHWreIUblm80azQDfzUSnUXs7xbcQGQRVX0zfOGXRRSbibuSsZFHXLk70sN9Z+u+J6P9Jg65jGbCbzDZBjzQ8Cqy59dfHTg/p8i4Su2RznWCuLD/aK/J7PFgc12eLIuAwrpqr2S8HgCA9O+t2gV5cTQOiZO6H0YHqSYIzf/t5Y+L0MLNv2MZGurdywujnghUzYgyud0KUDDbBzRBM+XXVWAqd08OtlOnl6zgS2+UVOYEKDMhG/77Ge7dn6zHOJMN24enOiSPoSV194EbamLf67LMabkqgXXKUiEeAN+K+omngSfmNwDQckNlP7gVMbr5OIbfL+RRguJrZ/SzAlPS1HZydW8I30+grSxQn5r1vq23H31BPVjkahMloWZtg9Vo3FUWvlMCdxNdQHnACQSw59GBSqoE2wnDZwPWtCFsEr92dk7RhIJm16kFCITkAUYvfTGeo2K1vbyeOThONK7hH2y4Xn74DEpqLxsyd0otOaBmSktkuDTJHT/yv5vue5podYssFcjEIl2SiKgBXo6D2SJEqKo2E5FqTKkCVR76ymVHF81aA8XfC6QKqpHEqxxRhn6EfTw2G+1C3cH4CBRgiu0Ze1qG3ao+803wmBUn9nEbM56YS5ZgNCziJxyaVTCJwFuGCeahs9BSrZwC+BFMlJ3YiuhDhoq0mkB701B1DFxfGPuK77NVyvIK9HH/tpJRPkLh+tujEjrlKwS9LECKLtNoVfbzxFDExxPBQbzi7kAV1rPxbNlefF5OwtlNjpbJ/sbZhpV7HaYZZ8/zNokuvOPTYDj+oWhyNTgJsiMn4sURXL/9mEsdyWQRgItrHgwgy4hdqewIfXwxkyYUbacz6KopOpbJcrjjOgKTUX+RuLlVIEagJM4JL2fhfFsIlmB+WGY5l4Xr09H6D9bqj72wdyu0PaEfBIbo6/Z059KT7FO9fLDoRz7qFR/7I2JDkq1XatnzG9pkbyEJjRKJ7OeVCk3wJlmPTPXBMpspbnCQPNQ2OiQ9edO2gU496SyfxQNC3A33kaZf7SodvJdZ22HepTGsKvJFUzr1GoZL+//446VS65fZpamxRUKadILQAfMnLhp2ABrss8qMdrVlqyPFaJJ0UOeG9yAVz//J0RNGQ04ZDdDpu6rLj1Vh7NoaBPSIGj5BfxMjjRjHDUz/NVIVXY+jO5mFRh2KZ9AWsMc3RPQ5MTsy/sl1FxemyOAbmOcpzZL28vNAgQbqbuWBxNtQ5RhtUpQG/2d1ulgRdzoPwIBioyuC6CoL3psMe9xVuysYByTY1GuNcPBEiOuooQX8EHPg0Op5RZ9DBjzZVOeX8hXtnlA5qA2CoruNFOBNgg18LaHWprmWKPunRX0CEluf5kkkuaUfiYD5Pkghvj6mJiIjcAjqRYboDj4TnRllwiO49OldfYW+pyQ2uoRbmf3FlJXkus5Y9K3QSm7w0DFUibAVWEeFFaghtspCWlIYaN31dy45W5B2yStnySmz611tg6Vajqb48qwbszmu+hwStw62gr1DnkYYv2O2/5h/rdm5lx1DZGSPMfhwWLKjnDyUzoJau+mmR8ILC9JXdsvhYXf9ZYNVfKfO9P8BDWzgpOJ6G5UassgIySjo/6UTy+o562jYLUP1wjQXv6SMSNfiwZryJl9hPpk+RTfbZ1FuA9JfR3pyMTXrdjPQOT5vJw8dB9H1HA9m5yePU/DeX/4IW3bfP5tOr3dSGWBCbwtqKUwnmGtcd1B2FWe+F7xGTJ5GdkX0yjfXejNgatbnmhrBIOWjZjqRjOFvMXV3GigTdRH7q878XASvyHJlPfMi1bm3bLeD8LVl04Qgrr9I9HoTpNVfyfI0JSYjhegO7zpn7Bi0V68IxJgCH90TOqhQ+Zdq6cAsGm47n8fDnZWZ19P0Chw9wc0M2uHCBGBNWVManNt+wkXUDQ+D0F9T4XJkK0j7sKI+9ACJG0rqoimA+snePdZooMjRJaKis+jZDnfj1bnre+QVdxVAP8DWZ9wRkIzJ8GTkBs58sooaiYXjRobRvMntxREG1YpcBDQGY2epic/WfZgXyhneIXBg9aqHoU5Wll2a0TvjmER+uTmbyM+cNcnGDM+SDv28iMawYaXncCQgzJagxk55hsuqKz9Ej4p9yUH9mQINfHPshAKvNIgYSE54IhAdnyzxKWCg3Gnvq9f19XpNB6Wmx98x9ek/4iCra7oRhPEIfrsS1ISlu0011khIN+yc292lgkBQsfG2LyltMYab3U6UzlqWySkPLSrxw6ReOrsB5XVpXkVV3uARNfdJ7ZjjrOOfZrmG0uCNSGoRDRCtk+FBqDU/IEHn8QC8U7PVdSORHyRTnwjKoIyNCp6KQuuaQ9Rg0zw3Z1rfwbS5PxjGoI5FkWbmT5W0i/vEvUxQg9Wn4DGlY7ciA9eqRrT0iuZMhOat7AcHQ+lZsiDju5+0/lO9uiUNzJUtFtFqwHtUjllXRPpBiB/v2GeA6ftdQbLFXJHLAHm34QpnbX6gc+DhGFB8Sc3Vxj71zs+nplCK8XM+gnP5h/uKUc6ubY+cOBmnJY9ssTZFUlpljD6seaknlYD0NT3huD4hpRHYhU8pQ6mn9qRsSm5l9rrKyLBEctmTGa8ehYiPIJStZLoREYoi6sv2wb4/iBVncNgFo6cwHFRolcDLLCwbdpZMPqCcGQj9jeRx8+ycHoD5ZFvhy3LglXd/ZIoT1JFZxjM/dkBhNqqcT9q2MpCeXbfEkN9xfU0zbiZX1sps1jS/PXweIr6WDi1W2e+/9VtOgyOcukvXTXU2jqCSNp3maOuM2IvY5KIRSzuL6GRS4RZSoenQj4B7RDM6u0xqk7iJpXFmvji4ZTopeHsyobi1s8F2qsH746xHk45X1EmmsVGVF4xZCEqPsFKDOzfQ0M/PdD7tjoozDikwynxikRJMu9u06UD3NTUZE5WLuGEn8tWOHVVsnQjjhoiNXx2HG/4dj9YnL/fsvKD3LhOVkoCEHRxZiy/PLK8eQ0rCs2Pu43r/SvADiU38xAH1QTl46rCkR9+BSSFx97kGmrUPRFXru+g6Fc/prxnxK9AuVZhv3x9iY2av+jyqoPwqb90jqbkcgc1Y2fAQkmfTJgB6nqcd8fMMrz19/XbQY0bJOm+emkdMbTmSkLyRzsKZXUan+U4NTSUZy1PxPrC5eDLn599Qwc35sqesEEUx4MKEa4QIhJKgZzNjg+YnazB2yXCC4RefjaDC6tUpd1U91oBnVZdTYu54E1VG8glGxEfOJoOsYCIv0+klzsFsovdBbHiFtd0GEwNPfNEhXhDz5J3cV6W4W5mgqpj8lY7bX7H4g31P10pojMb/YFz5RM1YbBjy264/CUGz417V+58m4nmMPAwgJsfGOTwzmPc84sQnHaLg6ifP8xI1j36ONQQ3YrN3L5l+ZNFNzjtjAtKuChmU7KKZmuk1txSstPTBhqjUrYp2ZwcuW9bMl7r12Uve2OmwCG3YgS5sqr/2F0quUEzjnspmfqx0tJrtvImE4R8UeqA+Sagidj8fIVL/1MK16hELE1BeROKY8p5V+p2GVRD24u7SHHEWye+EFEcA2U0638bg0qo/Ossow8LIQ0G8TPjuWCzZy2zLM6xIXsLPyzKdfB4s6gP9v8K5t9/+yryfM+CizVV9sR/5SaFdiBQg+sh5L3SrV5iRySaRMBvBQn50Ibx/TCoPkUo1pY3oFA+SbIYKKqYXMDLi2nABN+BA7qmyEBpZrdERVL1kiOGNLBoz6yt6ZvjQluw+qZ08u1eW+p7lo7D02U/XWaGgdBjNOAhIsQONA1fsZQbrWxkw6qSpehqTVamxumT2tA7LsTdyW5x6RjkwDXpCvY0hw5608NChekT6KxtclVngq+tYBKYa6mQhxqWls+TluquDkY03Z2ksqXaMlW1nEPa1uMHTDvuEKQ1kSpd+AMc1X2NxpK7WLoVKzJbCRA+FxonDIcqrOzR+m+1J7t2yW2Fa/XRKn4rL1kyjJC3U3kZ8g0bA4DXcbeLqipYfkB7/Z8IQPPR+AEgJsn7fdXc7EBSjvl7aCrUNJvYj0BBFQh+/r+aFGXIVdSoQ3vq9cvZwL6DUTs6OWUwjFkMJsm5USDxh07UYE9v7jx9snUXd9WQnVFtIjOWB6ZgGc1VTbIk6c5eQEmPn7nu3g+uZQcsakNkNLaJOBYJ1W5Q/TMq4VaxaUzvlWimeBMQOfrbMnVAWANaQA3uK+ZTSQSnrmkXokduEvgW8UgkVDbyGLmHAcu5Yn7i03bKU8VSOYx7yTZjKvY6z2pq5zJK9EI7ATWiiZgjhKK27j8bAnxlnlV94fhawyLiKLNWaF782DaW1b8ZOjLNkv8H5rcTRcbqa9+HgYFknurqGqQhALqkBcXNhXq9hgT52bVDNMbrb36lslRQM1c0LGGiYc62dSunDB8jKbpQ/9QvuC1va4qWn49E+iYO94v6O6H1ZRhoSabpbz23vwuUPMRegbIgGm++g8uEMzeT1/QZXm8iJXckUOJxm4wDmQprvJQN9qesiNh0/2dPtWkTcKl6UiVDBb6Y49S9R6gmKD8Owt1XqwtXDJgZAOcKxOiZ3J+egmu9CEhQWDUFMBF2OHv+8AY24FjZevpCni19qRbwoYZNS3wYsNNzDYIfnGscMBQ6fNrBfy0qFY9uT/Bzg3va2iM8uUCg+I7aLxRHh1y/Ddfl16vXGGgqqij/UPOcZTf/RTj7IkpMeC9qtKi0vHkVjnmOB2v3f0vJtCbFxktT/nNCkYY9pzgvjflC8wG+2XM0Oi8huFBcCiWkDvnssYIjhpTpw2WckepZKQ5cdCoJhXlKa8PhXS44Sz1O2xNuSVbenx7ZqZ79b6wiem3u3jtqQuWiOGj3bDSAGkhG6jeaZQXc1nFG1GNjMSmKVfic8O9QxWrabBu8hZWXWI+dzIw6v/tsOELATNqlfKT51ArCxygsWok1cmxzmFGn8uSleLdhcVrGMlv54olfXShYqhRwOG8I3DBJlYnU5A1F+NW5mpgHER9C0Oe2y6JPSioVI5zfd0OkTpo8iTp5mFf9rS0L/tlv+3pbOcaUp7YhlG5rRMuZXJAYZuX+TjnG90GthgJBsVsg6MFAnBO/F3FWrnafqGs0ViJDJU9K+QotXYZaPLQiVZ8DU4/Y1HDEvBYRaAdQZ/l0maxzLL/qlQDPV4vp/r6diUtM+92JrdH1qC806F0Tchfq1DIcDmraPViUaj3XMdr42x0pPOmrffALfxg2JMiE+S1LJ7s0O5RFGk704j3ZhLQr7urXKHFMbVTPlQPAodCxJZBWLI0sOimVnjqfpxgAKQnY+v1X7g2HDq8r2EGYSdA7XA/9MSbwbK4tsdUSW7Qxvuuhzx3NjcpGKB/5N4jrZMY3lTvxsHWntl5KZfT+9RElBbTqtUJjhfSQ8eVtytyPg00fiBrw2aLjhM4zC3FshBJfN6dL0tjt5QTrDrYHeIa8M7/tzezZl6+f/n+kdJgEnRIkoPhpcjdYCnlkhOLIRT9BeWO1bg+/SFFAhdRb/WOkmAsaPmkD3kaOYixbzOoc5RND1OvmoX8EMooaP7k+v2h9RgPXpZbJLZvkexFI3xfeuoUmxCIwA1fGQkAxETPyO8nnWmSck0HBLA+2qx2YT2qnqFWqufDR5vwVIYBZACJ3MGzopEqAwIxPlLjl/nwL8I8NT7q0v0srMbBZrvD6xLaVPPpZaMI/A1DOtA23aC8f329+bkpn/OWXSqNpzpwttNSPvQFeIVSpVeMSh1j2FQjCaii31gcviYwk0cZ3EidcNDRprSr90VeCJSyadVS1tncqqfJlQfPSePQESIwcyrJIsoAffhOKYqOqOnLF3m9TUvwsyKziDTdG3/aubvbesjBvsRFEtiXciFkzzypCanzE+DE42+WBp4NTjvOP/Wzw0XXLXH0GIp4wJ3iNcjnPIkRTFMiJ0CTdgOzizm6RW9LCWBIULYY7Qx3WDjiFjd+bPXg+TaQOsjzfMpL7KmEzwsce+KvtuC/QaWaPBZH4QkhfIYRvuUC2arDBBqcCvPU6gRSky9cRPezCGhNypgTG9J+roc8n5pKXu9KpJmlHb6sy1/Njwhw0VH/t++sZQrhYZxYRv7oEhIaHPM0AoBKEhmJURocgocGkBaGAwTDXPMyy5bI9kJRYPvwGxdbAm0IpZdbmNZG6jAyS61dxRU0h2sNHgu+ikrosKUfS/Vp2abdX3kllnePo0nnbsxchgNjMVbkItA3EZdwBYUbMYhp6ObUj0ojJn1t9aWgRCcYz05IFQfI7LSpJD+3QSRdbolwyqZj6TwOa8RCkZCe2gRhqdsUHguZBhvOS//cTIxSP3F/jNa1b7oObDovweCSQ1HGC4TbdgbOyR2aZlAV608LnJGTaEkT/ZcE2NnN9qJGBFEQIL3BXFNFIr1Iq3IExEJj7bluwJRAsKAUMhQ7LERsuwodR733KpEK+++EuQelsJ7xgR2dpJnTbeFodv5SQLubaLh8ujlCPsh2mUZPINVMAynEpcUlhHyk1I984lzpg3FKXDAwWtuhYVAI05kAsP2Md7R0lWC99LZl7oEWSHe7ofaJQ86Nyx4wMqjQ8PDeOEkTkGb6I2kMP7ri+2fZk8REVatJut1Ibsg9h5F5xRS4nlbeOWJOfj7VHS6eGE65boZ6hPsmDybOta/HmnCpAPf4C2DFEicX8Ou+qpeLzfE80b6hXm4hLYLHMgUOzzS6F9MktxRWLzhoHwXGOE4dqlGWbKgByLRmxtLBRLxFijN/wxulSsjqGM4/e7TIifQHqWVpK3q/j99MzOscMlqKQHwEW16o60iyiX/l8Io4aGqX4PiM4MySM8p9k3ViScyit/jH146eMe8Kla1XsbtEgBFojpOB6dRwH14teue2tSek62o9FcoJ4SJzsOshKEU+VTloKGazw8zVoSYK85yBUPTIGBxbS4DTXbiomckMEyu5ZQTu4SNVwh6TB1DaQqRGn3kA80064CkKpJikHIiMN5vNc8VrOk9BZrydK1MWhVWzU50/ArLzgEySVsOVGv4nG9xbywTt40vKR8m6kE9ahfEihdkb95z5kAIxCeh8WuIdYaIaMUzy9LQLjWLQ/5QWXxt51brfvriKTGca9+Yj+W0eoWjcEatgEb8gN0G9+Q9/9vV/j76XnNBgdKdk3xV1Abu3SYA2hvzkUZglQ/A5dOLntBtqfRBwMncFP1h115UTg0ZOipHrtJQ+A1L1zQ/VIDqyO0f4vWx1/F0kNMOVR+d4bfN+zSv981c0+dbU42g3Lp+AJh8i6JVPwoONAbrMwFGhhOtmcWpNy2nRObXmpkqAWXrN0jfFtnqH7+jANZX7BAUdTinmjQ5UM6i+1+yBD8nN+tPdgUy17ZrwWgLJs1r0AmSMQs83kd8ShjcQVZSAohsqKoK5rZxDgYsmXErOq9G2RAwzZpU33W93npdNlgVvBSszZgsJ4a73qLfzk+bRi5eu1Y+JJoFFoxLH1b+tCVIpKUhddU0FKK9kDxSj8qXDiG0DYPbwYNdd9Ncdty23NexoNPKwEIkxYZutY2r23UEZC8EzTLajrMUzkOjpqH/dm8P1ovtJ9DmfzIOrzaz3Dj3En7uvfxzIZpL5JIgnzT7JCztONRFQfzo3smGQ7VhjdCcILhYdZRzlTSw/MC0cxZsj3gvvoWpdyPNw5jCgXdPztq3EgKgVMAGzvGT6izBPLqb7bQP9nnftV91FTzB+6zFy3XCZKYHWkDDktKiuCI2MAZ3BvPbArl3cD+7mW+I2f4PuxdmURVSK+cExWktpo31cGSRr6z9gprGIxCBe0FZehxOv8nl1s74pSGpI8CFXkcXCbECQkz41WgqLmfldfRh18xdJ1lEUg/mJSVhqoIxCKQy65xnQo+94m/XvqdaQ3RAoA8CBYFtRlvyq3pBzHNfVj40MfUzw2ldfCmRF8L3vxAXXBGnnq/buhj9CLhDvF7x2lwduRMPsPU6pQ/FRZ7h/VQWIlM5gTzpm7J3b5NdQSv1FNjndSK1bDxU9/dJ9KzH7Tf62nZLEiUGL4qBA8mN6TCaFxPsYo9RmcKMFXqjcyi+JV8qWc1i7a9K/X2OyUyQ8gQGBIX4BJNWeqk9m40ohGJQ7xrdOJDYT6uI46Qy/WFCbU1zHkHvAmdKeLqLNxGu8WResenjocyaMPmnIByKS/ykSTjwVDnDGIM/wyANEF5RIsFHnw2GeHRiuo+JL9aNwG0kRr5YWagQtgy8WL8ky1RlmwUAGE5QOYWFCmhAulVnfRYB53Mk2zPLTf0TLbOQLeyPyrk27dIeDSUSmuSSCB6rUNYU0/lEHaZ25+0MzUTPVuGvCnuCPUTIDX68ys6ILXs4gXPhHq44RJg1GZR6MD7sbHizo2L2bXowC0BL72l5ICbeX918ZAq9miDdoq1Cx/B3G9VCnYiNcOBnpAO7M1rAZ0NhK/yIp4K53S8pvYMueH5+aycWk3bYnq3JvnYxvvkWzsJKuZ0lOD9JMXiPPp6jZDgwBi0cgSFvX1pNU0ZUo0TU14kzVaNF/Rm+cFRFiQ5o0trLHObV7Xd6ziwv8nNqxC+SaE8V0Qn2LUZIWOTA+ZLSsg6+HIQLGmV+bLjEqBal5hoFB3GI9tOEq5EQvvd6GtQrrJBwgUciF3ZNYr4meXYVuCVJnL8HDcWG4BOKJNjBR/Tx7GAhyHebdJQ6/n0HYcSX9F7N/ed9Q1VQdwAAZ88hqD5FksMgFP1OzXKUuOsBpLhqE04JUY6rAQGHJ9aAXhLIMWqTz59F5EcpYLiyz4HYn3/bd9jj7wA1FxBA00hlkQjemMlhH5LMawD7F9lDQULm9ypl7Xd69bsYEv6m/PEb6AXFSK8nTnPsDoUbdWMRhHFGrMlScSFr6cSykJ2TZ98OzFmQrD4TNBQHi88h/rJuSrj9L/iE5f4JSHh5MoDTuz5sXaNDdOsjEgows51to/+ZFiemnzIx28XzWpC8hq0tqLp8xpx2F+5RcnnqltF221XufEmsnPgqcrgwBfWg+KxsXWxkEqF1QbtA3WS/W7SUNyYynv0PupLNOs9aqgFUPnmJDlNZ7BxT28TupWKh0shVQYi/HObuFafaYnWoAqCm66FIlDMO+q07rGVlWR8vv2hN4jIDfo6Y8i2HMxCqpbBkSjrCLct7yG5GWOYC33gkoAfpT02nAtgObROqIqqm4UjoPRE7Dl7fVbd+5l7ZdU6uGuu2sgqvJ2OC2y7QqhUF++q+npEDegWhbYR+FbfK4K9IO1d4iFzloFbIq/ZGjrYyaIlFEi7ncT1I3UKgGKKg86IhjLp4STG0qioh+XW3ABQfxby3+GeQxNzCRmuQjuumZ3LZofMRSAoI/daZI49opQbT2j1rR/lCVXYF93YOA7VlxC9GNXQwhCf01cwJfN9n35k6sXrSpehoZ8AHVa6/AN6EKouvjkawf89f70ajERg3ODITZc2mIZdP+S326HaY1u7Vpp3SzDyXybWUEaa/44yfcVeFMBy/eMAueDHTM95dhZfY5mrEVOqIUYrtPd2SF5mNlu7cgaDkY9a96MoLnzmbaJvPd9+DIGr2cn2ggIDjtGOlVbc2kc/9Sc6M7AGlXT3FnJ/oFI0cjLIzSXxrxKDwEtX0zyeXGs0XJgIZgs8evqJwOJS3nY87VQnHPuuGhPzDb5BKaupb6ZoNxlwWbGvImg7AAIkw91jxfyHUXYT7RWXPCOjwJJnnZUn528obOlYl9dHN8qYeggMWvcqBa2mBJkv0/KwC9cEypa5Dg/+bRBvukCeKzXfCNzvSyD1FJH37lM7sT9usCUETt2Kgz8McdPW+35+VaxypxeInfV4zWaSDczzi740V/J+KQd6LeI8qn4RdgxsETYmR7xt6vZdMenV3kxnb0LAGuqN7kn7WKRyVIh01GQhzjBUX5BInSUJb7wM2MTcWw6/YDTisUq/dpxwsr4IP3gOdAUqdWW6ua+qK3eJgS8fddr0wAjfB78TcqZGOMvUQnccEghYJKw4+8guYDJ8En8a+p903M61CANkYZi7nIi6BwEUkjwScTQMeu+L2CaneyTiYjpETlvGxqeGQVdR3XWI04y0lo4T9LhHIMr97Lc5kMAl55VWb75cxt6lyuRam1V+LhTlBLwAdvastH016kxUMTIlTz4nT+gLyhtwb2udwZPh1h81P5qfNKmd+o39n9GoQOXLgtTE9K3JJWkexp0kbrVQIujwoGHeXIRAZRbd7rNgjV28hIPzi0fNJ8446NPN9v9w/rJwU1p10VNaGsenOr65GhnUywPgDe/SXKEK/FM4s0rm0AcatCRy4c+saSd9FYA4LakzyApAksKcn9fsWtAylQ6RJPhwyAaL1l0Agra8pyNcIQj9RZSwqyehnAw4mUmcDQuo5T8/8JE6Y6UoRnOUulL86Ov3970OTXqVr5KJ2W5oPXdKhgerFuhSeA/Cmg0IacF8TviqiNP/0YUmqlOFIF57AVcx0Q5VFWvPT6NYydx4X/USJmo0x7qXY2bWLTf5d4+1ciN0J+j9Z8u0CHYbBgj7+ERKukjLLrp1uqOIWZ7vKqMMmW07t9b8tHFs0vePopRPtopzeq9VI9EDE1CqpkIXTQGZIiaNgbBgYxQybfzAetaDNtx8fPXc1rYvs3oF04PvPxjSI8unrWD/bn64jV7Sfpqg4hLlq+jJhNVAKrq2BTFWmblm+JR1QQFpQTfY+FVAqMN0fYF2c7qPRsqPtPYkbO9laR0M9CNknwldTNf+YArJzO3bZjCDNRYeF+yK7APgipOKE92f9BWMRq4bE6Oyp8wsTiXtGET+T1aaQpqBZcl7Y6UJSaaZ+qh6xVOHtoqe4ldqhucBORH+ZOms/ulHQT65ibeX34c0ldC3eH/qL+ubTGp9U7HfL14bULvQJ6pub5gX9ovbsVJWTDdT6e4aLzi8fcnGOmbv/Eozu1Mgase9RCUBmvn6U3VuvZM+W7QjpAE1xAXyLo233N4ZPBT4iS4DsfASzyScMXJsHXd53X/fp6FPIFtZYbdWFAjXEBrlEEmQd3QjCMy2vvlTaEO0Yx5eoeMm/Pfqe74nXmGCZ9qpWJT7q+e9mYudjrzTuq3htEOcIkzMJTktprOIYPB1BWLUdi6E2pEHy3OowN4gC0OsPsrNUeBJdrDCPLzkuimELi7MQ8TaJkveA0UP5qf5cOpI9sLaO8kEz9ZNIClarGeWU5DnooOeAWyaJtLVgRM0lSS71vcU/yE0EDSU0jPB4Zcr1ec9OxLCJROHL07y0KnsOhOXO15ilmizoPMdshSqTZEM4qAgirXwseFM+UpIFGWB36No0YWCGg2wgKO8tAguMm4gxx7hOpjG4wCrAKSLUbH18x1MwXh7LvHQMI86tKdI8tTk1Th6M+H1g6zFMEPVaFqm9ojX/9aMjvIZFmrBc80n1fnka9pos2quT37HLhiB5AvjHB9DPOt3piy8eyWQNtaZeY4ZEwyvmyaBTC5w2JDe17wk6TU8Chpv0BLthjSJwozQeVF5jyJJ+ckQ9jzd7Ynvfa63z1ke/Q/zRmWx6Ihrd4ogY+XpzOFXaIK+FIzLExPAD/93to7GroSJIIhdhRvM/HYUMX4aF4gq0yqoDYzWdwleMQia4QosZMd4LkLokWXgQedznH5HF8q3mmj0xUqSpb2V7OF0xpbWZ/e48FJcTuzqp9y2F+Wum4jjOtYz5WRkGrW/BmJOslbWJ91YwJ4cT34f7V88siDkwhRQ4aClSqyPe0yme0LouEEvkbJycvk4J1a9HpWFS/9LPUQ25phJgjD8OBwUCCrI0k8lwVwjy5MyAm+2SDVXDZVCdPTLmpsM4+iPTP/2KO67no3nCp2bZgBc0YNK6mVHPCEnT+/wYA90e0cTqw46V0VXqRQbZqwElZDIIUEUNL/paZuHlh2tEOQjWLVBlQ/HduzeqEG7sp+7DBvbdK0u9VTDxW+UCD/GXXkgmy9CEDwizHKUGjxszcBhN9e08Rjh0NPZyvDdBkZu3oz4EIn01RH0LTiBVoJsJr/ypI159KteAZLGvSLLf/D14cuYw39i2SBMTPz9w4yNowrmhbeligy6Rk+r+6yOFvFytm31BBc1BnWdaxXtiPhiDLTqsC6WgetgPKhfoEaDLu8KgFf3ghimA7RMiR6JszGyzutWNH5QQfhHbEWJhqSAAmQl68wcgj95aoN7xOQyRzzhDTiUKRecwg5pchdDJ5RnM0xnYJe27Kje401iv5d6oxWasX6oKQ+v05K0XbtzSvqmZtMq8TbrUqVTg687jjJ7Ob1ZDsOLY3EqRB3QoBcLHb/xoQdRrJ1/Ju2JFlx0ZLgDKSU8soO5PGstS/L3wLuT34aEBvH7Zj4KBG+YUVWWp8oLR/Z22t1Y8U8gG5qS27BzFSflF9g4OUqxqYv/iIsbtyvxoa9kvXDfF3/nK6lnDi9E1k7AZ2efbwO5aCRPqOKkSIlr2nZ7ZS9VyWEmkX18nayqfVCc//t4Vfs4IBm9YOuNxQRAezf8lHeEwNsEonJHN/Em9TRGMQPLT6eKvHYq9Wrr+cxEuqK7NWs0JYhQ9hI1PNxIBpoi+ZBjFdTkHfrRxcHfRQigDtt4L0j7KpTd5wNyRx7/QpY89qlPwHXmZ7vHU4JskZ/JQHS3F4zFktzOeiCOq2V3WDGJiU2QdnqCery9Uqe4BDHY3letYsQQJoBnfXmFrXzYusgyUAbvXxhtw3meFZoVIYUMnrZMONyUmIfvP2sActnYNTnkhy8k3jKla37IRKWVYrSVJjfLrNYQ228VYhD839DAMQo86koWWrM9YvzCsA3c86R+56WVJ2GhBlrny0mmiUP3kbq6/J8edL15MDwOJL1SFsFUhWLUxglbneirp4NspIl/Ykhzd0oNs7BUVMxghpOkiIQr6SsI+W1PmCpZJgbPdVqdyAv2zGTfc563eEG86s9vkjLLK40ZByE93pXPqA8cRO2CDgBhcd/RWGdmG+3+olkrDcV+onxXSvjghg9DHaYSzZVWlnjsiBt4z/PRKsm1YMkGDoM6hT0gsNX8JW3xPaGlWt1htYjVsdzyjIjHkeRgkgY9YWfxLbQgUhL93tsEgnlvVKko7/VchDRLVKdkMRhoCxCkjQZme06wglZCFtj1h/zO509jZ7/vjz7iaPudd/sq0Rr3PkcUIZnie+VjQ2+SppLRmJzLpKq4LBKZ9VxNNrP6TEBAaI9pGTNNOr/7udwlEsquEeQBUO64u3cpVz7uSUirZWTJLd4/FcrhJw1lYysPGO2mYgrz84RgX0+tekHCZ2dBMzht0pRV9c6bDiJZbS9H7i0ZeLzPC+6davurvbOpYQliIISlnDZnhqpn6X2JWapkBdedT0k3EsxSaWx3JpuMyJ/oT2nbAdWCVku6gTqIyLas3jgpVU6dXhTfiWee7G/q77oC+pFWiMLOJjrqNL9I9aKsSZqGuD/IbHUNYUPQda9WNZA5DfEJ+iN4BR7y9A4MqCiuISyZyRWWcJKH5ltG/TyV+8Gaa6uiaRu+uva0FDs94xc57EgOK2O5nP0/VXSnznuMV3bm4w8l+z5xLlTzZIPAFe5BWg8XoGKHDpqUq6kxBRLdrlw61bcthATLs+b7yyfkLFY2Sh4fcwkzLF07ts3MgosS3Aw9uD3btPrt/Ila78EVIkeR7a3HBWC4UMDVOpIdslaOYeeKADkHRhgX1aAo4vK6EzWq40LbwFMc/CRKtWIgFUtUXLuGcSCE3qr11bAgPxsMh6dvjyNJbUj0Vyg3Zt1ZcuKXPgLgFIuy8cX9ozx24Vf5WiXCr5CjVV5m1Deo3vx0/l45Pzwk5emA/ZJLZLbposWCvvosTx5wTi3ep0XiI2zgAMoFn8AYECtgdtd2mD4ny2u6YdVCnKckP+x8TUgM7Lvl5yShEhX/zeyqFVqhbxabC1HjFqeFqrFciC9sEirnKVM8Rfx0pORL1ZdNAs2753VcpsaDsRbazsw369/1uEq8qmW22fSs07BJrZg0vjW2dL6Sx4tKcbip6BtQfwo/wWi/cphhbixnocT0hR/25bvhXgVUVTxRWEMQGowl3ddsBHhc8hp1Rjl1k+gmct1JCyHEyYwVQWMq253hvsu+p7JI01P6pq2mWmXPriLf3auJYazhq91/E/WejuTxE6MOmAKxZbxGKTCu1aJhXyd4AlKcApIAI1Bp2jzm6fMGtEsZT1/gk4ARZrr2TdztZxRPnpGF+uXWshfheXZr1oHRkevSTIH2OvBBsgyu" />
	</div>
	
<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
	if (!theForm) {
    theForm = document.form1;
}
	function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
	//]]>
</script>


	
<script src="/ScriptResource.axd?d=8gY6fdQfdm4bB1JrjR7MKFZsJlWTfWSOhkrP2hK67AAY8rxlD3hQKeWwZ2q6Xvophzv80Kox4P4Gf4LvuafzUq-tPJxzszblyWFetRgdqdzbpJGkCY6Ar5aiMXxm0Uv-i8hgmpckBsz5mwDaCUqw4b4vOqyFovTPlwe6Xtx77lGbJrelayNvt_YDvDjqtITrF91pEFXrn0Zcwg7iUv7YQa7eOUnQwBKBwBPrGnna8B7Y-8IiBMxoQHCaBntVgwsXVfAll8kbXROukOe9I48KkWHZYbfPKPXF3fXKZcGQQeI-eTM6Qpaqu-dL9LO4ZiI-zWCRZHIiLD3XzMQBh1A_cqdlY7JnhQp2z8KQO_6svd81" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
if (typeof(ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) return false;
return true;
}
	//]]>
</script>

	<div class="aspNetHidden">

		<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F2EE7CEF" />
	</div>
        <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ContentPlaceHolder1$scriptmanager', 'form1', ['tctl00$ContentPlaceHolder1$upPanelPhotos','ContentPlaceHolder1_upPanelPhotos','tctl00$ContentPlaceHolder1$upChildProduct2','ContentPlaceHolder1_upChildProduct2','tctl00$ContentPlaceHolder1$upChildProduct','ContentPlaceHolder1_upChildProduct','tctl00$ContentPlaceHolder1$upProductAdjustments','ContentPlaceHolder1_upProductAdjustments','tctl00$ContentPlaceHolder1$upPricePanelStd','ContentPlaceHolder1_upPricePanelStd','tctl00$ContentPlaceHolder1$upQtyPanel','ContentPlaceHolder1_upQtyPanel','tctl00$ContentPlaceHolder1$upPricePanel','ContentPlaceHolder1_upPricePanel','tctl00$ContentPlaceHolder1$upInstallments','ContentPlaceHolder1_upInstallments','tctl00$ContentPlaceHolder1$upCartBuyButton','ContentPlaceHolder1_upCartBuyButton','tctl00$ContentPlaceHolder1$upProductVariantFilter','ContentPlaceHolder1_upProductVariantFilter','tctl00$ContentPlaceHolder1$upVariantMatrixSidebar','ContentPlaceHolder1_upVariantMatrixSidebar','tctl00$ContentPlaceHolder1$upStockInfoDescription','ContentPlaceHolder1_upStockInfoDescription','tctl00$ContentPlaceHolder1$upStaffels','ContentPlaceHolder1_upStaffels','tctl00$ContentPlaceHolder1$upBarcode','upBarcode','tctl00$ContentPlaceHolder1$upDetails','ContentPlaceHolder1_upDetails','tctl00$ContentPlaceHolder1$upPanelTemplateValues','ContentPlaceHolder1_upPanelTemplateValues','tctl00$ContentPlaceHolder1$upPackage','ContentPlaceHolder1_upPackage','tctl00$ContentPlaceHolder1$updatepanel_Review','ContentPlaceHolder1_updatepanel_Review','tctl00$ContentPlaceHolder1$upPnlFAQ','ContentPlaceHolder1_upPnlFAQ','tctl00$ContentPlaceHolder1$upOptionVariant','ContentPlaceHolder1_upOptionVariant'], [], [], 90, 'ctl00');
	//]]>
</script>

        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdPageLoad" id="ContentPlaceHolder1_hdPageLoad" value="7-10-2023 16:32:30" />

        <div class="pdetail-backbuttonholder">
            <div class="innerbackbuttonholder">
                <a href='https://www.wollplatz.de/wolle/stylecraft' title='Stylecraft'><i class='fa fa-chevron-left'></i>Überprüfen Sie alles aus der Kategorie&nbsp;Stylecraft</a>
            </div>
        </div>

        <div id="price-calculator-popup" class="price-calculatorpopup">
            <span class="calcmenu-close" onclick="toggleAllmenu();"><i class="fa fa-times"></i></span>
            <div class="calculatorpopuptitle">Welke lengte heeft u nodig?</div>

            <div class="cacl-inputholder">
                <div class="calc-inputinner">
                    <label for="txt-popup-qty-m">meter</label>
                    <input id="txt-popup-qty-m" value="0" type="number" step="1" min="0" onmouseup="reCalculatePricePopup()" onkeyup="reCalculatePricePopup()" />
                </div>
                <div class="calc-inputinner">
                    <label for="txt-popup-qty-m">centimeter</label>
                    <input id="txt-popup-qty-cm" value="0" type="number" step="10" min="0" max="90" oninput="maxLengthCheck(this)" onmouseup="reCalculatePricePopup()" onkeyup="reCalculatePricePopup()" />
                </div>
                <div class="calc-inputinnertxt">
                    / <span id="spn-popup-unit-length"></span>
                </div>
                <span class="calc-resulttxt">U heeft <span id="spn-popup-product-count">0</span> stuks nodig voor <span id="spn-popup-price">0</span> euro incl. BTW
                    
                </span>
                <button id="btn-popup-fill-qty" type="button" onclick="fillQtyBoxWithNumber(); toggleAllmenu();">Automatisch invullen</button>
            </div>
        </div>
        <div id="ContentPlaceHolder1_digitalProductAlreadyBoughtHolder" class="popup-product-bought-visible">
		
            <!-- Wordt vervangen met digitale producten al besteld HTML indien van toepassing -->
        
	</div>

        <div id="product-variantpopup" class="pcart-cadeaupopup product-variantpopup"></div>
        <div id="product-variantpopupcategorie" class="pcart-cadeaupopup product-variantpopup product-variantpopupcategorie"></div>
        <div class="shopcover-insider" onclick="toggleAllmenu();"></div>

        <div id="ContentPlaceHolder1_upPanelPhotos">
		
                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdMainImageUrl" id="ContentPlaceHolder1_hdMainImageUrl" value="bwstspecialdk_1836_1_15688763200773.jpg" />
                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdMainImageUrl2" id="ContentPlaceHolder1_hdMainImageUrl2" />
                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdMainImageUrl3" id="ContentPlaceHolder1_hdMainImageUrl3" />
                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdChildImageUrl" id="ContentPlaceHolder1_hdChildImageUrl" value="bwstspecialdk_1001_1_16313763206784.jpg" />
                <div class='pdetail-mainphotoholder pdetail-mainphotoholderleft'><div class='pdetail-tumbslider pdetail-tumbsliderleft'>
<div class='hide' onclick='PauzeAllVideos()'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/bwstspecialdk_1001_1_16313763206784.jpg/200/200/True/stylecraft-special-dk-1001-white.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special dk 1001 White" class="lazyload"/>
</div>
<div class='hide' onclick='PauzeAllVideos()'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/bwstspecialdk_1001_21_16313763206783.jpg/200/200/True/stylecraft-special-dk-1001-white-2.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special dk 1001 White-2" class="lazyload"/>
</div>
<div class='hide' onclick='PauzeAllVideos()'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/stylecraft-special-dk-alle-kleuren-pakket-2_16313763225655.gif/200/200/True/stylecraft-special-dk-1001-white-3.gif" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special dk 1001 White-3" class="lazyload"/>
</div>
<div class='hide' onclick='PauzeAllVideos()'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer511938763208697.jpg/200/200/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</div>
<div class='hide' onclick='PauzeAllVideos()'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer611938763208714.jpg/200/200/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</div>
<div class='hide' onclick='PauzeAllVideos()'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer111938763208761.jpg/200/200/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</div>
<div class='hide' onclick='PauzeAllVideos()'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer411938763208778.jpg/200/200/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</div>
<div class='hide' onclick='PauzeAllVideos()'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer711938763208818.jpg/200/200/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</div>
<div class='hide' onclick='PauzeAllVideos()'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer92570013194771.jpg/200/200/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</div>
</div>
<div class='pdetail-photoholder pdetail-photoholderleft'>
<div class='hide'><a class='pdetail-fullscreen' href='https://www.wollplatz.de/resize/bwstspecialdk_1001_1_16313763206784.jpg/0/1100/True/stylecraft-special-dk-1001-white.jpg' title='Vergröß Stylecraft Special dk 1001 White'>

<img itemprop='image' src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/bwstspecialdk_1001_1_16313763206784.jpg/500/500/True/stylecraft-special-dk-1001-white.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special dk 1001 White" class="lazyload"/>
</a></div>
<div class='hide'><a class='pdetail-fullscreen' href='https://www.wollplatz.de/resize/bwstspecialdk_1001_21_16313763206783.jpg/0/1100/True/stylecraft-special-dk-1001-white-2.jpg' title='Vergröß Stylecraft Special dk 1001 White-2'>

<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/bwstspecialdk_1001_21_16313763206783.jpg/500/500/True/stylecraft-special-dk-1001-white-2.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special dk 1001 White-2" class="lazyload"/>
</a></div>
<div class='hide'><a class='pdetail-fullscreen' href='https://www.wollplatz.de/resize/stylecraft-special-dk-alle-kleuren-pakket-2_16313763225655.gif/0/1100/True/stylecraft-special-dk-1001-white-3.gif' title='Vergröß Stylecraft Special dk 1001 White-3'>

<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/stylecraft-special-dk-alle-kleuren-pakket-2_16313763225655.gif/500/500/True/stylecraft-special-dk-1001-white-3.gif" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special dk 1001 White-3" class="lazyload"/>
</a></div>
<div class='hide'><a class='pdetail-fullscreen' href='https://www.wollplatz.de/resize/sfeer511938763208697.jpg/0/1100/True/stylecraft-special-dk.jpg' title='Vergröß Stylecraft Special DK'>

<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer511938763208697.jpg/500/500/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</a></div>
<div class='hide'><a class='pdetail-fullscreen' href='https://www.wollplatz.de/resize/sfeer611938763208714.jpg/0/1100/True/stylecraft-special-dk.jpg' title='Vergröß Stylecraft Special DK'>

<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer611938763208714.jpg/500/500/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</a></div>
<div class='hide'><a class='pdetail-fullscreen' href='https://www.wollplatz.de/resize/sfeer111938763208761.jpg/0/1100/True/stylecraft-special-dk.jpg' title='Vergröß Stylecraft Special DK'>

<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer111938763208761.jpg/500/500/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</a></div>
<div class='hide'><a class='pdetail-fullscreen' href='https://www.wollplatz.de/resize/sfeer411938763208778.jpg/0/1100/True/stylecraft-special-dk.jpg' title='Vergröß Stylecraft Special DK'>

<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer411938763208778.jpg/500/500/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</a></div>
<div class='hide'><a class='pdetail-fullscreen' href='https://www.wollplatz.de/resize/sfeer711938763208818.jpg/0/1100/True/stylecraft-special-dk.jpg' title='Vergröß Stylecraft Special DK'>

<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer711938763208818.jpg/500/500/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</a></div>
<div class='hide'><a class='pdetail-fullscreen' href='https://www.wollplatz.de/resize/sfeer92570013194771.jpg/0/1100/True/stylecraft-special-dk.jpg' title='Vergröß Stylecraft Special DK'>

<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer92570013194771.jpg/500/500/True/stylecraft-special-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK" class="lazyload"/>
</a></div>
</div>
</div>

            
	</div>

        

        <div id="ContentPlaceHolder1_pnlPDetailBuyHolder" class="pdetail-buyholder pdetail-buyholderleft">
		
            <div id="ContentPlaceHolder1_divinnerbuyholder" class="innerbuyholder">
                <div id="ContentPlaceHolder1_upChildProduct2">
			
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdProductAdjustmentHeaderTemplateId" id="ContentPlaceHolder1_hdProductAdjustmentHeaderTemplateId" value="0" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdProductAdjustmentDebtorSpecificationId" id="ContentPlaceHolder1_hdProductAdjustmentDebtorSpecificationId" />
                    
		</div>
                <div id="ContentPlaceHolder1_upChildProduct">
			
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdMainProductId" id="ContentPlaceHolder1_hdMainProductId" value="18098" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdChildProductId" id="ContentPlaceHolder1_hdChildProductId" value="18099" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdChildProductItemPriceEx" id="ContentPlaceHolder1_hdChildProductItemPriceEx" value="2,60330000" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdChildProductItemPriceIn" id="ContentPlaceHolder1_hdChildProductItemPriceIn" value="3,10" />

                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdIsPackageProduct" id="ContentPlaceHolder1_hdIsPackageProduct" value="False" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdMainProductIsComposedProduct" id="ContentPlaceHolder1_hdMainProductIsComposedProduct" value="False" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdMainProductComposedProductSetChildSellPriceToZero" id="ContentPlaceHolder1_hdMainProductComposedProductSetChildSellPriceToZero" value="False" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdMainProductStockInfo" id="ContentPlaceHolder1_hdMainProductStockInfo" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdProductLabelClass" id="ContentPlaceHolder1_hdProductLabelClass" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdProductLabelText" id="ContentPlaceHolder1_hdProductLabelText" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdProductDesciption" id="ContentPlaceHolder1_hdProductDesciption" value="Stylecraft Special DK" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdChosenVariantId" id="ContentPlaceHolder1_hdChosenVariantId" value="17686_1417" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdVariantsDisplayType" id="ContentPlaceHolder1_hdVariantsDisplayType" value="3" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdTransNoValidEmail" id="ContentPlaceHolder1_hdTransNoValidEmail" value="E-mail nicht korrekt" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdColorPickerData" id="ContentPlaceHolder1_hdColorPickerData" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdExternalPickerUrl" id="ContentPlaceHolder1_hdExternalPickerUrl" />

                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdTransAdjustmentTextTooShort" id="ContentPlaceHolder1_hdTransAdjustmentTextTooShort" value="Invoer is te kort" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdTransAdjustmentTextTooLong" id="ContentPlaceHolder1_hdTransAdjustmentTextTooLong" value="Invoer is te lang" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdTransAdjustmentTextMinimalChars" id="ContentPlaceHolder1_hdTransAdjustmentTextMinimalChars" value="minimale aantal karakters" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdTransAdjustmentTextMaximalChars" id="ContentPlaceHolder1_hdTransAdjustmentTextMaximalChars" value="maximale aantal karakters" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdQtyUnitCorrected" id="hdQtyUnitCorrected" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdShowVariantsInGridViewAsBoxed" id="hdShowVariantsInGridViewAsBoxed" value="true" />
                        <input type="hidden" name="ctl00$ContentPlaceHolder1$hdVariantChildHasOwnUrl" id="ContentPlaceHolder1_hdVariantChildHasOwnUrl" value="KykqTQUA" />


                        <table itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                            <tr>
<td class='sb-container' colspan='2'>
<div class='filterloader filterloader-child-product' style='position: initial;'><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
<div class='variants-title'><span class='variants-title-txt'>Stylecraft Special dk</span>
<div class='sb-toggle'><i id='sb-box-view' class='fas fa-th active' onclick='ToggleGridView(true)'></i><i id='sb-list-view' class='fas fa-bars ' onclick='ToggleGridView(false)'></i></div>
</div>
<div class="variants-group-container">
<div onclick='VariantSelectedBlock(this)' data-id='17686_1417' class='variants-sb-box-item active '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1001_21_100x100_16313763207333.jpg/150/150/False/bwstspecialdk_1001_21_100x100_16313763207333.jpg' title='1001 White'></div><span data-box-text='1001' data-list-text='1001 White'>1001</span></div><div onclick='VariantSelectedBlock(this)' data-id='17687_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1002_21_100x100_16313763207335.jpg/150/150/False/bwstspecialdk_1002_21_100x100_16313763207335.jpg' title='1002 Black'></div><span data-box-text='1002' data-list-text='1002 Black'>1002</span></div><div onclick='VariantSelectedBlock(this)' data-id='17688_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1003_31_100x100_16313763207337.jpg/150/150/False/bwstspecialdk_1003_31_100x100_16313763207337.jpg' title='1003 Aster'></div><span data-box-text='1003' data-list-text='1003 Aster'>1003</span></div><div onclick='VariantSelectedBlock(this)' data-id='17689_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1004_31_100x100_16313763207339.jpg/150/150/False/bwstspecialdk_1004_31_100x100_16313763207339.jpg' title='1004 Dark Brown'></div><span data-box-text='1004' data-list-text='1004 Dark Brown'>1004</span></div><div onclick='VariantSelectedBlock(this)' data-id='17690_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1005_21_100x100_16313763207341.jpg/150/150/False/bwstspecialdk_1005_21_100x100_16313763207341.jpg' title='1005 Cream'></div><span data-box-text='1005' data-list-text='1005 Cream'>1005</span></div><div onclick='VariantSelectedBlock(this)' data-id='17691_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1009_21_100x100_16313763207342.jpg/150/150/False/bwstspecialdk_1009_21_100x100_16313763207342.jpg' title='1009 Bottle'></div><span data-box-text='1009' data-list-text='1009 Bottle'>1009</span></div><div onclick='VariantSelectedBlock(this)' data-id='17692_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1010_21_100x100_16313763207344.jpg/150/150/False/bwstspecialdk_1010_21_100x100_16313763207344.jpg' title='1010 Matador'></div><span data-box-text='1010' data-list-text='1010 Matador'>1010</span></div><div onclick='VariantSelectedBlock(this)' data-id='17693_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1011_21_100x100_16313763207346.jpg/150/150/False/bwstspecialdk_1011_21_100x100_16313763207346.jpg' title='1011 Midnight'></div><span data-box-text='1011' data-list-text='1011 Midnight'>1011</span></div><div onclick='VariantSelectedBlock(this)' data-id='17694_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1019_21_100x100_16313763207348.jpg/150/150/False/bwstspecialdk_1019_21_100x100_16313763207348.jpg' title='1019 Cloud Blue'></div><span data-box-text='1019' data-list-text='1019 Cloud Blue'>1019</span></div><div onclick='VariantSelectedBlock(this)' data-id='17695_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1020_21_100x100_16313763207350.jpg/150/150/False/bwstspecialdk_1020_21_100x100_16313763207350.jpg' title='1020 Lemon'></div><span data-box-text='1020' data-list-text='1020 Lemon'>1020</span></div><div onclick='VariantSelectedBlock(this)' data-id='17696_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1023_21_100x100_16313763207377.jpg/150/150/False/bwstspecialdk_1023_21_100x100_16313763207377.jpg' title='1023 Raspberry'></div><span data-box-text='1023' data-list-text='1023 Raspberry'>1023</span></div><div onclick='VariantSelectedBlock(this)' data-id='17697_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1026_31_100x100_16313763207379.jpg/150/150/False/bwstspecialdk_1026_31_100x100_16313763207379.jpg' title='1026 Apricot'></div><span data-box-text='1026' data-list-text='1026 Apricot'>1026</span></div><div onclick='VariantSelectedBlock(this)' data-id='17698_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1027_21_100x100_16313763207380.jpg/150/150/False/bwstspecialdk_1027_21_100x100_16313763207380.jpg' title='1027 Khaki'></div><span data-box-text='1027' data-list-text='1027 Khaki'>1027</span></div><div onclick='VariantSelectedBlock(this)' data-id='17699_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1029_31_100x100_16313763207382.jpg/150/150/False/bwstspecialdk_1029_31_100x100_16313763207382.jpg' title='1029 Copper'></div><span data-box-text='1029' data-list-text='1029 Copper'>1029</span></div><div onclick='VariantSelectedBlock(this)' data-id='17700_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1034_21_100x100_16313763207385.jpg/150/150/False/bwstspecialdk_1034_21_100x100_16313763207385.jpg' title='1034 Sherbet'></div><span data-box-text='1034' data-list-text='1034 Sherbet'>1034</span></div><div onclick='VariantSelectedBlock(this)' data-id='17701_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1035_21_100x100_16313763207388.jpg/150/150/False/bwstspecialdk_1035_21_100x100_16313763207388.jpg' title='1035 Burgundy'></div><span data-box-text='1035' data-list-text='1035 Burgundy'>1035</span></div><div onclick='VariantSelectedBlock(this)' data-id='17702_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1054_21_100x100_16313763207390.jpg/150/150/False/bwstspecialdk_1054_21_100x100_16313763207390.jpg' title='1054 Walnut'></div><span data-box-text='1054' data-list-text='1054 Walnut'>1054</span></div><div onclick='VariantSelectedBlock(this)' data-id='17703_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1061_21_100x100_16313763207391.jpg/150/150/False/bwstspecialdk_1061_21_100x100_16313763207391.jpg' title='1061 Plum'></div><span data-box-text='1061' data-list-text='1061 Plum'>1061</span></div><div onclick='VariantSelectedBlock(this)' data-id='17704_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1062_31_16938763188148.jpg/150/150/False/bwstspecialdk_1062_31_16938763188148.jpg' title='1062 Teal'></div><span data-box-text='1062' data-list-text='1062 Teal'>1062</span></div><div onclick='VariantSelectedBlock(this)' data-id='17705_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1063_21_100x100_16313763207396.jpg/150/150/False/bwstspecialdk_1063_21_100x100_16313763207396.jpg' title='1063 Graphite'></div><span data-box-text='1063' data-list-text='1063 Graphite'>1063</span></div><div onclick='VariantSelectedBlock(this)' data-id='17706_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1064_21_100x100_16313763207398.jpg/150/150/False/bwstspecialdk_1064_21_100x100_16313763207398.jpg' title='1064 Mocha'></div><span data-box-text='1064' data-list-text='1064 Mocha'>1064</span></div><div onclick='VariantSelectedBlock(this)' data-id='17707_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1065_21_100x100_16313763207399.jpg/150/150/False/bwstspecialdk_1065_21_100x100_16313763207399.jpg' title='1065 Meadow'></div><span data-box-text='1065' data-list-text='1065 Meadow'>1065</span></div><div onclick='VariantSelectedBlock(this)' data-id='17708_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1067_21_100x100_16313763207401.jpg/150/150/False/bwstspecialdk_1067_21_100x100_16313763207401.jpg' title='1067 Grape'></div><span data-box-text='1067' data-list-text='1067 Grape'>1067</span></div><div onclick='VariantSelectedBlock(this)' data-id='17709_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1068_21_100x100_16313763207403.jpg/150/150/False/bwstspecialdk_1068_21_100x100_16313763207403.jpg' title='1068 Turquoise'></div><span data-box-text='1068' data-list-text='1068 Turquoise'>1068</span></div><div onclick='VariantSelectedBlock(this)' data-id='17710_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1080_21_100x100_16313763207406.jpg/150/150/False/bwstspecialdk_1080_21_100x100_16313763207406.jpg' title='1080 Pale Rose'></div><span data-box-text='1080' data-list-text='1080 Pale Rose'>1080</span></div><div onclick='VariantSelectedBlock(this)' data-id='17711_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1081_31_100x100_16313763207410.jpg/150/150/False/bwstspecialdk_1081_31_100x100_16313763207410.jpg' title='1081 Saffron'></div><span data-box-text='1081' data-list-text='1081 Saffron'>1081</span></div><div onclick='VariantSelectedBlock(this)' data-id='17712_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1082_21_100x100_16313763207414.jpg/150/150/False/bwstspecialdk_1082_21_100x100_16313763207414.jpg' title='1082 Bluebell'></div><span data-box-text='1082' data-list-text='1082 Bluebell'>1082</span></div><div onclick='VariantSelectedBlock(this)' data-id='17713_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1083_21_100x100_16313763207441.jpg/150/150/False/bwstspecialdk_1083_21_100x100_16313763207441.jpg' title='1083 Pomegranate'></div><span data-box-text='1083' data-list-text='1083 Pomegranate'>1083</span></div><div onclick='VariantSelectedBlock(this)' data-id='17714_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1084_21_100x100_16313763207754.jpg/150/150/False/bwstspecialdk_1084_21_100x100_16313763207754.jpg' title='1084 Magenta'></div><span data-box-text='1084' data-list-text='1084 Magenta'>1084</span></div><div onclick='VariantSelectedBlock(this)' data-id='17715_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1099_21_100x100_16313763207816.jpg/150/150/False/bwstspecialdk_1099_21_100x100_16313763207816.jpg' title='1099 Grey'></div><span data-box-text='1099' data-list-text='1099 Grey'>1099</span></div><div onclick='VariantSelectedBlock(this)' data-id='17716_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1114_31_100x100_16313763207820.jpg/150/150/False/bwstspecialdk_1114_31_100x100_16313763207820.jpg' title='1114 Sunshine'></div><span data-box-text='1114' data-list-text='1114 Sunshine'>1114</span></div><div onclick='VariantSelectedBlock(this)' data-id='17717_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1116_31_100x100_16313763207821.jpg/150/150/False/bwstspecialdk_1116_31_100x100_16313763207821.jpg' title='1116 Green'></div><span data-box-text='1116' data-list-text='1116 Green'>1116</span></div><div onclick='VariantSelectedBlock(this)' data-id='17718_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1117_21_100x100_16313763207824.jpg/150/150/False/bwstspecialdk_1117_21_100x100_16313763207824.jpg' title='1117 Royal'></div><span data-box-text='1117' data-list-text='1117 Royal'>1117</span></div><div onclick='VariantSelectedBlock(this)' data-id='17719_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1123_31_100x100_16313763207827.jpg/150/150/False/bwstspecialdk_1123_31_100x100_16313763207827.jpg' title='1123 Claret'></div><span data-box-text='1123' data-list-text='1123 Claret'>1123</span></div><div onclick='VariantSelectedBlock(this)' data-id='17720_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1124_21_100x100_16313763207837.jpg/150/150/False/bwstspecialdk_1124_21_100x100_16313763207837.jpg' title='1124 Greengage'></div><span data-box-text='1124' data-list-text='1124 Greengage'>1124</span></div><div onclick='VariantSelectedBlock(this)' data-id='17721_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1125_21_100x100_16313763207839.jpg/150/150/False/bwstspecialdk_1125_21_100x100_16313763207839.jpg' title='1125 Waterfall'></div><span data-box-text='1125' data-list-text='1125 Waterfall'>1125</span></div><div onclick='VariantSelectedBlock(this)' data-id='17722_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1126_21_100x100_16313763207841.jpg/150/150/False/bwstspecialdk_1126_21_100x100_16313763207841.jpg' title='1126 Sandstone'></div><span data-box-text='1126' data-list-text='1126 Sandstone'>1126</span></div><div onclick='VariantSelectedBlock(this)' data-id='17723_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1127_31_100x100_16313763207843.jpg/150/150/False/bwstspecialdk_1127_31_100x100_16313763207843.jpg' title='1127 Peony'></div><span data-box-text='1127' data-list-text='1127 Peony'>1127</span></div><div onclick='VariantSelectedBlock(this)' data-id='17724_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1128_21_100x100_16313763207846.jpg/150/150/False/bwstspecialdk_1128_21_100x100_16313763207846.jpg' title='1128 Charcoal'></div><span data-box-text='1128' data-list-text='1128 Charcoal'>1128</span></div><div onclick='VariantSelectedBlock(this)' data-id='17725_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1129_21_100x100_16313763207848.jpg/150/150/False/bwstspecialdk_1129_21_100x100_16313763207848.jpg' title='1129 Viola'></div><span data-box-text='1129' data-list-text='1129 Viola'>1129</span></div><div onclick='VariantSelectedBlock(this)' data-id='17726_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1130_31_100x100_16313763207851.jpg/150/150/False/bwstspecialdk_1130_31_100x100_16313763207851.jpg' title='1130 Candyfloss'></div><span data-box-text='1130' data-list-text='1130 Candyfloss'>1130</span></div><div onclick='VariantSelectedBlock(this)' data-id='17727_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1132_21_100x100_16313763207877.jpg/150/150/False/bwstspecialdk_1132_21_100x100_16313763207877.jpg' title='1132 Shrimp'></div><span data-box-text='1132' data-list-text='1132 Shrimp'>1132</span></div><div onclick='VariantSelectedBlock(this)' data-id='17728_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1188_31_100x100_16313763207881.jpg/150/150/False/bwstspecialdk_1188_31_100x100_16313763207881.jpg' title='1188 Lavender'></div><span data-box-text='1188' data-list-text='1188 Lavender'>1188</span></div><div onclick='VariantSelectedBlock(this)' data-id='17729_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1202_21_100x100_16313763207896.jpg/150/150/False/bwstspecialdk_1202_21_100x100_16313763207896.jpg' title='1202 Atlantis'></div><span data-box-text='1202' data-list-text='1202 Atlantis'>1202</span></div><div onclick='VariantSelectedBlock(this)' data-id='17730_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1203_21_100x100_16313763207898.jpg/150/150/False/bwstspecialdk_1203_21_100x100_16313763207898.jpg' title='1203 Silver'></div><span data-box-text='1203' data-list-text='1203 Silver'>1203</span></div><div onclick='VariantSelectedBlock(this)' data-id='17731_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1204_31_100x100_16313763207900.jpg/150/150/False/bwstspecialdk_1204_31_100x100_16313763207900.jpg' title='1204 Camation'></div><span data-box-text='1204' data-list-text='1204 Camation'>1204</span></div><div onclick='VariantSelectedBlock(this)' data-id='17732_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1218_21_100x100_16313763207909.jpg/150/150/False/bwstspecialdk_1218_21_100x100_16313763207909.jpg' title='1218 Parchment'></div><span data-box-text='1218' data-list-text='1218 Parchment'>1218</span></div><div onclick='VariantSelectedBlock(this)' data-id='17733_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1240_21_100x100_16313763207940.jpg/150/150/False/bwstspecialdk_1240_21_100x100_16313763207940.jpg' title='1240 Soft Peach'></div><span data-box-text='1240' data-list-text='1240 Soft Peach'>1240</span></div><div onclick='VariantSelectedBlock(this)' data-id='17734_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1241_21_100x100_16313763207942.jpg/150/150/False/bwstspecialdk_1241_21_100x100_16313763207942.jpg' title='1241 Fondant'></div><span data-box-text='1241' data-list-text='1241 Fondant'>1241</span></div><div onclick='VariantSelectedBlock(this)' data-id='17735_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1246_31_100x100_16313763207944.jpg/150/150/False/bwstspecialdk_1246_31_100x100_16313763207944.jpg' title='1246 Lipstick'></div><span data-box-text='1246' data-list-text='1246 Lipstick'>1246</span></div><div onclick='VariantSelectedBlock(this)' data-id='17736_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1256_21_100x100_16313763207946.jpg/150/150/False/bwstspecialdk_1256_21_100x100_16313763207946.jpg' title='1256 Jaffa'></div><span data-box-text='1256' data-list-text='1256 Jaffa'>1256</span></div><div onclick='VariantSelectedBlock(this)' data-id='17737_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1257_21_100x100_16313763207951.jpg/150/150/False/bwstspecialdk_1257_21_100x100_16313763207951.jpg' title='1257 Fiesta'></div><span data-box-text='1257' data-list-text='1257 Fiesta'>1257</span></div><div onclick='VariantSelectedBlock(this)' data-id='17738_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1259_21_100x100_16313763214630.jpg/150/150/False/bwstspecialdk_1259_21_100x100_16313763214630.jpg' title='1259 Bright Green'></div><span data-box-text='1259' data-list-text='1259 Bright Green'>1259</span></div><div onclick='VariantSelectedBlock(this)' data-id='17739_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1263_21_100x100_16313763214634.jpg/150/150/False/bwstspecialdk_1263_21_100x100_16313763214634.jpg' title='1263 Citron'></div><span data-box-text='1263' data-list-text='1263 Citron'>1263</span></div><div onclick='VariantSelectedBlock(this)' data-id='17740_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1277_21_100x100_16313763214753.jpg/150/150/False/bwstspecialdk_1277_21_100x100_16313763214753.jpg' title='1277 Violet'></div><span data-box-text='1277' data-list-text='1277 Violet'>1277</span></div><div onclick='VariantSelectedBlock(this)' data-id='17741_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1302_21_100x100_16313763214756.jpg/150/150/False/bwstspecialdk_1302_21_100x100_16313763214756.jpg' title='1302 Denim'></div><span data-box-text='1302' data-list-text='1302 Denim'>1302</span></div><div onclick='VariantSelectedBlock(this)' data-id='17742_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1316_21_100x100_16313763214761.jpg/150/150/False/bwstspecialdk_1316_21_100x100_16313763214761.jpg' title='1316 Spring Green'></div><span data-box-text='1316' data-list-text='1316 Spring Green'>1316</span></div><div onclick='VariantSelectedBlock(this)' data-id='17743_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1390_21_100x100_16313763214763.jpg/150/150/False/bwstspecialdk_1390_21_100x100_16313763214763.jpg' title='1390 Clematis'></div><span data-box-text='1390' data-list-text='1390 Clematis'>1390</span></div><div onclick='VariantSelectedBlock(this)' data-id='17744_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1420_21_100x100_16313763214768.jpg/150/150/False/bwstspecialdk_1420_21_100x100_16313763214768.jpg' title='1420 Camel'></div><span data-box-text='1420' data-list-text='1420 Camel'>1420</span></div><div onclick='VariantSelectedBlock(this)' data-id='17745_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1422_31_100x100_16313763214773.jpg/150/150/False/bwstspecialdk_1422_31_100x100_16313763214773.jpg' title='1422 Aspen'></div><span data-box-text='1422' data-list-text='1422 Aspen'>1422</span></div><div onclick='VariantSelectedBlock(this)' data-id='17746_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1425_21_100x100_16313763214776.jpg/150/150/False/bwstspecialdk_1425_21_100x100_16313763214776.jpg' title='1425 Emperor'></div><span data-box-text='1425' data-list-text='1425 Emperor'>1425</span></div><div onclick='VariantSelectedBlock(this)' data-id='17747_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1432_21_100x100_16313763214780.jpg/150/150/False/bwstspecialdk_1432_21_100x100_16313763214780.jpg' title='1432 Wisteria'></div><span data-box-text='1432' data-list-text='1432 Wisteria'>1432</span></div><div onclick='VariantSelectedBlock(this)' data-id='17748_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1435_31_100x100_16313763214788.jpg/150/150/False/bwstspecialdk_1435_31_100x100_16313763214788.jpg' title='1435 Bright Pink'></div><span data-box-text='1435' data-list-text='1435 Bright Pink'>1435</span></div><div onclick='VariantSelectedBlock(this)' data-id='17749_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1708_21_100x100_16313763214816.jpg/150/150/False/bwstspecialdk_1708_21_100x100_16313763214816.jpg' title='1708 Petrol'></div><span data-box-text='1708' data-list-text='1708 Petrol'>1708</span></div><div onclick='VariantSelectedBlock(this)' data-id='17750_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1709_21_100x100_16313763214821.jpg/150/150/False/bwstspecialdk_1709_21_100x100_16313763214821.jpg' title='1709 Gold'></div><span data-box-text='1709' data-list-text='1709 Gold'>1709</span></div><div onclick='VariantSelectedBlock(this)' data-id='17751_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1710_21_100x100_16313763214828.jpg/150/150/False/bwstspecialdk_1710_21_100x100_16313763214828.jpg' title='1710 Stone'></div><span data-box-text='1710' data-list-text='1710 Stone'>1710</span></div><div onclick='VariantSelectedBlock(this)' data-id='17752_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1711_21_100x100_16313763214831.jpg/150/150/False/bwstspecialdk_1711_21_100x100_16313763214831.jpg' title='1711 Spice'></div><span data-box-text='1711' data-list-text='1711 Spice'>1711</span></div><div onclick='VariantSelectedBlock(this)' data-id='17753_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1712_21_100x100_16313763214836.jpg/150/150/False/bwstspecialdk_1712_21_100x100_16313763214836.jpg' title='1712 Lime'></div><span data-box-text='1712' data-list-text='1712 Lime'>1712</span></div><div onclick='VariantSelectedBlock(this)' data-id='17754_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1722_21_100x100_16313763214840.jpg/150/150/False/bwstspecialdk_1722_21_100x100_16313763214840.jpg' title='1722 Storm-Blue'></div><span data-box-text='1722' data-list-text='1722 Storm-Blue'>1722</span></div><div onclick='VariantSelectedBlock(this)' data-id='17755_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1723_21_100x100_16313763214844.jpg/150/150/False/bwstspecialdk_1723_21_100x100_16313763214844.jpg' title='1723 Tomato'></div><span data-box-text='1723' data-list-text='1723 Tomato'>1723</span></div><div onclick='VariantSelectedBlock(this)' data-id='17756_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1724_21_100x100_16313763214884.jpg/150/150/False/bwstspecialdk_1724_21_100x100_16313763214884.jpg' title='1724 Parma-Violet'></div><span data-box-text='1724' data-list-text='1724 Parma-Violet'>1724</span></div><div onclick='VariantSelectedBlock(this)' data-id='17757_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1725_31_100x100_16313763214887.jpg/150/150/False/bwstspecialdk_1725_31_100x100_16313763214887.jpg' title='1725 Sage'></div><span data-box-text='1725' data-list-text='1725 Sage'>1725</span></div><div onclick='VariantSelectedBlock(this)' data-id='23765_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1805_21_100x100_10070013176948.jpg/150/150/False/bwstspecialdk_1805_21_100x100_10070013176948.jpg' title='1805 Warm Grey'></div><span data-box-text='1805' data-list-text='1805 Warm Grey'>1805</span></div><div onclick='VariantSelectedBlock(this)' data-id='23766_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1806_21_100x100_16313763214894.jpg/150/150/False/bwstspecialdk_1806_21_100x100_16313763214894.jpg' title='1806 Gingerbread'></div><span data-box-text='1806' data-list-text='1806 Gingerbread'>1806</span></div><div onclick='VariantSelectedBlock(this)' data-id='23767_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1807_21_100x100_16313763214910.jpg/150/150/False/bwstspecialdk_1807_21_100x100_16313763214910.jpg' title='1807 Hint of Silver'></div><span data-box-text='1807' data-list-text='1807 Hint of Silver'>1807</span></div><div onclick='VariantSelectedBlock(this)' data-id='17758_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1820_21_100x100_16313763214942.jpg/150/150/False/bwstspecialdk_1820_21_100x100_16313763214942.jpg' title='1820 Duck-Egg'></div><span data-box-text='1820' data-list-text='1820 Duck-Egg'>1820</span></div><div onclick='VariantSelectedBlock(this)' data-id='17759_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1821_21_100x100_16313763214946.jpg/150/150/False/bwstspecialdk_1821_21_100x100_16313763214946.jpg' title='1821 Grass-Green'></div><span data-box-text='1821' data-list-text='1821 Grass-Green'>1821</span></div><div onclick='VariantSelectedBlock(this)' data-id='17760_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1822_21_100x100_16313763214950.jpg/150/150/False/bwstspecialdk_1822_21_100x100_16313763214950.jpg' title='1822 Pistachio'></div><span data-box-text='1822' data-list-text='1822 Pistachio'>1822</span></div><div onclick='VariantSelectedBlock(this)' data-id='17761_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1823_21_100x100_16313763214954.jpg/150/150/False/bwstspecialdk_1823_21_100x100_16313763214954.jpg' title='1823 Mustard'></div><span data-box-text='1823' data-list-text='1823 Mustard'>1823</span></div><div onclick='VariantSelectedBlock(this)' data-id='17762_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1824_21_100x100_16313763214961.jpg/150/150/False/bwstspecialdk_1824_21_100x100_16313763214961.jpg' title='1824 Cypress'></div><span data-box-text='1824' data-list-text='1824 Cypress'>1824</span></div><div onclick='VariantSelectedBlock(this)' data-id='17763_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1825_21_100x100_16313763214963.jpg/150/150/False/bwstspecialdk_1825_21_100x100_16313763214963.jpg' title='1825 Lobelia'></div><span data-box-text='1825' data-list-text='1825 Lobelia'>1825</span></div><div onclick='VariantSelectedBlock(this)' data-id='17764_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1826_21_100x100_16313763214966.jpg/150/150/False/bwstspecialdk_1826_21_100x100_16313763214966.jpg' title='1826 Kelly-green'></div><span data-box-text='1826' data-list-text='1826 Kelly-green'>1826</span></div><div onclick='VariantSelectedBlock(this)' data-id='17765_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1827_21_100x100_16313763214976.jpg/150/150/False/bwstspecialdk_1827_21_100x100_16313763214976.jpg' title='1827 Fuchsia-purple'></div><span data-box-text='1827' data-list-text='1827 Fuchsia-purple'>1827</span></div><div onclick='VariantSelectedBlock(this)' data-id='17766_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1828_21_100x100_16313763215006.jpg/150/150/False/bwstspecialdk_1828_21_100x100_16313763215006.jpg' title='1828 Boysenberry'></div><span data-box-text='1828' data-list-text='1828 Boysenberry'>1828</span></div><div onclick='VariantSelectedBlock(this)' data-id='17767_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1829_31_100x100_16313763215010.jpg/150/150/False/bwstspecialdk_1829_31_100x100_16313763215010.jpg' title='1829 Empire'></div><span data-box-text='1829' data-list-text='1829 Empire'>1829</span></div><div onclick='VariantSelectedBlock(this)' data-id='17768_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1831_21_100x100_16313763215014.jpg/150/150/False/bwstspecialdk_1831_21_100x100_16313763215014.jpg' title='1831 Lapis'></div><span data-box-text='1831' data-list-text='1831 Lapis'>1831</span></div><div onclick='VariantSelectedBlock(this)' data-id='17769_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1832_21_100x100_16313763215024.jpg/150/150/False/bwstspecialdk_1832_21_100x100_16313763215024.jpg' title='1832 Mushroom'></div><span data-box-text='1832' data-list-text='1832 Mushroom'>1832</span></div><div onclick='VariantSelectedBlock(this)' data-id='17770_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1833_21_100x100_16313763215028.jpg/150/150/False/bwstspecialdk_1833_21_100x100_16313763215028.jpg' title='1833 Blush'></div><span data-box-text='1833' data-list-text='1833 Blush'>1833</span></div><div onclick='VariantSelectedBlock(this)' data-id='17771_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1834_21_100x100_16313763215096.jpg/150/150/False/bwstspecialdk_1834_21_100x100_16313763215096.jpg' title='1834 Lincoln'></div><span data-box-text='1834' data-list-text='1834 Lincoln'>1834</span></div><div onclick='VariantSelectedBlock(this)' data-id='17772_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1835_21_100x100_16313763215130.jpg/150/150/False/bwstspecialdk_1835_21_100x100_16313763215130.jpg' title='1835 Buttermilk'></div><span data-box-text='1835' data-list-text='1835 Buttermilk'>1835</span></div><div onclick='VariantSelectedBlock(this)' data-id='17773_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1836_21_100x100_16313763215134.jpg/150/150/False/bwstspecialdk_1836_21_100x100_16313763215134.jpg' title='1836 Vintage Peach'></div><span data-box-text='1836' data-list-text='1836 Vintage Peach'>1836</span></div><div onclick='VariantSelectedBlock(this)' data-id='17774_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1841_21_100x100_16313763215140.jpg/150/150/False/bwstspecialdk_1841_21_100x100_16313763215140.jpg' title='1841 Cornish Blue'></div><span data-box-text='1841' data-list-text='1841 Cornish Blue'>1841</span></div><div onclick='VariantSelectedBlock(this)' data-id='17775_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1842_21_100x100_16313763215144.jpg/150/150/False/bwstspecialdk_1842_21_100x100_16313763215144.jpg' title='1842 Spearmint'></div><span data-box-text='1842' data-list-text='1842 Spearmint'>1842</span></div><div onclick='VariantSelectedBlock(this)' data-id='17776_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1843_21_100x100_16313763215268.jpg/150/150/False/bwstspecialdk_1843_21_100x100_16313763215268.jpg' title='1843 Powder Pink'></div><span data-box-text='1843' data-list-text='1843 Powder Pink'>1843</span></div><div onclick='VariantSelectedBlock(this)' data-id='17777_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1844_21_100x100_16313763215271.jpg/150/150/False/bwstspecialdk_1844_21_100x100_16313763215271.jpg' title='1844 Toy'></div><span data-box-text='1844' data-list-text='1844 Toy'>1844</span></div><div onclick='VariantSelectedBlock(this)' data-id='17778_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1852_21_100x100_16313763215273.jpg/150/150/False/bwstspecialdk_1852_21_100x100_16313763215273.jpg' title='1852 Apple'></div><span data-box-text='1852' data-list-text='1852 Apple'>1852</span></div><div onclick='VariantSelectedBlock(this)' data-id='17779_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1853_21_100x100_16313763215275.jpg/150/150/False/bwstspecialdk_1853_21_100x100_16313763215275.jpg' title='1853 Clementine'></div><span data-box-text='1853' data-list-text='1853 Clementine'>1853</span></div><div onclick='VariantSelectedBlock(this)' data-id='17780_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1854_21_100x100_16313763215278.jpg/150/150/False/bwstspecialdk_1854_21_100x100_16313763215278.jpg' title='1854 French Navy'></div><span data-box-text='1854' data-list-text='1854 French Navy'>1854</span></div><div onclick='VariantSelectedBlock(this)' data-id='17781_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1855_21_100x100_16313763215285.jpg/150/150/False/bwstspecialdk_1855_21_100x100_16313763215285.jpg' title='1855 Proper Purple'></div><span data-box-text='1855' data-list-text='1855 Proper Purple'>1855</span></div><div onclick='VariantSelectedBlock(this)' data-id='17782_1417' class='variants-sb-box-item '><div class='variants-sb-img-holder'><img src='https://www.wollplatz.de/resize/bwstspecialdk_1856_21_100x100_16313763215531.jpg/150/150/False/bwstspecialdk_1856_21_100x100_16313763215531.jpg' title='1856 Dandelion'></div><span data-box-text='1856' data-list-text='1856 Dandelion'>1856</span></div></div>
</td>
</tr>


                                <tr class="pbuy-buybx">
                                    <td colspan="2">
                                        <div class='buy-extrainfo'><span class='product-oldprice'><del><span class='product-price-currency'>€ </span><span class='product-price-amount'>3,25</span></del><span class='product-cheaper'><span>-</span>5%</span></span></div><div id="ContentPlaceHolder1_upProductAdjustments">
				
                                                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdProductAdjustmentCheckedValues" id="ContentPlaceHolder1_hdProductAdjustmentCheckedValues" />
                                                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdProductAdjustmentHasPreChosen" id="ContentPlaceHolder1_hdProductAdjustmentHasPreChosen" />
                                                
                                                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdProductAdjustmentsIsValid" id="ContentPlaceHolder1_hdProductAdjustmentsIsValid" />
                                                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdStandardAmountQtyCorrected" id="ContentPlaceHolder1_hdStandardAmountQtyCorrected" />
                                                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdStandardAmountQty" id="ContentPlaceHolder1_hdStandardAmountQty" />
                                            
			</div><div id="ContentPlaceHolder1_upPricePanelStd">
				
                                                
                                            
			</div><div id="ContentPlaceHolder1_pPanelNormal">
				
                                            <div id="ContentPlaceHolder1_pnlQty" class="buy-quantity">
					
                                                <div id="ContentPlaceHolder1_upQtyPanel">
						
                                                        <div class="input-quantiy-holder">
                                                            
                                                            <span class="btn-quantity btn-quantity-minus" onclick="QuantityMin('txtQty')"><i class="fa fa-minus"></i></span>
                                                            <input name="ctl00$ContentPlaceHolder1$txtQty" type="number" id="txtQty" onchange="ChangeQty(this.value)" class="input-white input-quantity" value="1" min="1" step="1" max="9999999" />
                                                            <span class="btn-quantity btn-quantity-plus" onclick="QuantityPlus('txtQty')"><i class="fa fa-plus"></i></span>
                                                        </div>
                                                    
					</div>
                                            
				</div>

                                            <div class="buy-price">
                                                <div id="ContentPlaceHolder1_upPricePanel">
					
                                                        
                                                        <meta itemprop='url' content='https://www.wollplatz.de/artikel/18099/stylecraft-special-dk-1001-white.html' /><meta itemprop='priceCurrency' content='EUR' /><meta itemprop='itemCondition' content='http://schema.org/NewCondition' /><span ' 
                  class='product-price' itemprop='price' 
                  content='3.10'>
                    <span class='product-price-currency'>€ </span><span class='product-price-amount'>3,10</span>
                  </span>

                                                        Inkl. MwSt zzgl. <a class='product-vatprice' target="_blank" href="/i/versandkosten.html">Versandkosten</a>

                                                        
                                                        
                                                    
				</div>
                                            </div>
                                        
			</div><div id="ContentPlaceHolder1_upInstallments">
				
                                                
                                            
			</div><meta itemprop='availability' content='http://schema.org/InStock' /><div id="ContentPlaceHolder1_upCartBuyButton">
				
                                                <button id="ContentPlaceHolder1_btnAddToCart" data-qty="txtQty.Value" data-productprice="2.60" data-productcode="bwstspecialdk_1" aria-label="Add" data-productbarcode="5034533027017" class="btn btn-buy" onclick="return AddToCartAlt(this, true);AddProductToShoppingCart(this);" data-productbrand="Stylecraft" type="button" data-productname="Stylecraft Special dk 1001 White" data-productid="18099"> <i class="fa fa-basket"></i>In den Warenkorb</button>

                                                
                                            
			</div>

                                        <div class="ajax-loading btn btn-buy" style="display: none;">
                                            <i class="fa fa-circle-o-notch fa-spin"></i>In Bearbeitung...
                                        </div>


                                        <span id="ContentPlaceHolder1_lblAddToWishList" class="placeholder-wishlistbtn"><a id="ContentPlaceHolder1_btnAddToWishlist" class="btn btn-wishlist" href="javascript:__doPostBack(&#39;ctl00$ContentPlaceHolder1$btnAddToWishlist&#39;,&#39;&#39;)"><i class="fa fa-heart-o"></i>Auf den Wunschzettel</a></span>

                                    </td>
                                </tr>
                            
                            
                            <tr><td colspan='2'></td></tr>

                            
                                <tr class="pbuy-voorraad">
                                    <td colspan="2">
                                        <div id="ContentPlaceHolder1_upStockInfoDescription">
				
                                                <span class='stock-green'>Lieferbar</span>
                                            
			</div>
                                    </td>
                                </tr>
                            

                            
                            
                                <tr class="pbuy-reviews">
                                    <td>Bewertungen
                                    </td>
                                    <td>
                                        <span class='buy-writereview' onclick="ScrollToDiv('.pdetail-reviewsholder');">
<i class='fa fa-chevron-right'></i>Bewertung schreiben
</span>

                                    </td>
                                </tr>
                            

                            
                                <tr class="pbuy-social">
                                    <td colspan="2">
                                        <span class="pbuy-social-whatsapp">
<a href="https://wa.me/?text= https://www.wollplatz.de/wolle/stylecraft/stylecraft-special-dk">
<i class="fa fa-whatsapp"></i>Teilen mit WhatsApp
</a>
</span>

                                    </td>
                                </tr>
                            

                            

                            <tr class='htmllazyload' data-function='getGiveawayProducts' data-productid='18099'><td colspan='2'>&nbsp;</td></tr>
                            



                        </table>
                        <div id="ContentPlaceHolder1_upStaffels">
				
                                
                            
			</div>
                        
                    
		</div>
            </div>
            <div class="pdetail-extraoptions">
                <i class="fa fa-star"></i>Zu Favoriten hinzufügen
            </div>
        
	</div>

        <div class='pdetail-cmsuspholder pdetail-cmsuspholderleft'>
<div class='innercmsuspholder'>
<ul class="fa-ul">
<li><i class="fa-li fa fa-check-circle"></i>Gratis Versand ab 75€ 
</li><li><i class="fa-li fa fa-check-circle"></i>SOFORT Überweisung, PayPal, GiroPay oder auf Rechnung (Klarna)
</li><li><i class="fa-li fa fa-check-circle"></i>14 Tage Widerrufsrecht
</li><li><i class="fa-li fa fa-check-circle"></i>Garne immer aus dem selben Farbbad</li></ul>
</div>
</div>


        

        
        <div id="ContentPlaceHolder1_pnlQuickMenuProductDetail" class="pdetail-quickmenu">
		
            <ul class="innerquickmenuholder">
                
                
                <li>
                    <span id="ContentPlaceHolder1_spnProductDetailsAndSpecs" onclick="ScrollToDiv(&#39;.pdetail-detailholder&#39;);">Artikel Beschreibung &amp; Spezifikationen</span>
                </li>
                <li id="lblDocuments" class="hide">
                    <span onclick="ScrollToDiv('.documentsholder');">Dokumente</span>
                </li>
                
                

                
                    <li>
                        <span id="spnProductAccessories" onclick="ScrollToDiv('.pdetail-assesoiresholder');">Anleitungen</span>
                    </li>
                
                
                    <li>
                        <span id="spnProductFaq" onclick="ScrollToDiv('.pdetail-faqholder');">Stellen Sie eine Frage</span>
                    </li>
                
                
                
                
                    <li>
                        <span id="spnProductAlternatives" onclick="ScrollToDiv('.inneralternatiefholder');">Alternative Artikel</span>
                    </li>
                
                
                    <li>
                        <span id="spnRelatedProducts" onclick="ScrollToDiv('.innergerelateerdholder');">Themenverwandte Artikel</span>
                    </li>
                
                

            </ul>
        
	</div>

        <div id="ContentPlaceHolder1_pnlProductVariant2Matrix">
		
            <div class="filterloader filterloader-productvariant2matrix" style="position: initial;">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
            <span id="spanProductVariant2Matrix"></span>
        
	</div>

        <div class="clear"></div>
        

        <div class="pdetail-main-container">
            <div id="upBarcode" class="pdetail-barcodeholder hide">
		
                    <script src="/js/JsBarcode.all.js"></script>
                    <h3>Barcode</h3>
                    <svg id="pdetail-barcode"></svg>
                
	</div>

            <div id="ContentPlaceHolder1_upDetails" class="pdetail-detailholder">
		
                    <div class="innerdetailholder">
                        <h3>
                            Artikel Beschreibung
                        </h3>
                        <div class='product-note-collection'></div>
                        <div id='pdetailInfoText' itemprop="description" class='cms-styling'>
Stylecraft Special DK - Bestellen Sie das einzigartige Original Stylecraft Special DK Garn bei Wollplatz. Ein 100% hochwertiges Acrylgarn, das von Millionen Häkel- und Strickbegeisterten für seine fantastische Qualität, satten Farben und seinen unschlagbaren Preis gelobt wird. Stylecraft Special DK ist sehr weich und Projekte mit diesem Garn bleiben lange schön. Ob Kuscheltier, Decke oder Kleidung. Ein Knäuel Stylecraft Special DK wiegt 100 Gramm und hat eine Lauflänge von fast 300 Metern. Damit lässt sich eine warme Decke mit 18 Knäueln (je nach verwendeten Häkelmustern) häkeln. <br>
<br>
Auf unserer Website und im Experience Center bieten wir verschiedene Stylecraft Special DK Garnpakete von beliebten MALs und CALs internationaler Designer an, wie zum Beispiel die Queen Blanket CAL und die Fruit Garden Blanket Love is Enough CAL. Alle diese Pakete enthalten 100 % Original Stylecraft-Garne.<br>
<div>
<table>
<tr>
<td>
<img alt="Special dk new" src="https://www.wolplein.nl/resize/special-dk-still-life_011_5038763212851.jpg/500/500/True/stylecraft-special-dk-1805-warm-grey-2.jpg" width="250px" style=" border-radius: 0%">
</td>
<td style="vertical-align: middle">
<FONT size="+2">Jetzt bestellen: Neue Farben!</FONT>
<br><br>
<li>1805 Warm Grey</li>
<li>1806 Gingerbread</li>
<li>1807 Hint of Silver</li></td>
</tr>
</table>
</div>
<div><br /><br /></div>
</div>

                        <div class='product-note-collection'></div>
                    </div>
                    
                
	</div>
        </div>
        <div id="ContentPlaceHolder1_pnShortSpecs" class="pdetail-specsholder">
		
            <!-- if(no icecat template than add class  pdetail-extraspecificatie -->
            <div class="innerspecsholder">
                <h3>
                    Spezifikationen</h3>

                <div id="ContentPlaceHolder1_upPanelTemplateValues">
			
                        <div class="filterloader filterloader-templatevalues" style="position: initial;">
                            <div class="spinner">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                            </div>
                        </div>
                        <div id="pdetailTableSpecs">
                            <div id="ContentPlaceHolder1_pnlDynamicTemplateValuesBefore"></div>
                            
                            <meta itemprop='mpn' content='904/1001/01/B100' />
                            <table>
                                <tr><td>Gewicht</td><td>100 Gramm</td></tr>
<tr><td>Länge</td><td>295 Meter</td></tr>
<tr><td>Zusammenstellung</td><td>100% Acryl</td></tr>
<tr><td>Nadelstärke</td><td>4 mm</td></tr>
<tr><td>Garnstärke</td><td>DK</td></tr>
<tr><td>Maschenprobe 10x10cm (S)</td><td>22 Maschen</td></tr>
<tr><td>Maschenprobe 10x10cm (R)</td><td>30 Reihen</td></tr>
<tr><td>Pullovergröße</td><td>40</td></tr>
<tr><td>Knäuel pro Pullover</td><td>6</td></tr>
<tr><td>Waschen</td><td>Maximal bei 30 ºC waschen</td></tr>
<tr><td>Bleichen</td><td>Nein</td></tr>
<tr><td>Wäschetrockner</td><td>Darf mit niedriger Temperatur in den Wäschetrockner</td></tr>
<tr><td>Liegend trocknen</td><td>Ja</td></tr>
<tr><td>Bügeln</td><td>Bügeln mit geringer Temperatur</td></tr>
<tr><td>Chemische Reinigung</td><td>Ja</td></tr>

<tr><td>Marke</td><td><a href='https://www.wollplatz.de/marke/34/stylecraft.html' title='Stylecraft'><span itemprop='brand' itemscope itemtype='http://schema.org/Brand'><span itemprop='name'>Stylecraft</span></span></a></td></tr>
<tr><td>Kategorie</td><td><a href='https://www.wollplatz.de/wolle/stylecraft' title='Stylecraft'><span itemprop='category'>Stylecraft</span></a></td></tr>
<tr><td>GTIN</td><td id='TableSpecs_Barcode'><span itemprop="gtin">5034533027017</span></td></tr>
<tr><td>Artikelnummer</td><td id='TableSpecs_ProductCode'><span itemprop='sku'>bwstspecialdk_1</span></td></tr>
<tr><td>Verpackung</td><td>Pro Stück</td></tr>

                            </table>
                            <div id="ContentPlaceHolder1_pnlDynamicTemplateValuesAfter"></div>

                        </div>
                        
                    
		</div>
            </div>
        
	</div>

        

        <div id="ContentPlaceHolder1_upPackage">
		
                
            
	</div>

        <div id="ContentPlaceHolder1_pnlAccessoires" class="pdetail-assesoiresholder">
		
            <div class="innerassesoiresholder">
                <h3 id="h3AccessoiresHeader">
                    Anleitungen</h3>
            </div>
            <div class='box pcw100 pcs0020 tabw100 tabs0020 mobw100 mobs0020 element artikelen-element slide5'>
<div class='productlistholder productlist20'>
<div id='pid-20824' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='ghp000263de' data-productname='Häkelanleitung Duck Egg Umschlagtuch' data-productbrand='Wolplein' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-duck-egg-umschlagtuch' title='Häkelanleitung Duck Egg Umschlagtuch' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/omslagdoek-2_1.jpg/250/250/True/haekelanleitung-duck-egg-umschlagtuch.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Duck Egg Umschlagtuch" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-duck-egg-umschlagtuch' title='Häkelanleitung Duck Egg Umschlagtuch'>Häkelanleitung Duck Egg Umschlagtuch</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='20824' data-productcode='ghp000263de' data-productname='Häkelanleitung Duck Egg Umschlagtuch' data-productprice='0.00' data-productbrand='Wolplein' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-duck-egg-umschlagtuch' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40325' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='ghpst9528' data-productname='Häkelanleitung Stylecraft Life & Special DK No. 9528 Decke und Kissen' data-productbrand='Stylecraft' data-productbarcode='5034533072093'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-life-special-dk-no-9528-decke-und-kissen' title='Häkelanleitung Stylecraft Life & Special DK No. 9528 Decke und Kissen' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9528-0_hoofdafbeelding_13188763183454.jpg/250/250/True/haekelanleitung-stylecraft-life-special-dk-no-9528-decke-und-kissen.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Stylecraft Life & Special DK No. 9528 Decke und Kissen" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-life-special-dk-no-9528-decke-und-kissen' title='Häkelanleitung Stylecraft Life & Special DK No. 9528 Decke und Kissen'>Häkelanleitung Stylecraft Life & Special DK No. 9528 Decke und Kissen</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40325' data-productcode='ghpst9528' data-productname='Häkelanleitung Stylecraft Life & Special DK No. 9528 Decke und Kissen' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-life-special-dk-no-9528-decke-und-kissen' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40346' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='ghpst9255' data-productname='Häkelanleitung Stylecraft Special DK No. 9255 Decke und Kissen' data-productbrand='Stylecraft' data-productbarcode='5034533055539'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-9255-decke-und-kissen' title='Häkelanleitung Stylecraft Special DK No. 9255 Decke und Kissen' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9255-1_hoofdafbeelding_15688763183885.jpg/250/250/True/haekelanleitung-stylecraft-special-dk-no-9255-decke-und-kissen.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Stylecraft Special DK No. 9255 Decke und Kissen" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-9255-decke-und-kissen' title='Häkelanleitung Stylecraft Special DK No. 9255 Decke und Kissen'>Häkelanleitung Stylecraft Special DK No. 9255 Decke und Kissen</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40346' data-productcode='ghpst9255' data-productname='Häkelanleitung Stylecraft Special DK No. 9255 Decke und Kissen' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-9255-decke-und-kissen' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40374' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='ghpst9645' data-productname='Häkelanleitung Stylecraft Special DK No. 9645 Pullover und Weste' data-productbrand='Stylecraft' data-productbarcode='5034533073403'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-9645-pullover-und-weste' title='Häkelanleitung Stylecraft Special DK No. 9645 Pullover und Weste' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9645-0_samen_15688763193942.jpg/250/250/True/haekelanleitung-stylecraft-special-dk-no-9645-pullover-und-weste.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Stylecraft Special DK No. 9645 Pullover und Weste" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-9645-pullover-und-weste' title='Häkelanleitung Stylecraft Special DK No. 9645 Pullover und Weste'>Häkelanleitung Stylecraft Special DK No. 9645 Pullover und Weste</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40374' data-productcode='ghpst9645' data-productname='Häkelanleitung Stylecraft Special DK No. 9645 Pullover und Weste' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-9645-pullover-und-weste' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40361' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='ghpstF003' data-productname='Häkelanleitung Stylecraft Special DK No. F003 Candy Pop Decke und Kissen' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/40361/haekelanleitung-stylecraft-special-dk-no-f003-candy-pop-decke-und-kissen' title='Häkelanleitung Stylecraft Special DK No. F003 Candy Pop Decke und Kissen' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/f003-0_hoofdafbeelding_15688763190261.jpg/250/250/True/haekelanleitung-stylecraft-special-dk-no-f003-candy-pop-decke-und-kissen.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Stylecraft Special DK No. F003 Candy Pop Decke und Kissen" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/40361/haekelanleitung-stylecraft-special-dk-no-f003-candy-pop-decke-und-kissen' title='Häkelanleitung Stylecraft Special DK No. F003 Candy Pop Decke und Kissen'>Häkelanleitung Stylecraft Special DK No. F003 Candy Pop Decke und Kissen</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40361' data-productcode='ghpstF003' data-productname='Häkelanleitung Stylecraft Special DK No. F003 Candy Pop Decke und Kissen' data-productprice='0.00' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitungen/40361/haekelanleitung-stylecraft-special-dk-no-f003-candy-pop-decke-und-kissen' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40367' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='ghpstF006' data-productname='Häkelanleitung Stylecraft Special DK No. F006 Kissen' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-f006-kissen' title='Häkelanleitung Stylecraft Special DK No. F006 Kissen' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/f006-0_hoofdafbeelding_15688763190532.jpg/250/250/True/haekelanleitung-stylecraft-special-dk-no-f006-kissen.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Stylecraft Special DK No. F006 Kissen" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-f006-kissen' title='Häkelanleitung Stylecraft Special DK No. F006 Kissen'>Häkelanleitung Stylecraft Special DK No. F006 Kissen</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40367' data-productcode='ghpstF006' data-productname='Häkelanleitung Stylecraft Special DK No. F006 Kissen' data-productprice='0.00' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-f006-kissen' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40368' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='ghpstF019' data-productname='Häkelanleitung Stylecraft Special DK No. F019 Decke' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-f019-decke' title='Häkelanleitung Stylecraft Special DK No. F019 Decke' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/f019-1_hoofdafbeelding_15688763190637.jpg/250/250/True/haekelanleitung-stylecraft-special-dk-no-f019-decke.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Stylecraft Special DK No. F019 Decke" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-f019-decke' title='Häkelanleitung Stylecraft Special DK No. F019 Decke'>Häkelanleitung Stylecraft Special DK No. F019 Decke</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40368' data-productcode='ghpstF019' data-productname='Häkelanleitung Stylecraft Special DK No. F019 Decke' data-productprice='0.00' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-f019-decke' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40371' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='ghpstF067' data-productname='Häkelanleitung Stylecraft Special DK No. F067 Schal' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-f067-schal' title='Häkelanleitung Stylecraft Special DK No. F067 Schal' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/f067-0_hoofdafbeelding_15688763190906.jpg/250/250/True/haekelanleitung-stylecraft-special-dk-no-f067-schal.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Stylecraft Special DK No. F067 Schal" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-f067-schal' title='Häkelanleitung Stylecraft Special DK No. F067 Schal'>Häkelanleitung Stylecraft Special DK No. F067 Schal</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40371' data-productcode='ghpstF067' data-productname='Häkelanleitung Stylecraft Special DK No. F067 Schal' data-productprice='0.00' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-dk-no-f067-schal' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40373' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='ghpstF093' data-productname='Häkelanleitung Stylecraft Special DK No. F093 Narzisse' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/artikel/40373/haekelanleitung-stylecraft-special-dk-no-f093-narzisse.html' title='Häkelanleitung Stylecraft Special DK No. F093 Narzisse' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/f093-0_hoofdafbeelding_15688763193820.jpg/250/250/True/haekelanleitung-stylecraft-special-dk-no-f093-narzisse.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Stylecraft Special DK No. F093 Narzisse" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/artikel/40373/haekelanleitung-stylecraft-special-dk-no-f093-narzisse.html' title='Häkelanleitung Stylecraft Special DK No. F093 Narzisse'>Häkelanleitung Stylecraft Special DK No. F093 Narzisse</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40373' data-productcode='ghpstF093' data-productname='Häkelanleitung Stylecraft Special DK No. F093 Narzisse' data-productprice='0.00' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/artikel/40373/haekelanleitung-stylecraft-special-dk-no-f093-narzisse.html' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40375' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='ghpst9667' data-productname='Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9667 Jessie die Puppe' data-productbrand='Stylecraft' data-productbarcode='5034533073625'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-batik-bellissima-dk-no-9667-jessie-die-puppe' title='Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9667 Jessie die Puppe' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9667-1_zoom_15688763194069.jpg/250/250/True/haekelanleitung-stylecraft-special-batik-bellissima-dk-no-9667-jessie-die-puppe.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9667 Jessie die Puppe" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-batik-bellissima-dk-no-9667-jessie-die-puppe' title='Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9667 Jessie die Puppe'>Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9667 Jessie die Puppe</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40375' data-productcode='ghpst9667' data-productname='Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9667 Jessie die Puppe' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-batik-bellissima-dk-no-9667-jessie-die-puppe' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40376' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='ghpst9669' data-productname='Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9669 Bruno der Bär' data-productbrand='Stylecraft' data-productbarcode='5034533073649'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-batik-bellissima-dk-no-9669-bruno-der-baer' title='Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9669 Bruno der Bär' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9669-1_zoom_15688763194152.jpg/250/250/True/haekelanleitung-stylecraft-special-batik-bellissima-dk-no-9669-bruno-der-baer.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9669 Bruno der Bär" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-batik-bellissima-dk-no-9669-bruno-der-baer' title='Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9669 Bruno der Bär'>Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9669 Bruno der Bär</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40376' data-productcode='ghpst9669' data-productname='Häkelanleitung Stylecraft Special, Batik & Bellissima DK No. 9669 Bruno der Bär' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-stylecraft-special-batik-bellissima-dk-no-9669-bruno-der-baer' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-28610' class='innerproductlist' data-qty='1,0000' data-productprice='0.83' data-productcode='ghp0000513de' data-productname='Häkelanleitung Wohndecke In Love We Grow' data-productbrand='Wolplein' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitung-wohndecke-in-love-we-grow' title='Häkelanleitung Wohndecke In Love We Grow' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/sfeer3_9.jpg/250/250/True/haekelanleitung-wohndecke-in-love-we-grow.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Wohndecke In Love We Grow" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitung-wohndecke-in-love-we-grow' title='Häkelanleitung Wohndecke In Love We Grow'>Häkelanleitung Wohndecke In Love We Grow</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,98</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='28610' data-productcode='ghp0000513de' data-productname='Häkelanleitung Wohndecke In Love We Grow' data-productprice='0.83' data-productbrand='Wolplein' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitung-wohndecke-in-love-we-grow' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-28609' class='innerproductlist' data-qty='1,0000' data-productprice='0.83' data-productcode='ghp0000511de' data-productname='Häkelanleitung Wohndecke Siem' data-productbrand='Wolplein' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitung-wohndecke-siem' title='Häkelanleitung Wohndecke Siem' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/15_sfeer_02_2.jpg/250/250/True/haekelanleitung-wohndecke-siem.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung Wohndecke Siem" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitung-wohndecke-siem' title='Häkelanleitung Wohndecke Siem'>Häkelanleitung Wohndecke Siem</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,98</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='28609' data-productcode='ghp0000511de' data-productname='Häkelanleitung Wohndecke Siem' data-productprice='0.83' data-productbrand='Wolplein' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitung-wohndecke-siem' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-20660' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='ghp000225de' data-productname='Häkelanleitung XL Kaktus' data-productbrand='Wolplein' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-xl-kaktus' title='Häkelanleitung XL Kaktus' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/11_17_10.jpg/250/250/True/haekelanleitung-xl-kaktus.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Häkelanleitung XL Kaktus" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-xl-kaktus' title='Häkelanleitung XL Kaktus'>Häkelanleitung XL Kaktus</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='20660' data-productcode='ghp000225de' data-productname='Häkelanleitung XL Kaktus' data-productprice='0.00' data-productbrand='Wolplein' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/haekelanleitungen/haekelanleitung-xl-kaktus' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-39848' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9356' data-productname='Strickanleitung Stylecraft Batik & Special DK No. 9356 Sleepy Sheep' data-productbrand='Stylecraft' data-productbarcode='5034533070310'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-batik-special-dk-no-9356-sleepy-sheep' title='Strickanleitung Stylecraft Batik & Special DK No. 9356 Sleepy Sheep' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9356-1_zoom_10682513178130.jpg/250/250/True/strickanleitung-stylecraft-batik-special-dk-no-9356-sleepy-sheep.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Batik & Special DK No. 9356 Sleepy Sheep" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-batik-special-dk-no-9356-sleepy-sheep' title='Strickanleitung Stylecraft Batik & Special DK No. 9356 Sleepy Sheep'>Strickanleitung Stylecraft Batik & Special DK No. 9356 Sleepy Sheep</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='39848' data-productcode='gbpst9356' data-productname='Strickanleitung Stylecraft Batik & Special DK No. 9356 Sleepy Sheep' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-batik-special-dk-no-9356-sleepy-sheep' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40348' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9355' data-productname='Strickanleitung Stylecraft Special & Batik DK No. 9355 Kuschel Kaninchen und Mütze' data-productbrand='Stylecraft' data-productbarcode='5034533070303'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-batik-dk-no-9355-kuschel-kaninchen-und-muetze' title='Strickanleitung Stylecraft Special & Batik DK No. 9355 Kuschel Kaninchen und Mütze' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9355-1_hoofdafbeelding_15688763184447.jpg/250/250/True/strickanleitung-stylecraft-special-batik-dk-no-9355-kuschel-kaninchen-und-muetze.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special & Batik DK No. 9355 Kuschel Kaninchen und Mütze" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-batik-dk-no-9355-kuschel-kaninchen-und-muetze' title='Strickanleitung Stylecraft Special & Batik DK No. 9355 Kuschel Kaninchen und Mütze'>Strickanleitung Stylecraft Special & Batik DK No. 9355 Kuschel Kaninchen und Mütze</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40348' data-productcode='gbpst9355' data-productname='Strickanleitung Stylecraft Special & Batik DK No. 9355 Kuschel Kaninchen und Mütze' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-batik-dk-no-9355-kuschel-kaninchen-und-muetze' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40344' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst8902' data-productname='Strickanleitung Stylecraft Special DK No. 8902 Mädchen Cardigans' data-productbrand='Stylecraft' data-productbarcode='5034533053153'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-8902-maedchen-cardigans' title='Strickanleitung Stylecraft Special DK No. 8902 Mädchen Cardigans' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/8902-1_15688763183571.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-8902-maedchen-cardigans.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 8902 Mädchen Cardigans" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-8902-maedchen-cardigans' title='Strickanleitung Stylecraft Special DK No. 8902 Mädchen Cardigans'>Strickanleitung Stylecraft Special DK No. 8902 Mädchen Cardigans</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40344' data-productcode='gbpst8902' data-productname='Strickanleitung Stylecraft Special DK No. 8902 Mädchen Cardigans' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-8902-maedchen-cardigans' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40345' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9308' data-productname='Strickanleitung Stylecraft Special DK No. 9308 Pullover und Mütze' data-productbrand='Stylecraft' data-productbarcode='5034533069819'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9308-pullover-und-muetze' title='Strickanleitung Stylecraft Special DK No. 9308 Pullover und Mütze' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9308-0_hoofdafbeelding_15688763183714.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9308-pullover-und-muetze.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9308 Pullover und Mütze" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9308-pullover-und-muetze' title='Strickanleitung Stylecraft Special DK No. 9308 Pullover und Mütze'>Strickanleitung Stylecraft Special DK No. 9308 Pullover und Mütze</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40345' data-productcode='gbpst9308' data-productname='Strickanleitung Stylecraft Special DK No. 9308 Pullover und Mütze' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9308-pullover-und-muetze' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40347' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9311' data-productname='Strickanleitung Stylecraft Special DK No. 9311 Hundejacke' data-productbrand='Stylecraft' data-productbarcode='5034533069840'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9311-hundejacke' title='Strickanleitung Stylecraft Special DK No. 9311 Hundejacke' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9311-1_hoodfafbeelding_15688763184253.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9311-hundejacke.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9311 Hundejacke" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9311-hundejacke' title='Strickanleitung Stylecraft Special DK No. 9311 Hundejacke'>Strickanleitung Stylecraft Special DK No. 9311 Hundejacke</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40347' data-productcode='gbpst9311' data-productname='Strickanleitung Stylecraft Special DK No. 9311 Hundejacke' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9311-hundejacke' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40349' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9393' data-productname='Strickanleitung Stylecraft Special DK No. 9393 Pullover und Top' data-productbrand='Stylecraft' data-productbarcode='5034533070679'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9393-pullover-und-top' title='Strickanleitung Stylecraft Special DK No. 9393 Pullover und Top' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9393-0_samen_15688763184694.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9393-pullover-und-top.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9393 Pullover und Top" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9393-pullover-und-top' title='Strickanleitung Stylecraft Special DK No. 9393 Pullover und Top'>Strickanleitung Stylecraft Special DK No. 9393 Pullover und Top</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40349' data-productcode='gbpst9393' data-productname='Strickanleitung Stylecraft Special DK No. 9393 Pullover und Top' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9393-pullover-und-top' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40350' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9396' data-productname='Strickanleitung Stylecraft Special DK No. 9396 Jacke und Pullover' data-productbrand='Stylecraft' data-productbarcode='5034533070709'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9396-jacke-und-pullover' title='Strickanleitung Stylecraft Special DK No. 9396 Jacke und Pullover' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9396-0_samen_15688763184780.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9396-jacke-und-pullover.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9396 Jacke und Pullover" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9396-jacke-und-pullover' title='Strickanleitung Stylecraft Special DK No. 9396 Jacke und Pullover'>Strickanleitung Stylecraft Special DK No. 9396 Jacke und Pullover</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40350' data-productcode='gbpst9396' data-productname='Strickanleitung Stylecraft Special DK No. 9396 Jacke und Pullover' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9396-jacke-und-pullover' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40351' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9399' data-productname='Strickanleitung Stylecraft Special DK No. 9399 Mädchenkleid und Tunika' data-productbrand='Stylecraft' data-productbarcode='5034533070730'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9399-maedchenkleid-und-tunika' title='Strickanleitung Stylecraft Special DK No. 9399 Mädchenkleid und Tunika' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9399-0_samen_15688763184891.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9399-maedchenkleid-und-tunika.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9399 Mädchenkleid und Tunika" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9399-maedchenkleid-und-tunika' title='Strickanleitung Stylecraft Special DK No. 9399 Mädchenkleid und Tunika'>Strickanleitung Stylecraft Special DK No. 9399 Mädchenkleid und Tunika</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40351' data-productcode='gbpst9399' data-productname='Strickanleitung Stylecraft Special DK No. 9399 Mädchenkleid und Tunika' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9399-maedchenkleid-und-tunika' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40352' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9640' data-productname='Strickanleitung Stylecraft Special DK No. 9640 Pullover' data-productbrand='Stylecraft' data-productbarcode='5034533073359'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9640-pullover' title='Strickanleitung Stylecraft Special DK No. 9640 Pullover' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9640-0_samen_15688763187510.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9640-pullover.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9640 Pullover" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9640-pullover' title='Strickanleitung Stylecraft Special DK No. 9640 Pullover'>Strickanleitung Stylecraft Special DK No. 9640 Pullover</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40352' data-productcode='gbpst9640' data-productname='Strickanleitung Stylecraft Special DK No. 9640 Pullover' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9640-pullover' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40353' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9641' data-productname='Strickanleitung Stylecraft Special DK No. 9641 Pullover und Jacke' data-productbrand='Stylecraft' data-productbarcode='5034533073366'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9641-pullover-und-jacke' title='Strickanleitung Stylecraft Special DK No. 9641 Pullover und Jacke' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9641-0_samen_15688763187627.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9641-pullover-und-jacke.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9641 Pullover und Jacke" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9641-pullover-und-jacke' title='Strickanleitung Stylecraft Special DK No. 9641 Pullover und Jacke'>Strickanleitung Stylecraft Special DK No. 9641 Pullover und Jacke</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40353' data-productcode='gbpst9641' data-productname='Strickanleitung Stylecraft Special DK No. 9641 Pullover und Jacke' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9641-pullover-und-jacke' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40354' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9642' data-productname='Strickanleitung Stylecraft Special DK No. 9642 Pullover' data-productbrand='Stylecraft' data-productbarcode='5034533073373'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9642-pullover' title='Strickanleitung Stylecraft Special DK No. 9642 Pullover' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9642-0_samen_15688763187723.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9642-pullover.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9642 Pullover" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9642-pullover' title='Strickanleitung Stylecraft Special DK No. 9642 Pullover'>Strickanleitung Stylecraft Special DK No. 9642 Pullover</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40354' data-productcode='gbpst9642' data-productname='Strickanleitung Stylecraft Special DK No. 9642 Pullover' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9642-pullover' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40355' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9643' data-productname='Strickanleitung Stylecraft Special DK No. 9643 Jacke und Pullover' data-productbrand='Stylecraft' data-productbarcode='5034533073380'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9643-jacke-und-pullover' title='Strickanleitung Stylecraft Special DK No. 9643 Jacke und Pullover' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9643-0_samen_15688763187848.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9643-jacke-und-pullover.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9643 Jacke und Pullover" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9643-jacke-und-pullover' title='Strickanleitung Stylecraft Special DK No. 9643 Jacke und Pullover'>Strickanleitung Stylecraft Special DK No. 9643 Jacke und Pullover</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40355' data-productcode='gbpst9643' data-productname='Strickanleitung Stylecraft Special DK No. 9643 Jacke und Pullover' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9643-jacke-und-pullover' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40356' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9644' data-productname='Strickanleitung Stylecraft Special DK No. 9644 Jacke und Pullover' data-productbrand='Stylecraft' data-productbarcode='5034533073397'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9644-jacke-und-pullover' title='Strickanleitung Stylecraft Special DK No. 9644 Jacke und Pullover' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9644-0_samen_15688763188006.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9644-jacke-und-pullover.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9644 Jacke und Pullover" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9644-jacke-und-pullover' title='Strickanleitung Stylecraft Special DK No. 9644 Jacke und Pullover'>Strickanleitung Stylecraft Special DK No. 9644 Jacke und Pullover</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40356' data-productcode='gbpst9644' data-productname='Strickanleitung Stylecraft Special DK No. 9644 Jacke und Pullover' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9644-jacke-und-pullover' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40359' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9762' data-productname='Strickanleitung Stylecraft Special DK No. 9762 Pullover und Mützen' data-productbrand='Stylecraft' data-productbarcode='5034533074585'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9762-pullover-und-muetzen' title='Strickanleitung Stylecraft Special DK No. 9762 Pullover und Mützen' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9762-0_samen_15688763188940.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9762-pullover-und-muetzen.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9762 Pullover und Mützen" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9762-pullover-und-muetzen' title='Strickanleitung Stylecraft Special DK No. 9762 Pullover und Mützen'>Strickanleitung Stylecraft Special DK No. 9762 Pullover und Mützen</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40359' data-productcode='gbpst9762' data-productname='Strickanleitung Stylecraft Special DK No. 9762 Pullover und Mützen' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9762-pullover-und-muetzen' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40360' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9763' data-productname='Strickanleitung Stylecraft Special DK No. 9763 Pullover und Mützen' data-productbrand='Stylecraft' data-productbarcode='5034533074592'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9763-pullover-und-muetzen' title='Strickanleitung Stylecraft Special DK No. 9763 Pullover und Mützen' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9763-0_samen_15688763189023.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9763-pullover-und-muetzen.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9763 Pullover und Mützen" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9763-pullover-und-muetzen' title='Strickanleitung Stylecraft Special DK No. 9763 Pullover und Mützen'>Strickanleitung Stylecraft Special DK No. 9763 Pullover und Mützen</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40360' data-productcode='gbpst9763' data-productname='Strickanleitung Stylecraft Special DK No. 9763 Pullover und Mützen' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9763-pullover-und-muetzen' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40362' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9764' data-productname='Strickanleitung Stylecraft Special DK No. 9764 Cardigans' data-productbrand='Stylecraft' data-productbarcode='5034533074608'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9764-cardigans' title='Strickanleitung Stylecraft Special DK No. 9764 Cardigans' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9764-0_samen_15688763189148.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9764-cardigans.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9764 Cardigans" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9764-cardigans' title='Strickanleitung Stylecraft Special DK No. 9764 Cardigans'>Strickanleitung Stylecraft Special DK No. 9764 Cardigans</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40362' data-productcode='gbpst9764' data-productname='Strickanleitung Stylecraft Special DK No. 9764 Cardigans' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9764-cardigans' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40363' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9765' data-productname='Strickanleitung Stylecraft Special DK No. 9765 Mützen und Schals' data-productbrand='Stylecraft' data-productbarcode='5034533074615'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9765-muetzen-und-schals' title='Strickanleitung Stylecraft Special DK No. 9765 Mützen und Schals' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9765-0_samen_15688763189388.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9765-muetzen-und-schals.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9765 Mützen und Schals" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9765-muetzen-und-schals' title='Strickanleitung Stylecraft Special DK No. 9765 Mützen und Schals'>Strickanleitung Stylecraft Special DK No. 9765 Mützen und Schals</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40363' data-productcode='gbpst9765' data-productname='Strickanleitung Stylecraft Special DK No. 9765 Mützen und Schals' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9765-muetzen-und-schals' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40364' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9766' data-productname='Strickanleitung Stylecraft Special DK No. 9766 Jacken und Mützen' data-productbrand='Stylecraft' data-productbarcode='5034533074622'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9766-jacken-und-muetzen' title='Strickanleitung Stylecraft Special DK No. 9766 Jacken und Mützen' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9766-1_15688763189468.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9766-jacken-und-muetzen.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9766 Jacken und Mützen" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9766-jacken-und-muetzen' title='Strickanleitung Stylecraft Special DK No. 9766 Jacken und Mützen'>Strickanleitung Stylecraft Special DK No. 9766 Jacken und Mützen</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40364' data-productcode='gbpst9766' data-productname='Strickanleitung Stylecraft Special DK No. 9766 Jacken und Mützen' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9766-jacken-und-muetzen' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40365' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9767' data-productname='Strickanleitung Stylecraft Special DK No. 9767 Pullover und Mützen' data-productbrand='Stylecraft' data-productbarcode='5034533074639'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9767-pullover-und-muetzen' title='Strickanleitung Stylecraft Special DK No. 9767 Pullover und Mützen' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9767-1_15688763189567.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-9767-pullover-und-muetzen.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. 9767 Pullover und Mützen" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9767-pullover-und-muetzen' title='Strickanleitung Stylecraft Special DK No. 9767 Pullover und Mützen'>Strickanleitung Stylecraft Special DK No. 9767 Pullover und Mützen</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40365' data-productcode='gbpst9767' data-productname='Strickanleitung Stylecraft Special DK No. 9767 Pullover und Mützen' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-9767-pullover-und-muetzen' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40366' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='gbpstF005' data-productname='Strickanleitung Stylecraft Special DK No. F005 Fair Isle Kissen' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f005-fair-isle-kissen' title='Strickanleitung Stylecraft Special DK No. F005 Fair Isle Kissen' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/f005-0_hoofdafbeelding_15688763190401.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-f005-fair-isle-kissen.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. F005 Fair Isle Kissen" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f005-fair-isle-kissen' title='Strickanleitung Stylecraft Special DK No. F005 Fair Isle Kissen'>Strickanleitung Stylecraft Special DK No. F005 Fair Isle Kissen</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40366' data-productcode='gbpstF005' data-productname='Strickanleitung Stylecraft Special DK No. F005 Fair Isle Kissen' data-productprice='0.00' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f005-fair-isle-kissen' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40369' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='gbpstF033' data-productname='Strickanleitung Stylecraft Special DK No. F033 Herzen' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f033-herzen' title='Strickanleitung Stylecraft Special DK No. F033 Herzen' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/f033-0_hoofdafbeelding_15688763190720.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-f033-herzen.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. F033 Herzen" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f033-herzen' title='Strickanleitung Stylecraft Special DK No. F033 Herzen'>Strickanleitung Stylecraft Special DK No. F033 Herzen</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40369' data-productcode='gbpstF033' data-productname='Strickanleitung Stylecraft Special DK No. F033 Herzen' data-productprice='0.00' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f033-herzen' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40370' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='gbpstF066' data-productname='Strickanleitung Stylecraft Special DK No. F066 Schal' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f066-schal' title='Strickanleitung Stylecraft Special DK No. F066 Schal' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/f066-0_hoofdafbeelding_15688763190826.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-f066-schal.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. F066 Schal" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f066-schal' title='Strickanleitung Stylecraft Special DK No. F066 Schal'>Strickanleitung Stylecraft Special DK No. F066 Schal</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40370' data-productcode='gbpstF066' data-productname='Strickanleitung Stylecraft Special DK No. F066 Schal' data-productprice='0.00' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f066-schal' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40372' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='gbpstF071' data-productname='Strickanleitung Stylecraft Special DK No. F071 Mini Pullover' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f071-mini-pullover' title='Strickanleitung Stylecraft Special DK No. F071 Mini Pullover' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/f071-0_hoofdafbeelding_15688763191208.jpg/250/250/True/strickanleitung-stylecraft-special-dk-no-f071-mini-pullover.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special DK No. F071 Mini Pullover" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f071-mini-pullover' title='Strickanleitung Stylecraft Special DK No. F071 Mini Pullover'>Strickanleitung Stylecraft Special DK No. F071 Mini Pullover</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40372' data-productcode='gbpstF071' data-productname='Strickanleitung Stylecraft Special DK No. F071 Mini Pullover' data-productprice='0.00' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-dk-no-f071-mini-pullover' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40357' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9668' data-productname='Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9668 Jessie die Puppe' data-productbrand='Stylecraft' data-productbarcode='5034533073632'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-batik-bellissima-dk-no-9668-jessie-die-puppe' title='Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9668 Jessie die Puppe' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9668-1_zoom_15688763188206.jpg/250/250/True/strickanleitung-stylecraft-special-batik-bellissima-dk-no-9668-jessie-die-puppe.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9668 Jessie die Puppe" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-batik-bellissima-dk-no-9668-jessie-die-puppe' title='Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9668 Jessie die Puppe'>Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9668 Jessie die Puppe</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40357' data-productcode='gbpst9668' data-productname='Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9668 Jessie die Puppe' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-batik-bellissima-dk-no-9668-jessie-die-puppe' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-40358' class='innerproductlist' data-qty='1,0000' data-productprice='3.26' data-productcode='gbpst9670' data-productname='Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9670 Bruno der Bär' data-productbrand='Stylecraft' data-productbarcode='5034533073656'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-batik-bellissima-dk-no-9670-bruno-der-baer' title='Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9670 Bruno der Bär' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/9670-1_zoom_15688763188773.jpg/250/250/True/strickanleitung-stylecraft-special-batik-bellissima-dk-no-9670-bruno-der-baer.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9670 Bruno der Bär" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-batik-bellissima-dk-no-9670-bruno-der-baer' title='Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9670 Bruno der Bär'>Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9670 Bruno der Bär</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>3,88</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='40358' data-productcode='gbpst9670' data-productname='Strickanleitung Stylecraft Special, Batik & Bellissima DK No. 9670 Bruno der Bär' data-productprice='3.26' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/strickanleitungen/strickanleitung-stylecraft-special-batik-bellissima-dk-no-9670-bruno-der-baer' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist20'>
<div id='pid-22018' class='innerproductlist' data-qty='1,0000' data-productprice='0.00' data-productcode='ghp0000522de' data-productname='Tiger Alfred Häkelanleitung' data-productbrand='Wolplein' data-productbarcode=''>
<a href='https://www.wollplatz.de/anleitungen/tiger-alfred-haekelanleitung' title='Tiger Alfred Häkelanleitung' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/600x600-7448e79f-16c2-411f-92c6-29a1b124394a_17557512602216.jpg/250/250/True/tiger-alfred-haekelanleitung.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Tiger Alfred Häkelanleitung" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/anleitungen/tiger-alfred-haekelanleitung' title='Tiger Alfred Häkelanleitung'>Tiger Alfred Häkelanleitung</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>0,00</span>
</span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span  data-productid='22018' data-productcode='ghp0000522de' data-productname='Tiger Alfred Häkelanleitung' data-productprice='0.00' data-productbrand='Wolplein' data-qty='1' data-url='https://www.wollplatz.de/anleitungen/tiger-alfred-haekelanleitung' onclick="AddToCart(this,true);">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
</div>

        
	</div>

        <div id="ContentPlaceHolder1_pnlProductRelatedItemHolder" class="pdetail-gerelaterholder related-items-full-width">
		
            


            <div id="ContentPlaceHolder1_pnlRelatedItems" class="gerelateerdfix">
			
                <div class="innergerelateerdholder">
                    <h3 id="h3RelatedArticles">
                        Themenverwandte Artikel</h3>
                </div>
                <div class='box pcw100 pcs0020 tabw100 tabs0020 mobw100 mobs0020 element artikelen-element slide3'>
<div class='productlistholder productlist16'>
<div id='pid-24372' class='innerproductlist' data-qty='1,0000' data-productprice='7.02' data-productcode='nldclhaak' data-productname='Clover Amour Häkelnadel' data-productbrand='Clover' data-productbarcode=''>
<a href='https://www.wollplatz.de/nadeln/haekelnadeln/clover-amour-hakelnadel' title='Clover Amour Häkelnadel' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/clover-amour-haaknaald-3-22.jpg/250/250/True/clover-amour-haekelnadel.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Clover Amour Häkelnadel" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/nadeln/haekelnadeln/clover-amour-hakelnadel' title='Clover Amour Häkelnadel'>Clover Amour Häkelnadel</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>
<del>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>8,36</span>
</del> 
<span class='productlist-cheaper'><span>-</span>24%</span>
</span>
</span>
<span class='st-price'>Ab </span><span class="productlist-price"><span class='product-price-currency'>€ </span><span class='product-price-amount'>6,39</span></span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='24372' data-productcode='nldclhaak' data-productname='Clover Amour Häkelnadel' data-productprice='7.02' data-productbrand='Clover' data-qty='1' data-url='https://www.wollplatz.de/nadeln/haekelnadeln/clover-amour-hakelnadel' onclick="OpenLinkWithQty(this, '24372');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-22845' class='innerproductlist' data-qty='1,0000' data-productprice='40.54' data-productcode='hp1000411en' data-productname='Delft Decke Special DK Häkelpaket' data-productbrand='Stylecraft' data-productbarcode='2990000082107'>
<a href='https://www.wollplatz.de/pakete/haekelpakete/delft-decke-special-dk-haekelpaket' title='Delft Decke Special DK Häkelpaket' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/img_9513_3.jpg/250/250/True/delft-decke-special-dk-haekelpaket.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Delft Decke Special DK Häkelpaket" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/pakete/haekelpakete/delft-decke-special-dk-haekelpaket' title='Delft Decke Special DK Häkelpaket'>Delft Decke Special DK Häkelpaket</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='composedProductPrice' data-productid='22845' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='22845' data-productcode='hp1000411en' data-productname='Delft Decke Special DK Häkelpaket' data-productprice='40.54' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/pakete/haekelpakete/delft-decke-special-dk-haekelpaket' onclick="OpenLinkWithQty(this, '22845');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-38637' class='innerproductlist' data-qty='1,0000' data-productprice='35.25' data-productcode='hp10001598en' data-productname='Persian Tiles Decke Häkelpaket' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/pakete/haekelpakete/persian-tiles-decke-haekelpaket' title='Persian Tiles Decke Häkelpaket' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/img_5327_11938763201015.jpg/250/250/True/persian-tiles-decke-haekelpaket.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Persian Tiles Decke Häkelpaket" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/pakete/haekelpakete/persian-tiles-decke-haekelpaket' title='Persian Tiles Decke Häkelpaket'>Persian Tiles Decke Häkelpaket</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='variantProductPrice' data-productid='38637' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='38637' data-productcode='hp10001598en' data-productname='Persian Tiles Decke Häkelpaket' data-productprice='35.25' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/pakete/haekelpakete/persian-tiles-decke-haekelpaket' onclick="OpenLinkWithQty(this, '38637');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-23211' class='innerproductlist' data-qty='1,0000' data-productprice='45.50' data-productcode='hp1000943' data-productname='Special Square Decke Häkelpaket' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/pakete/haekelpakete/special-square-decke-haekelpaket' title='Special Square Decke Häkelpaket' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/img_9785_oranje.jpg/250/250/True/special-square-decke-haekelpaket.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Special Square Decke Häkelpaket" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/pakete/haekelpakete/special-square-decke-haekelpaket' title='Special Square Decke Häkelpaket'>Special Square Decke Häkelpaket</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='variantProductPrice' data-productid='23211' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='23211' data-productcode='hp1000943' data-productname='Special Square Decke Häkelpaket' data-productprice='45.50' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/pakete/haekelpakete/special-square-decke-haekelpaket' onclick="OpenLinkWithQty(this, '23211');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-42373' class='innerproductlist' data-qty='1,0000' data-productprice='37.77' data-productcode='gp10001709' data-productname='Stylecraft Frida&apos;s Flowers CAL Original Special DK Garnpaket' data-productbrand='Stylecraft' data-productbarcode='2990000133724'>
<a href='https://www.wollplatz.de/pakete/garnpakete/stylecraft-frida-s-flowers-cal-original-special-dk-garnpaket' title='Stylecraft Frida's Flowers CAL Original Special DK Garnpaket' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/afbeelding-1-1000x1000_17507513839508.jpg/250/250/True/stylecraft-frida-s-flowers-cal-original-special-dk-garnpaket.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Frida&apos;s Flowers CAL Original Special DK Garnpaket" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/pakete/garnpakete/stylecraft-frida-s-flowers-cal-original-special-dk-garnpaket' title='Stylecraft Frida&apos;s Flowers CAL Original Special DK Garnpaket'>Stylecraft Frida's Flowers CAL Original Special DK Garnpaket</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>44,95</span>
</span>


</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='42373' data-productcode='gp10001709' data-productname='Stylecraft Frida&apos;s Flowers CAL Original Special DK Garnpaket' data-productprice='37.77' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/pakete/garnpakete/stylecraft-frida-s-flowers-cal-original-special-dk-garnpaket' onclick="OpenLinkWithQty(this, '42373');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-42374' class='innerproductlist' data-qty='1,0000' data-productprice='69.71' data-productcode='gp10001710' data-productname='Stylecraft Frida’s Flowers CAL Primavera Garnpaket' data-productbrand='Stylecraft' data-productbarcode='2990000133731'>
<a href='https://www.wollplatz.de/pakete/garnpakete/stylecraft-fridas-flowers-cal-primavera-garnpaket' title='Stylecraft Frida’s Flowers CAL Primavera Garnpaket' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/afbeelding-1-1000x1000_17507513839523.jpg/250/250/True/stylecraft-fridas-flowers-cal-primavera-garnpaket.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Frida’s Flowers CAL Primavera Garnpaket" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/pakete/garnpakete/stylecraft-fridas-flowers-cal-primavera-garnpaket' title='Stylecraft Frida’s Flowers CAL Primavera Garnpaket'>Stylecraft Frida’s Flowers CAL Primavera Garnpaket</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>82,95</span>
</span>


</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='42374' data-productcode='gp10001710' data-productname='Stylecraft Frida’s Flowers CAL Primavera Garnpaket' data-productprice='69.71' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/pakete/garnpakete/stylecraft-fridas-flowers-cal-primavera-garnpaket' onclick="OpenLinkWithQty(this, '42374');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-22836' class='innerproductlist' data-qty='1,0000' data-productprice='30.29' data-productcode='hp1000357' data-productname='Stylecraft Schal Häkelpaket' data-productbrand='Stylecraft' data-productbarcode='2990000016157'>
<a href='https://www.wollplatz.de/pakete/haekelpakete/stylecraft-schal-hakelpaket' title='Stylecraft Schal Häkelpaket' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/2_58_22.jpg/250/250/True/stylecraft-schal-haekelpaket.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Schal Häkelpaket" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/pakete/haekelpakete/stylecraft-schal-hakelpaket' title='Stylecraft Schal Häkelpaket'>Stylecraft Schal Häkelpaket</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='composedProductPrice' data-productid='22836' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='22836' data-productcode='hp1000357' data-productname='Stylecraft Schal Häkelpaket' data-productprice='30.29' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/pakete/haekelpakete/stylecraft-schal-hakelpaket' onclick="OpenLinkWithQty(this, '22836');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-37199' class='innerproductlist' data-qty='1,0000' data-productprice='239.45' data-productcode='gp10001590' data-productname='Stylecraft Special DK Alle Farben Paket' data-productbrand='Stylecraft' data-productbarcode='2990000128591'>
<a href='https://www.wollplatz.de/pakete/rabattpakete/stylecraft-special-dk-alle-farben-paket' title='Stylecraft Special DK Alle Farben Paket' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/pakket_16313763227021.jpg/250/250/True/stylecraft-special-dk-alle-farben-paket.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special DK Alle Farben Paket" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/pakete/rabattpakete/stylecraft-special-dk-alle-farben-paket' title='Stylecraft Special DK Alle Farben Paket'>Stylecraft Special DK Alle Farben Paket</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='composedProductPrice' data-productid='37199' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='37199' data-productcode='gp10001590' data-productname='Stylecraft Special DK Alle Farben Paket' data-productprice='239.45' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/pakete/rabattpakete/stylecraft-special-dk-alle-farben-paket' onclick="OpenLinkWithQty(this, '37199');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-20733' class='innerproductlist' data-qty='10,0000' data-productprice='4.09' data-productcode='fntst1961112' data-productname='Stylecraft Special dk Farbkarte' data-productbrand='Stylecraft' data-productbarcode='5034533068270'>
<a href='https://www.wollplatz.de/wolle/stylecraft/stylecraft-special-dk-farbkarte' title='Stylecraft Special dk Farbkarte' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/stylecraft-kleurenkaart.jpg/250/250/True/stylecraft-special-dk-farbkarte.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special dk Farbkarte" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/wolle/stylecraft/stylecraft-special-dk-farbkarte' title='Stylecraft Special dk Farbkarte'>Stylecraft Special dk Farbkarte</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>4,87</span>
</span>

</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-22766' class='innerproductlist' data-qty='1,0000' data-productprice='30.21' data-productcode='hp10001265' data-productname='Sunstar Decke CAL Garnpaket' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/pakete/andere-pakete/garenpakketten/sun-star-deken-cal-garenpakket' title='Sunstar Decke CAL Garnpaket' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/600x600_41d6349a-9940-4f37-ba93-790e0c0b2951_1.jpg/250/250/True/sunstar-decke-cal-garnpaket.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Sunstar Decke CAL Garnpaket" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/pakete/andere-pakete/garenpakketten/sun-star-deken-cal-garenpakket' title='Sunstar Decke CAL Garnpaket'>Sunstar Decke CAL Garnpaket</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='variantProductPrice' data-productid='22766' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='22766' data-productcode='hp10001265' data-productname='Sunstar Decke CAL Garnpaket' data-productprice='30.21' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/pakete/andere-pakete/garenpakketten/sun-star-deken-cal-garenpakket' onclick="OpenLinkWithQty(this, '22766');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-22769' class='innerproductlist' data-qty='1,0000' data-productprice='49.54' data-productcode='hp10001266' data-productname='Tessellation Decke Häkelpaket ' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/pakete/haekelpakete/tessellation-decke-haekelpaket' title='Tessellation Decke Häkelpaket ' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/600x600_styl-9683_2_1.jpg/250/250/True/tessellation-decke-haekelpaket.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Tessellation Decke Häkelpaket " class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/pakete/haekelpakete/tessellation-decke-haekelpaket' title='Tessellation Decke Häkelpaket '>Tessellation Decke Häkelpaket </a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='variantProductPrice' data-productid='22769' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='22769' data-productcode='hp10001266' data-productname='Tessellation Decke Häkelpaket ' data-productprice='49.54' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/pakete/haekelpakete/tessellation-decke-haekelpaket' onclick="OpenLinkWithQty(this, '22769');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-26426' class='innerproductlist' data-qty='5,0000' data-productprice='6.20' data-productcode='wpfnt35004xl' data-productname='Wollplatz Wollspender-Etui XL' data-productbrand='Wolplein' data-productbarcode='8720211200489'>
<a href='https://www.wollplatz.de/zubehoer/strick-haekel-zubehoer/wollplatz-wollspender-etui-xl' title='Wollplatz Wollspender-Etui XL' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/garen-etui-xl-3.jpg/250/250/True/wollplatz-wollspender-etui-xl.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Wollplatz Wollspender-Etui XL" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/zubehoer/strick-haekel-zubehoer/wollplatz-wollspender-etui-xl' title='Wollplatz Wollspender-Etui XL'>Wollplatz Wollspender-Etui XL</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<div class='productlist-priceholder'>
<span class='productlist-price'>
<span class='product-price-currency'>€ </span><span class='product-price-amount'>7,38</span>
</span>

</div>
</div>
</div>
</div>
</div>

            
		</div>
            <div id="ContentPlaceHolder1_pnlAlternatieven" class="alternatieffix related-items-full-width">
			
                <div class="inneralternatiefholder">
                    <h3>Alternative Artikel</h3>
                </div>
                <div class='box pcw100 pcs0020 tabw100 tabs0020 mobw100 mobs0020 element artikelen-element slide3'>
<div class='productlistholder productlist16'>
<div id='pid-3993' class='innerproductlist' data-qty='1,0000' data-productprice='1.03' data-productcode='bwbyadk' data-productname='Budgetyarn Acrylic DK' data-productbrand='Budgetyarn' data-productbarcode=''>
<a href='https://www.wollplatz.de/wolle/budgetyarn/budgetyarn-acrylic-dk' title='Budgetyarn Acrylic DK' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/bwbyadk_017_4407513209830.jpg/250/250/True/budgetyarn-acrylic-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Budgetyarn Acrylic DK" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/wolle/budgetyarn/budgetyarn-acrylic-dk' title='Budgetyarn Acrylic DK'>Budgetyarn Acrylic DK</a>
</h3>
<div class='productlist-stars productAveragerating'>
<span class='starholder gele-sterretjes45'><i style='font-weight:900 !important;' class='fa fa-star'></i><i style='font-weight:900 !important;' class='fa fa-star'></i><i style='font-weight:900 !important;' class='fa fa-star'></i><i style='font-weight:900 !important;' class='fa fa-star'></i><i style='font-weight:900 !important;' class='fa fa-star-half-alt'></i></span>
<a href='https://www.wollplatz.de/wolle/budgetyarn/budgetyarn-acrylic-dk#revtitlelnk' title='Bekijk de reviews'>2 reviews</a></div>
<div class='productlist-usplist'>
<ul>
<li>
100% Acryl</li><li>Light</li>
</ul>
</div>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='variantProductPrice' data-productid='3993' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='3993' data-productcode='bwbyadk' data-productname='Budgetyarn Acrylic DK' data-productprice='1.03' data-productbrand='Budgetyarn' data-qty='1' data-url='https://www.wollplatz.de/wolle/budgetyarn/budgetyarn-acrylic-dk' onclick="OpenLinkWithQty(this, '3993');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-17001' class='innerproductlist' data-qty='1,0000' data-productprice='3.30' data-productcode='bwri3981' data-productname='Rico Baby Classic dk' data-productbrand='Rico' data-productbarcode=''>
<a href='https://www.wollplatz.de/wolle/rico/rico-baby-classic-dk' title='Rico Baby Classic dk' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/bwri3981_077_18801263183503.jpg/250/250/True/rico-baby-classic-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Rico Baby Classic dk" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/wolle/rico/rico-baby-classic-dk' title='Rico Baby Classic dk'>Rico Baby Classic dk</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='variantProductPrice' data-productid='17001' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='17001' data-productcode='bwri3981' data-productname='Rico Baby Classic dk' data-productprice='3.30' data-productbrand='Rico' data-qty='1' data-url='https://www.wollplatz.de/wolle/rico/rico-baby-classic-dk' onclick="OpenLinkWithQty(this, '17001');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-51795' class='innerproductlist' data-qty='1,0000' data-productprice='2.56' data-productcode='bwsw1680' data-productname='Scheepjes Colour Crafter' data-productbrand='Scheepjes' data-productbarcode=''>
<a href='https://www.wollplatz.de/wolle/scheepjes/scheepjes-colour-crafter' title='Scheepjes Colour Crafter' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/scheepjes-colour-crafter-lelystad_15657514428523.jpg/250/250/True/scheepjes-colour-crafter.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Scheepjes Colour Crafter" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/wolle/scheepjes/scheepjes-colour-crafter' title='Scheepjes Colour Crafter'>Scheepjes Colour Crafter</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder'>
<span class='productlist-oldprice'>
<span class='productlist-oldprice'>

</span>
</span>
<span class="productlist-price"><span class='product-price-currency'>€ </span><span class='product-price-amount'>3,05</span></span>

</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='51795' data-productcode='bwsw1680' data-productname='Scheepjes Colour Crafter' data-productprice='2.56' data-productbrand='Scheepjes' data-qty='1' data-url='https://www.wollplatz.de/wolle/scheepjes/scheepjes-colour-crafter' onclick="OpenLinkWithQty(this, '51795');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-18082' class='innerproductlist' data-qty='1,0000' data-productprice='3.51' data-productcode='bwst914' data-productname='Stylecraft Bambino DK' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/wolle/stylecraft/stylecraft-bambino-dk' title='Stylecraft Bambino DK' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/7116_1.jpg/250/250/True/stylecraft-bambino-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Bambino DK" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/wolle/stylecraft/stylecraft-bambino-dk' title='Stylecraft Bambino DK'>Stylecraft Bambino DK</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='variantProductPrice' data-productid='18082' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='18082' data-productcode='bwst914' data-productname='Stylecraft Bambino DK' data-productprice='3.51' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/wolle/stylecraft/stylecraft-bambino-dk' onclick="OpenLinkWithQty(this, '18082');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-17970' class='innerproductlist' data-qty='10,0000' data-productprice='2.73' data-productcode='bwst265' data-productname='Stylecraft Batik DK' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/wolle/stylecraft/stylecraft-batik-dk' title='Stylecraft Batik DK' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/1903_1.jpg/250/250/True/stylecraft-batik-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Batik DK" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/wolle/stylecraft/stylecraft-batik-dk' title='Stylecraft Batik DK'>Stylecraft Batik DK</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='variantProductPrice' data-productid='17970' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='17970' data-productcode='bwst265' data-productname='Stylecraft Batik DK' data-productprice='2.73' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/wolle/stylecraft/stylecraft-batik-dk' onclick="OpenLinkWithQty(this, '17970');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-17874' class='innerproductlist' data-qty='1,0000' data-productprice='4.17' data-productcode='bwst206' data-productname='Stylecraft Life DK' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/wolle/stylecraft/stylecraft-life-dk' title='Stylecraft Life DK' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/bwst206_2496_1_18807513209660.jpg/250/250/True/stylecraft-life-dk.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Life DK" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/wolle/stylecraft/stylecraft-life-dk' title='Stylecraft Life DK'>Stylecraft Life DK</a>
</h3>
<div class='productlist-stars productAveragerating'>
<span class='starholder gele-sterretjes50'><i style='font-weight:900 !important;' class='fa fa-star'></i><i style='font-weight:900 !important;' class='fa fa-star'></i><i style='font-weight:900 !important;' class='fa fa-star'></i><i style='font-weight:900 !important;' class='fa fa-star'></i><i style='font-weight:900 !important;' class='fa fa-star'></i></span>
<a href='https://www.wollplatz.de/wolle/stylecraft/stylecraft-life-dk#revtitlelnk' title='Bekijk de reviews'>1 reviews</a></div>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='variantProductPrice' data-productid='17874' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='17874' data-productcode='bwst206' data-productname='Stylecraft Life DK' data-productprice='4.17' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/wolle/stylecraft/stylecraft-life-dk' onclick="OpenLinkWithQty(this, '17874');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
<div class='productlistholder productlist16'>
<div id='pid-18069' class='innerproductlist' data-qty='1,0000' data-productprice='6.32' data-productcode='bwst910' data-productname='Stylecraft Special XL Super Chunky' data-productbrand='Stylecraft' data-productbarcode=''>
<a href='https://www.wollplatz.de/wolle/synthetisch/stylecraft-special-xl' title='Stylecraft Special XL Super Chunky' class='productlist-imgholder gtm-product-impression'>
<img src="/img/placeholder.png" data-src="https://www.wollplatz.de/resize/bwst910_1820_1_1932513197328.jpg/250/250/True/stylecraft-special-xl-super-chunky.jpg" onerror="this.src='https://www.wollplatz.de/styles/clients/wolplein-wollplatz/img/no-img.svg';this.onerror='';" alt="Stylecraft Special XL Super Chunky" class="lazyload"/>
</a>
<div class='productlist-topfx'>
<h3 class='productlist-title gtm-product-impression'>
<a href='https://www.wollplatz.de/wolle/synthetisch/stylecraft-special-xl' title='Stylecraft Special XL Super Chunky'>Stylecraft Special XL Super Chunky</a>
</h3>
</div>
<div class='productlist-bottomfx'>
<span class='productlist-oldprice'>
</span>
<div class='productlist-priceholder htmllazyload' data-function='variantProductPrice' data-productid='18069' data-listtype='productlist'>
<div class='spinnercontainer''><div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div></div>
</div>
<div class='product-buyinfoholder product-buyholderonly'>
<div class='productlist-buyholder' title='In den Warenkorb'>
<span class='gtm-product-impression' data-productid='18069' data-productcode='bwst910' data-productname='Stylecraft Special XL Super Chunky' data-productprice='6.32' data-productbrand='Stylecraft' data-qty='1' data-url='https://www.wollplatz.de/wolle/synthetisch/stylecraft-special-xl' onclick="OpenLinkWithQty(this, '18069');">
<i class='fa fa-basket'></i>
</span>

</div>
</div>
</div>
</div>
</div>
</div>

            
		</div>
        
	</div>


        <div id="ContentPlaceHolder1_pnlReviews" class="pdetail-reviewsholder">
		
            <div class="innerreviewsholder">
                
            </div>
            <div id="ContentPlaceHolder1_updatepanel_Review" class="writereviewholder">
			
                    <h3 id="reviewschrijven" class="writerev-title">Bewertung schreiben</h3>
                    <div id="ContentPlaceHolder1_pnlWrite" class="review-makebox" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;ContentPlaceHolder1_btnSendReview&#39;)">
				
                        <table>
                            <tr>
                                <td>
                                    <div class="starrating">
                                        <span>Anzahl Sterne</span>
                                        <input id="rating5" type="radio" name="ctl00$ContentPlaceHolder1$ratings" value="rating5" checked="checked" />
                                        <label for="rating5"><i class="fa fa-star-o"></i></label>
                                        <input id="rating4" type="radio" name="ctl00$ContentPlaceHolder1$ratings" value="rating4" />
                                        <label for="rating4"><i class="fa fa-star-o"></i></label>
                                        <input id="rating3" type="radio" name="ctl00$ContentPlaceHolder1$ratings" value="rating3" />
                                        <label for="rating3"><i class="fa fa-star-o"></i></label>
                                        <input id="rating2" type="radio" name="ctl00$ContentPlaceHolder1$ratings" value="rating2" />
                                        <label for="rating2"><i class="fa fa-star-o"></i></label>
                                        <input id="rating1" type="radio" name="ctl00$ContentPlaceHolder1$ratings" value="rating1" />
                                        <label for="rating1"><i class="fa fa-star-o"></i></label>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input name="ctl00$ContentPlaceHolder1$txtReviewName" type="text" maxlength="100" id="txtReviewName" class="input-gray input-revnaam" onblur="Validate_Group(this.id,&#39;validationGroupReview&#39;)" placeholder="Name" />

                                    <span id="ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName" class="hide" style="visibility:hidden;">*</span>

                                    <input name="ctl00$ContentPlaceHolder1$txtReviewEmailAddress" type="text" maxlength="100" id="txtReviewEmailAddress" class="input-gray input-revmail" onblur="Validate_Group(this.id,&#39;validationGroupReview&#39;)" placeholder="E-Mail-Adresse (bleibt geheim)" />
                                    <span id="ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress" class="hide" style="visibility:hidden;">*</span>
                                    <span id="ContentPlaceHolder1_CustomValidator_txtEmail" class="hide" style="visibility:hidden;">*</span>

                                    <input name="ctl00$ContentPlaceHolder1$txtFullNameCity" type="text" id="txtFullNameCity" tabindex="-1" class="bumbli-wrap bumbli-sol" name="full-name-city" autocomplete="tottalyvalidautocompletething" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <textarea name="ctl00$ContentPlaceHolder1$txtReviewArgument" rows="10" cols="20" id="txtReviewArgument" class="input-gray input-revtekst" onblur="Validate_Group(this.id,&#39;validationGroupReview&#39;)" placeholder="Beschreiben Sie Ihre Erfahrungen mit diesem Produkt">
</textarea>

                                    <span id="ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument" class="hide" style="visibility:hidden;">*</span>


                                    <span id="ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument" class="hide" style="visibility:hidden;">*</span>

                                    <span class="bumbli-wrap bumbli-sol">
                                        <input name="ctl00$ContentPlaceHolder1$txtCompeleteFullName" type="text" id="ContentPlaceHolder1_txtCompeleteFullName" class="bumblinumber" placeholder="full name" />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span id="ContentPlaceHolder1_CustomValidator_Recaptcha" class="hide  Recaptcha-validator" style="visibility:hidden;">*</span>

                                    <a onclick="Validate_Group(&#39;&#39;,&#39;validationGroupReview&#39;);" id="ContentPlaceHolder1_btnSendReview" class="button-plaatsreview" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btnSendReview&quot;, &quot;&quot;, true, &quot;validationGroupReview&quot;, &quot;&quot;, false, true))">Bewertung schreiben</a>
                                    <div id="ContentPlaceHolder1_divLoginError" class="error-red" style="display:none;">

				</div>
                                </td>
                            </tr>
                        </table>

                    
			</div>

                    
                    <input type="hidden" name="ctl00$ContentPlaceHolder1$reviewverzonden" id="ContentPlaceHolder1_reviewverzonden" />
                
		</div>
        
	</div>

        <div id="ContentPlaceHolder1_pnlProductBlogs" class="pdetail-blogholder">
		
            <div class="innerblogsholder">
                
            </div>
        
	</div>

        

        <div id="ContentPlaceHolder1_pnlFaqHolder" class="pdetail-faqholder">
		

            <section class="innerfaqholder">

                <h2 class='faqnoitem'>Stellen Sie eine Frage zu diesem Produkt</h2>
                

                
                    <article class="askitem">
                        <div id="ContentPlaceHolder1_upPnlFAQ">
			
                                <div id="ContentPlaceHolder1_pnlFAQQuestionWrite" class="askinputholder" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;ContentPlaceHolder1_btnQuestion&#39;)">
				
                                    <textarea name="ctl00$ContentPlaceHolder1$txtQuestion" rows="6" cols="20" id="txtQuestion" class="input-white aks-txtinput ask-vraag" placeholder="Frage" onblur="Validate_Group(this.id,&#39;ValidateQuestion&#39;)">
</textarea>
                                    <span id="ContentPlaceHolder1_RequiredFieldValidator_txtQuestion" class="hide" style="visibility:hidden;">*</span>
                                    <input name="ctl00$ContentPlaceHolder1$txtQuestionName" type="text" maxlength="100" id="txtQuestionName" class="input-white ask-txtinput ask-name" onblur="Validate_Group(this.id,&#39;ValidateQuestion&#39;)" placeholder="Name" />
                                    <span id="ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName" class="hide" style="visibility:hidden;">*</span>

                                    <input name="ctl00$ContentPlaceHolder1$txtQuestionEmailTel" type="text" maxlength="100" id="txtQuestionEmailTel" class="input-white ask-txtinput ask-mail" onblur="Validate_Group(this.id,&#39;ValidateQuestion&#39;)" placeholder="E-Mail-Adresse oder Telefonnummer" />
                                    <span id="ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel" class="hide" style="visibility:hidden;">*</span>

                                    
                                    <span id="ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail" class="hide" style="visibility:hidden;">*</span>

                                    

                                    <span class="bumbli-wrap bumbli-sol">
                                        <input name="ctl00$ContentPlaceHolder1$txtCompleteZipCode" type="text" id="ContentPlaceHolder1_txtCompleteZipCode" class="bumblinumber" placeholder="zip code" />
                                    </span>

                                    <span id="ContentPlaceHolder1_CustomValidator_Recaptcha_QuestionEmail" class="hide  Recaptcha-validator" style="visibility:hidden;">*</span>

                                    <input type="submit" name="ctl00$ContentPlaceHolder1$btnQuestion" value="Senden" onclick="Validate_Group(&#39;&#39;,&#39;ValidateQuestion&#39;);WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btnQuestion&quot;, &quot;&quot;, true, &quot;ValidateQuestion&quot;, &quot;&quot;, false, false))" id="ContentPlaceHolder1_btnQuestion" class="btn-asksend" />
                                    <div id="ContentPlaceHolder1_validationSummary" class="error-red" style="display:none;">

				</div>
                                
			</div>
                                
                            
		</div>
                    </article>
                
            </section>
        
	</div>
        

        <div id="ContentPlaceHolder1_upOptionVariant">
		
                <input type="hidden" name="ctl00$ContentPlaceHolder1$hdChosenOptions" id="ContentPlaceHolder1_hdChosenOptions" />
            
	</div>
    
<script type="text/javascript">
//<![CDATA[
var Page_ValidationSummaries =  new Array(document.getElementById("ContentPlaceHolder1_divLoginError"), document.getElementById("ContentPlaceHolder1_validationSummary"));
	var Page_Validators =  new Array(document.getElementById("ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName"), document.getElementById("ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress"), document.getElementById("ContentPlaceHolder1_CustomValidator_txtEmail"), document.getElementById("ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument"), document.getElementById("ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument"), document.getElementById("ContentPlaceHolder1_CustomValidator_Recaptcha"), document.getElementById("ContentPlaceHolder1_RequiredFieldValidator_txtQuestion"), document.getElementById("ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName"), document.getElementById("ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel"), document.getElementById("ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail"), document.getElementById("ContentPlaceHolder1_CustomValidator_Recaptcha_QuestionEmail"));
	//]]>
</script>

<script type="text/javascript">
//<![CDATA[
var ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName = document.all ? document.all["ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName"] : document.getElementById("ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName");
	ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName.controltovalidate = "txtReviewName";
	ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName.errormessage = "<label for=\'txtReviewName\'>Es ist kein <u>Name</u> eingegeben.</label>";
	ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName.validationGroup = "validationGroupReview";
	ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
	ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName.initialvalue = "";
	var ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress = document.all ? document.all["ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress"] : document.getElementById("ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress");
	ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress.controltovalidate = "txtReviewEmailAddress";
	ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress.errormessage = "<label for=\'txtReviewEmailAddress\'>Es ist keine <u>E-Mail-Adresse</u> eingegeben.</label>";
	ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress.validationGroup = "validationGroupReview";
	ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
	ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress.initialvalue = "";
	var ContentPlaceHolder1_CustomValidator_txtEmail = document.all ? document.all["ContentPlaceHolder1_CustomValidator_txtEmail"] : document.getElementById("ContentPlaceHolder1_CustomValidator_txtEmail");
	ContentPlaceHolder1_CustomValidator_txtEmail.controltovalidate = "txtReviewEmailAddress";
	ContentPlaceHolder1_CustomValidator_txtEmail.validationGroup = "validationGroupReview";
	ContentPlaceHolder1_CustomValidator_txtEmail.evaluationfunction = "CustomValidatorEvaluateIsValid";
	var ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument = document.all ? document.all["ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument"] : document.getElementById("ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument");
	ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument.controltovalidate = "txtReviewArgument";
	ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument.errormessage = "<label for=\'txtReviewArgument\'>Es ist keine <u>Umschreibung</u> eingegeben.</label>";
	ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument.validationGroup = "validationGroupReview";
	ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
	ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument.initialvalue = "";
	var ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument = document.all ? document.all["ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument"] : document.getElementById("ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument");
	ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument.controltovalidate = "txtReviewArgument";
	ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument.errormessage = "<label for=\'txtReviewArgument\'>Die <u>Umschreibung</u> muss mindestens 25 Zeichen lang sein.</label>";
	ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument.validationGroup = "validationGroupReview";
	ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";
	ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument.validationexpression = "^([\\S\\s]{25,})$";
	var ContentPlaceHolder1_CustomValidator_Recaptcha = document.all ? document.all["ContentPlaceHolder1_CustomValidator_Recaptcha"] : document.getElementById("ContentPlaceHolder1_CustomValidator_Recaptcha");
	ContentPlaceHolder1_CustomValidator_Recaptcha.errormessage = "\r\nDie Überprüfung von Google Recaptcha ist fehlgeschlagen. Wenn Sie diese Aktion dennoch ausführen möchten, kontaktieren Sie uns bitte.";
	ContentPlaceHolder1_CustomValidator_Recaptcha.validationGroup = "validationGroupReview";
	ContentPlaceHolder1_CustomValidator_Recaptcha.evaluationfunction = "CustomValidatorEvaluateIsValid";
	var ContentPlaceHolder1_divLoginError = document.all ? document.all["ContentPlaceHolder1_divLoginError"] : document.getElementById("ContentPlaceHolder1_divLoginError");
	ContentPlaceHolder1_divLoginError.headertext = "<b>Es ist ein Fehler aufgetreten!</b>";
	ContentPlaceHolder1_divLoginError.displaymode = "List";
	ContentPlaceHolder1_divLoginError.validationGroup = "validationGroupReview";
	var ContentPlaceHolder1_RequiredFieldValidator_txtQuestion = document.all ? document.all["ContentPlaceHolder1_RequiredFieldValidator_txtQuestion"] : document.getElementById("ContentPlaceHolder1_RequiredFieldValidator_txtQuestion");
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestion.controltovalidate = "txtQuestion";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestion.errormessage = "<label for=\'txtQuestion\'>Es ist keine <u>Frage</u> eingegeben.</label> ";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestion.validationGroup = "ValidateQuestion";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestion.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestion.initialvalue = "";
	var ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName = document.all ? document.all["ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName"] : document.getElementById("ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName");
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName.controltovalidate = "txtQuestionName";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName.errormessage = "<label for=\'txtQuestionName\'>Es ist kein <u>Name</u> eingegeben.</label> ";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName.validationGroup = "ValidateQuestion";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName.initialvalue = "";
	var ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel = document.all ? document.all["ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel"] : document.getElementById("ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel");
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel.controltovalidate = "txtQuestionEmailTel";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel.errormessage = "<label for=\'txtQuestionEmail\'>Es ist keine <u>E-Mail Adresse oder Telefonnummer</u> eingegeben.</label> ";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel.validationGroup = "ValidateQuestion";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel.initialvalue = "";
	var ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail = document.all ? document.all["ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail"] : document.getElementById("ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail");
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail.controltovalidate = "txtQuestionEmail";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail.errormessage = "<label for=\'txtQuestionEmail\'>Es ist keine <u>E-Mail Adresse</u> eingegeben.</label> ";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail.enabled = "False";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail.validationGroup = "ValidateQuestion";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
	ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail.initialvalue = "";
	var ContentPlaceHolder1_CustomValidator_Recaptcha_QuestionEmail = document.all ? document.all["ContentPlaceHolder1_CustomValidator_Recaptcha_QuestionEmail"] : document.getElementById("ContentPlaceHolder1_CustomValidator_Recaptcha_QuestionEmail");
	ContentPlaceHolder1_CustomValidator_Recaptcha_QuestionEmail.errormessage = "\r\nDie Überprüfung von Google Recaptcha ist fehlgeschlagen. Wenn Sie diese Aktion dennoch ausführen möchten, kontaktieren Sie uns bitte.";
	ContentPlaceHolder1_CustomValidator_Recaptcha_QuestionEmail.validationGroup = "ValidateQuestion";
	ContentPlaceHolder1_CustomValidator_Recaptcha_QuestionEmail.evaluationfunction = "CustomValidatorEvaluateIsValid";
	var ContentPlaceHolder1_validationSummary = document.all ? document.all["ContentPlaceHolder1_validationSummary"] : document.getElementById("ContentPlaceHolder1_validationSummary");
	ContentPlaceHolder1_validationSummary.headertext = "<b>Es ist ein Fehler aufgetreten!</b>";
	ContentPlaceHolder1_validationSummary.displaymode = "List";
	ContentPlaceHolder1_validationSummary.validationGroup = "ValidateQuestion";
	//]]>
</script>

	
<script type="text/javascript">
//<![CDATA[
LoadProductBarcode('');
var Page_ValidationActive = false;
if (typeof(ValidatorOnLoad) == "function") {
    ValidatorOnLoad();
}

function ValidatorOnSubmit() {
    if (Page_ValidationActive) {
        return ValidatorCommonOnSubmit();
    }
    else {
        return true;
    }
}
        
(function(id) {
    var e = document.getElementById(id);
    if (e) {
        e.dispose = function() {
            Array.remove(Page_ValidationSummaries, document.getElementById(id));
        }
        e = null;
    }
})('ContentPlaceHolder1_divLoginError');

(function(id) {
    var e = document.getElementById(id);
    if (e) {
        e.dispose = function() {
            Array.remove(Page_ValidationSummaries, document.getElementById(id));
        }
        e = null;
    }
})('ContentPlaceHolder1_validationSummary');

document.getElementById('ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ContentPlaceHolder1_ReviewRequiredFieldValidatortxtName'));
}

document.getElementById('ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ContentPlaceHolder1_RequiredFieldValidator_txtReviewEmailAddress'));
}

document.getElementById('ContentPlaceHolder1_CustomValidator_txtEmail').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ContentPlaceHolder1_CustomValidator_txtEmail'));
}

document.getElementById('ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ContentPlaceHolder1_ReviewRequiredFieldValidatortxtReviewArgument'));
}

document.getElementById('ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ContentPlaceHolder1_RegularExpressionValidator_txtReviewArgument'));
}

document.getElementById('ContentPlaceHolder1_CustomValidator_Recaptcha').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ContentPlaceHolder1_CustomValidator_Recaptcha'));
}

document.getElementById('ContentPlaceHolder1_RequiredFieldValidator_txtQuestion').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ContentPlaceHolder1_RequiredFieldValidator_txtQuestion'));
}

document.getElementById('ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ContentPlaceHolder1_RequiredFieldValidator_txtQuestionName'));
}

document.getElementById('ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmailTel'));
}

document.getElementById('ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ContentPlaceHolder1_RequiredFieldValidator_txtQuestionEmail'));
}

document.getElementById('ContentPlaceHolder1_CustomValidator_Recaptcha_QuestionEmail').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ContentPlaceHolder1_CustomValidator_Recaptcha_QuestionEmail'));
}
//]]>
</script>
</form>

    <script type="text/javascript">

        var scrollTopGridView;
        window.onload = function () {
            loadTemplateValuesForProductDetailPage();
            loadProductVariant2MatrixForProductDetailPage();
            AddViewedProductToLastViewedProductList();
            calculatePriceBasedOnDropdowns();
            LoadProductStockWarehouseInformation();

            //Case insensitive contains, gebruikt voor zoeken in artikelvariant tabel
            jQuery.extend(
                jQuery.expr[':'].containsCI = function (a, i, m) {
                    var sText = (a.textContent || a.innerText || "");
                    var zRegExp = new RegExp(m[3], 'i');
                    return zRegExp.test(sText);
                }
            );
        };


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(InitializeRequest);
        prm.add_endRequest(EndRequest);

        function InitializeRequest(sender, args) {

            
            var elem = $('.variants-group-container');
            if (elem !== null) {
                scrollTopGridView = elem.scrollTop();
            }
            
        }

        function EndRequest(sender, args) {

            
            var elem = $('.variants-group-container');
            if (elem !== null && scrollTopGridView !== null) {
                elem.scrollTop(scrollTopGridView);
            }
            
        }

        function LoadProductStockWarehouseInformation() {
            
        }

        function AddProductAdjustmentTemplateHeaderValue(templateHeaderValueId, inputType) {
            toastr.clear();
            __doPostBack('ContentPlaceHolder1_upProductAdjustments', 'addProductAdjustmentTemplateHeaderValue_' + inputType + '_' + templateHeaderValueId);
        }

        function ResetProductAdjustmentTemplate() {
            toastr.clear();
            __doPostBack('ContentPlaceHolder1_upProductAdjustments', 'resetProductAdjustments');
        }

        function ResetProductImage(treeId) {
            __doPostBack('ContentPlaceHolder1_upProductAdjustments', 'resetProductAdjustmentImage_' + treeId);
        }

        function ChangeQty(qty) {
            __doPostBack('ContentPlaceHolder1_upProductAdjustments', 'ChangeQty_' + qty);
        }

        // NOTE: same logic can be found in Product-detail.aspx.CS, function SetStandardAmountQtyFields()
        function ChangeQtyStd(qty, updateUserInputValue = false) {
            var value = parseFloat(qty);    // value is higher if there is a loss percentage
            var stdAmountQtyNeeded = GetStandardAmountQty();

            //console.log("- stdAmountQtyNeeded = " + stdAmountQtyNeeded + ", txtQtyUnit = " + txtQtyUnit.value);

            // wat de koper moet aanschaffen, eventueel meer vanwege een verliespercentage (zaagverlies, snijverlies)
            var txtQtyUnitToPurchase = document.getElementById("txtQtyUnitToPurchase");
            if (txtQtyUnitToPurchase) {
                var stdAmountQtyPurchase = parseFloat(document.getElementById('ContentPlaceHolder1_hdStandardAmountQty').value);
                txtQtyUnitToPurchase.value = Math.round(((value / stdAmountQtyPurchase) + 0.00001) * 100) / 100;
                //console.log("- stdAmountQtyPurchase = " + stdAmountQtyPurchase + ", txtQtyUnitToPurchase = " + txtQtyUnitToPurchase.value);
            }

            // wat de koper daadwerkelijk koopt.
            var actualQtyForProduct = Math.round(((value / stdAmountQtyNeeded) + 0.00001) * 100) / 100;
            UpdateQtyUnit(actualQtyForProduct, updateUserInputValue);

            ChangeQty(qty);
        }

        function UpdateQtyUnit(correctedValue, updateUserInputValue) {
            // Hiddenfield set voor postback
            var lblQtyCorrected = document.getElementById("lblQtyUnitCorrected");
            lblQtyCorrected.innerHTML = correctedValue;
            $("#hdQtyUnitCorrected").val(correctedValue);

            if (updateUserInputValue) {
                var txtQty = document.getElementById('txtQtyUnit');
                txtQty.value = correctedValue;
            }
        }

        function GetStandardAmountQty() {
            var useLossPercentage = document.getElementById('ContentPlaceHolder1_chkIncludeLossPercentage');
            if (useLossPercentage && useLossPercentage.checked) {
                return parseFloat(document.getElementById('ContentPlaceHolder1_hdStandardAmountQtyCorrected').value);
            }
            else {
                return parseFloat(document.getElementById('ContentPlaceHolder1_hdStandardAmountQty').value);
            }
        }

        function ChangeQtyUnit(qty, id) {
            var txtQty = document.getElementById(id);                   // field with quantity, id = 'txtQtyStd'
            if (txtQty != null) {
                var value = parseFloat(qty);                            // value that the user has entered
                var stdAmountQty = GetStandardAmountQty();              // corrected: higher value with loss percentage
                var quantity = Math.ceil(value * stdAmountQty);         // so quantity is also higher (need to buy more)

                txtQty.value = quantity.toString();

                //console.log("ChangeQtyUnit(" + qty + ", " + id + ") -> stdAmountQty = " + stdAmountQty + ", quantity = " + quantity);
                ChangeQtyStd(quantity);
            }
        }

        /**
         * Object om alle benodigde gegevens voor een tekst invoerveld van productbewerkingen op te sturen bij een postback.
         */
        class AdjustmentText {
            constructor(id, value, minCharCount, maxCharCount) {
                this.id = id;
                this.inputId = id.replace("txtinput", "");
                this.value = value.replace("_", "-");
                this.minCharCount = parseFloat(minCharCount);
                this.maxCharCount = parseFloat(maxCharCount);
            }
        }

        /**
         * Object om alle benodigde gegevens voor een dimensie (hoogte/breedte) invoerveld van productbewerkingen op te sturen bij een postback.
         */
        class AdjustmentDimension {
            constructor(id, value, minCharCount, maxCharCount) {
                this.id = id;
                this.value = value;

                this.minCharCount = parseFloat(minCharCount);
                this.maxCharCount = parseFloat(maxCharCount);

                var res = id.replace("txtDimension", "").split("-");
                this.adjustmentId = res.shift();
                this.groupId = res.shift();
                this.adjustmentValueId = res.shift();
                this.dimensionId = res.shift();
            }
        }

        /**
         * Pas een tekstveld aan bij een productbewerking. Alle tekstvelden worden in Ã©Ã©n postback verzonden naar de server zodat er geen waarden verloren gaan.
         */
        function ChangeAdjustmentText(treeId) {

            var txtInput = $("#txtinput" + treeId);

            var tooShortText = document.getElementById('ContentPlaceHolder1_hdTransAdjustmentTextTooShort').value;
            var minCharsText = document.getElementById('ContentPlaceHolder1_hdTransAdjustmentTextMinimalChars').value;

            var tooLongText = document.getElementById('ContentPlaceHolder1_hdTransAdjustmentTextTooLong').value;
            var maxCharsText = document.getElementById('ContentPlaceHolder1_hdTransAdjustmentTextMaximalChars').value;

            let adjustmentTextProps = new AdjustmentText(txtInput.attr("id"), txtInput.val(), txtInput.attr("data-min"), txtInput.attr("data-max"));
            var json = [];
            json.push(adjustmentTextProps);

            var correct = true;

            var jqInput = $("#" + json[0].id);

            var label = $('label[for=' + json[0].id + ']');
            var labelText = "";
            if (label.length > 0) {
                labelText = label.text() + ": ";
            }

            jqInput.removeClass("incorrect-input");
            var data = json[0].value;
            var minLength = json[0].minCharCount;
            var maxLength = json[0].maxCharCount;

            if (!isNaN(minLength) && data.length < minLength) {
                correct = false;
                toastr.error(labelText + tooShortText + " (" + data.length + "), " + minCharsText + " " + minLength + "\n");
            } else if (!isNaN(maxLength) && data.length > maxLength) {
                correct = false;
                toastr.error(labelText + tooLongText + " (" + data.length + "), " + maxCharsText + " " + maxLength + "\n");
            }

            if (!correct) {
                jqInput.addClass("incorrect-input");
            }
            else {
                $("#spAdjComment" + treeId).html("<i class='fa fa-pencil-square-o'></i>" + txtInput.val());

                $("#spAdjComment" + treeId).delay(300).fadeIn(300);
                $("#inputHolder" + treeId).fadeOut(300);

                __doPostBack('ContentPlaceHolder1_upProductAdjustments', 'ChangeTextInput_' + JSON.stringify(json));
            }
        }

        /**
         * Pas de waarden van een productbewerking aan. Bijvoorbeeld hoogte/breedte.
         * Deze worden beiden tegelijk verzonden in de postback i.p.v. apart om geen waarden te verliezen.
         */
        function ChangeDimension(input) {
            var lastDash = input.id.lastIndexOf("-");
            var idToSearch = input.id.substring(0, lastDash);
            var txtInputs = $('*[id*=' + idToSearch + ']:visible');

            var json = [];
            txtInputs.each(function () {
                var item = $(this);
                var itemValue = item.val();
                if (itemValue.length > 0) {
                    var realTextValue = item.val().replace(/\./g, '').replace(',', '.');
                    itemValue = parseFloat(realTextValue);

                    if (!isNaN(itemValue)) {
                        // Parse 10.00 wordt gezien als 1000
                        // Op deze manier laten we duidelijk zien waarom de input niet correct is. Als wij van 10.000 10000 maken ipv 10 moet de klant dat ook kunnen zien.
                        item.val(itemValue.toString().replace('.', ','));
                    }
                    else {
                        // Indien de waarde bijvoorbeeld een tekst is laten we deze zo. Hierdoor kunnen we later netjes aan de klant tonen dat dit fout is.
                        itemValue = item.val();
                    }
                }
                let adjustmentTextProps = new AdjustmentDimension(item.attr("id"), itemValue, item.attr("data-min"), item.attr("data-max"));

                json.push(adjustmentTextProps);
            });

            var correct = true;

            for (var i = 0; i < json.length; i++) {
                var jqInput = $("#" + json[i].id);
                var minValue = json[i].minCharCount;
                var maxValue = json[i].maxCharCount;
                var inputValue = json[i].value;

                var thisCorrect = true;
                jqInput.removeClass("incorrect-input");

                if (isNaN(inputValue) || inputValue.length == 0) {
                    // Een lege waarde is niet correct voor de postback, maar nog niet per se fout. 
                    // Misschien moet de gebruiker het nog invullen?
                    correct = false;
                    if (inputValue !== undefined && inputValue.length > 0) {
                        // Als er een waarde is ingevuld dat geen cijfer is, is het echt fout.
                        thisCorrect = false;
                    }
                }
                else if (inputValue < minValue || inputValue > maxValue) {
                    // Check of de waarde wel echt in de range is.
                    correct = false;
                    thisCorrect = false;
                }

                if (!thisCorrect) {
                    jqInput.addClass("incorrect-input");
                }
            }

            if (correct) {
                __doPostBack('ContentPlaceHolder1_upProductAdjustments', 'ChangeDimension_' + JSON.stringify(json));
            }
        }

        function fileSelected(input, templateHeaderValueId, productId, spanId) {
            StartUpload(input, templateHeaderValueId, productId, spanId)
        }

        function StartUpload(input, templateHeaderValueId, productId, spanId) {
            if (input.files && input.files[0]) {

                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    var data = e.target.result; //Generated DataURL
                    var fileName = input.value.substring((input.value.lastIndexOf("\\")) + 1)

                    UploadFileForLabel(data, fileName, templateHeaderValueId, productId, spanId);
                }
                filerdr.readAsDataURL(input.files[0]);
            }

        }

        function UploadFileForLabel(data, filename, templateHeaderValueId, productId, spanId) {
            var fileData = [];

            fileData.push({ "data": data, "fileName": filename, "productTemplateHeaderValueId": templateHeaderValueId, "productId": productId });

            var jsonData = JSON.stringify(fileData);//"{'dataURL':'" + data + "','fileName':'" + filename + "', 'productTemplateHeaderValueIdString':'" + templateHeaderValueId + "' , 'productIdString':'" + productId + "' }");

            $.ajax({
                type: "POST",
                url: "/webfunctions/FileUpload.ashx",  //pageName.aspx/MethodName

                data: jsonData,
                contentType: "application/json",
                //dataType: "json",
                success: function (jsonData) {
                    if (!jsonData.success) {
                        toastr.error(jsonData.message, "Fout bij aanpassen waarden");
                    } else {
                        var guid = jsonData.guidDirectory;
                        var filename = jsonData.imageFilename;
                        __doPostBack('ContentPlaceHolder1_upProductAdjustments', 'addProductAdjustmentTemplateHeaderValueSetFile_' + templateHeaderValueId + '_' + guid + '_' + filename);
                    }
                },
                error: function (jsonData) {
                    alert(jsonData.message);
                }
            });

        }

        function loadProductVariant2MatrixForProductDetailPage() {
            
        }


        function ChangeAdjustmentPreset() {
            var id = $("#paPreset").val();
            __doPostBack('ContentPlaceHolder1_upProductAdjustments', 'loadDebtorAdjustment_' + id);
        }

        function GetMainProductId() {
            return $('#ContentPlaceHolder1_hdMainProductId').val();
        }

        function GetChildProductId() {
            return $('#ContentPlaceHolder1_hdChildProductId').val();
        }

        function loadTemplateValuesForProductDetailPage() {
            //Load dynamic
            
        };

        function VariantSelected(sel) {
            $('#ContentPlaceHolder1_btnAddToCart').hide();
            $('.ajax-loading').show();
            __doPostBack('ContentPlaceHolder1_upChildProduct', 'VariantGroup_' + sel.value);
        }

        function VariantSelectedBlock(element) {
            $('.filterloader-child-product').css({ 'display': "block" });
            $('.filterloader-child-product').css({ 'position': "fixed" });
            var value = element.getAttribute("data-id");
            __doPostBack('ContentPlaceHolder1_upChildProduct', 'VariantGroup_' + value);
        }

        function OptionSelected(sel, automaticallyChangePrice) {
            SetBuyButtonIcon($('#ContentPlaceHolder1_btnAddToCart'), false);

            if (automaticallyChangePrice) {
                calculatePriceBasedOnDropdowns();
            }
            __doPostBack('ContentPlaceHolder1_upOptionVariant', 'OptionGroup_' + sel.value);
        }

        function CheckInvalidItems() {
            var jqError = $("#adjustment-error-message");
            var imageError = false;

            jqError.hide();
            toastr.remove();
            toastr.options.preventDuplicates = false;

            var emptyImages = $(".image-name-empty");
            if (emptyImages.length > 0) {
                imageError = true;
                emptyImages.addClass("needs-checking");
            }

            var noOptionSelected = $(".none-selected");

            noOptionSelected.each(function () {
                $(this).addClass("needs-checking");
            });

            var allInvalidItems = $(".needs-checking");

            $(".dimension-input").each(function () {
                var val = $(this).val();
                var max = $(this).attr("data-max");
                var min = $(this).attr("data-min");
                if (valInRange(val, max, min) === false) {
                    $(this).addClass("incorrect-input");
                }
            });

            var allItemsIncorrectInput = $(".incorrect-input").map(function () {
                return $(this);
            }).get();

            $.merge(allInvalidItems, allItemsIncorrectInput);

            if (allInvalidItems.length > 0) {
                var errorText = "Zorg er eerst voor dat alle bewerkingen volledig ingesteld zijn.";
                if (allItemsIncorrectInput.length > 0) {
                    toastr.error("Er zijn invoervelden waarvan de invoer incorrect is.", "", { timeOut: 0, extendedTimeOut: 0 });
                }

                if (imageError) {
                    toastr.error("Er is geen afbeelding gekozen bij bewerkingen waar een afbeelding verplicht is.", "", { timeOut: 0, extendedTimeOut: 0 });
                }

                if (noOptionSelected.length > 0) {
                    toastr.error("Kies ten minste een van de opties om het product te kunnen bestellen.", "", { timeOut: 0, extendedTimeOut: 0 });
                }
                jqError.text(errorText);
                jqError.show();

                //Indien er items gevonden zijn die niet correct zijn (niet ingevuld of ongeldige invoer) staan we het toevoegen nog niet toe.
                return false;
            }
            return true;
        }

        // Deze methode gemaakt omdat op de 1 of andere manier data-qty niet goed pakt bij methode boven
        function AddToCartAlt(ctrl, showShopBuyPopup) {
            var txtqty = ctrl.getAttribute("data-qty").split('.')[0];
            var _qty = document.getElementById(txtqty).value;
            if (CheckInvalidItems() == false) {
                return;
            }

            var productAdjustmentTemplateHeaderValueId = document.getElementById('ContentPlaceHolder1_hdProductAdjustmentHeaderTemplateId').value;

            if (productAdjustmentTemplateHeaderValueId != "" && productAdjustmentTemplateHeaderValueId != 0) {
                __doPostBack('ContentPlaceHolder1_upChildProduct', 'AddToCart_SaveSpecification');
            }
            else {
                //ZET DE OPTIONS
                chosenoptions = document.getElementById('ContentPlaceHolder1_hdChosenOptions').value;
                var colorpickerJson = document.getElementById('ContentPlaceHolder1_hdColorPickerData').value;
                AddToCartWithReferrerToPreviousPage(ctrl, _qty, showShopBuyPopup, true, colorpickerJson);
            }
        }

        function valInRange(value, max, min) {
            if (typeof value === 'undefined') {
                return false;
            }

            var valFloat = parseFloat(value.replace(',', '.'));
            var maxFloat = parseFloat(max.replace(',', '.'));
            var minFloat = parseFloat(min.replace(',', '.'));

            return (valFloat <= maxFloat &&
                valFloat >= minFloat);
        }

        function ChangeAdjustmentName() {
            // changeAdjustmentName
            var oldValue = $("#paPreset option:selected").text();
            var paId = $("#paPreset option:selected").val();
            var newName = prompt("Geef een nieuwe naam op voor deze configuratie.", oldValue);

            if (newName != null && oldValue != newName) {
                // saven naar de db
                $.ajax({
                    type: "POST",
                    url: "/webfunctions/ProductAdjustments.ashx",
                    data: {
                        RequestType: "UpdateDebtorAdjustmentName",
                        PaId: paId,
                        PaName: newName
                    },
                    async: true,
                    success: function (response) {
                        toastr.success("Naam van de bewerking succesvol aangepast.");
                        $("#paPreset option:selected").text(newName);
                    },
                    error: function (response) {
                        toastr.error("Er is iets fout gegaan bij het aanpassen van de naam. " + response.Message);
                    },
                });
            }
            return false;
        }


        function RemoveAdjustment() {
            var currentName = $("#paPreset option:selected").text();
            var paId = $("#paPreset option:selected").val();
            if (confirm("Weet je zeker dat je '" + currentName + "' wilt verwijderen?")) {
                $.ajax({
                    type: "POST",
                    url: "/webfunctions/ProductAdjustments.ashx",
                    data: {
                        RequestType: "DeleteDebtorAdjustment",
                        PaId: paId,

                    },
                    async: true,
                    success: function (response) {
                        if (response.Success) {
                            // popup dat het gelukt is
                            $("#paPreset option:selected").remove();
                            $("#paPreset").val('0');
                            ChangeAdjustmentPreset();
                            toastr.success("Bewerking succesvol verwijderd.");
                        }
                    },
                    error: function (response) {
                        toastr.error("Er is iets fout gegaan bij het verwijderen van de bewerking. " + response.Message);
                    },
                });
            }
            return false;
        }

        function AddProductToShoppingCart(button) {
            MakeFacebookAddToCartEventCall(button);
            var qtyValue = $("#txtQty").val();

            if (qtyValue) {
                LoadGoogleTagManagerEcommerce(button, true, qtyValue);
            }

            __doPostBack('ContentPlaceHolder1_btnAddToCart', 'BtnAddToCart_ServerClick_');
        }

        function AddProductToWishList() {
            __doPostBack('ContentPlaceHolder1_btnAddToWishlist', 'BtnAddToWishlist_ServerClick_');
        }

        function AddToCartAfterPostBackWithProductAdjustments(btnId, ctrlQty, productAdjustmentDebtorSpecificationId, productId) {

            var _qty = ctrlQty.value;

            chosenoptions = document.getElementById('ContentPlaceHolder1_hdChosenOptions').value;

            var ctrl = document.getElementById(btnId);

            AddToCartWithQty(ctrl, _qty, true, productId, productAdjustmentDebtorSpecificationId);
        }

        function AddViewedProductToLastViewedProductList() {
            
        }

        


        function ChangeTableSpecsValues(data) {
            var jsonData
                = JSON.parse(data);
            $.each(jsonData, function (i, item) {
                $('#' + item.Key).html(item.Value);
            });
        }

        function closeDigitalProductPopUp() {
            $('#ContentPlaceHolder1_digitalProductAlreadyBoughtHolder').removeClass("popup-product-bought-visible");
        }

        function CalcTotalQtyForBasket() {
            var count = 0;
            $('.variantholder .input-quantity').each(function () {
                count += parseInt(this.value);
            });

            var btnBuy = $('.variantholder .btn-buy');
            let basketIcon = "<i class='fa fa-basket'></i>";

            let text;
            if (count > 0) { text = btnBuy.data("counttext"); }
            else { text = btnBuy.data("defaulttext"); }
            text = text.replace("{0}", count);
            btnBuy.html(basketIcon + text);
        }

        function SearchTable(table) {
            var arrayisloaded = false;
            var array = [];
            $('.variantholder .universal-dropdown option:selected').each(function () {
                if (this.value) {
                    var usingSplit = this.value.split(',');
                    if (arrayisloaded) {
                        array = array.filter(x => usingSplit.includes(x));
                    }
                    else {
                        arrayisloaded = true;
                        array = usingSplit;
                    }
                }
            });

            var search = $('.variantholder #txtSearchVariants').val();

            if (search) {
                // Verberg alle regels
                $(`#${table} tbody tr`).hide();

                // Toon regel waar de zoekterm in voorkomt
                $(`#${table} td:first-child:containsCI(${search})`).each(function () {
                    $(this).closest('tr').show();
                })
            }
            else {
                // Toon alle regels
                $("#" + table + " tbody tr").show();
            }

            if (arrayisloaded) {
                $(`#${table} tbody tr`).each(function () {
                    var productid = $(this).data('productid').toString();
                    if (!array.includes(productid)) {
                        $(this).hide();
                    }
                })
            }
        }

        

        function ToggleGridView(box_view) {

            if (box_view != document.getElementById('hdShowVariantsInGridViewAsBoxed').value) {

                document.getElementById('hdShowVariantsInGridViewAsBoxed').value = box_view;

                $("#sb-list-view").toggleClass('active');
                $("#sb-box-view").toggleClass('active');

                $('.variants-group-container').children('div').each(function () {
                    var span = $(this).children("span");
                    if (box_view) {
                        $(this).addClass('variants-sb-box-item');
                        $(this).removeClass('variants-sb-list-item');
                        span.text(span.attr("data-box-text"));
                    }
                    else {
                        $(this).addClass('variants-sb-list-item');
                        $(this).removeClass('variants-sb-box-item');
                        span.text(span.attr("data-list-text"));
                        var elem = $('.variants-group-container');
                        if (elem !== null && scrollTopGridView !== null) {
                            elem.scrollTop(scrollTopGridView);
                        }
                    }
                });
            }
        }
         

        /**
         * Voeg artikel varianten toe aan het winkelmandje o.b.v. de waarden in de aangeduide tabel.
         * @param {JQuery}    button                        Button als jQuery object bijv: $(ctrl).
         * @param {string}    table                         ID van tabel in HTML structuur.
         * @param {boolean}   savedProductAdjustments       Zijn eventuele productbewerkingen bekend op de server?
         * @returns {boolean}                               True indien toevoegen uitgevoerd is, false als alle aantallen 0 waren. 
         */
        function AddVariantsToCart(button, table, savedProductAdjustments) {
            if (savedProductAdjustments === undefined) {
                savedProductAdjustments = false;
            }

            let productsToOrder = [];

            // Get ProductId + Qty from table where Qty > 0
            $(`#${table} input`).each(function () {
                var qty = $(this).val();
                if (qty > 0) {
                    var productId = $(this).attr("data-productid");
                    productsToOrder.push({ productId, qty });
                }
            })

            if (productsToOrder.length == 0) {
                toastr.error("Geben Sie für mindestens 1 Artikel eine Zahl größer als 0 ein.");
                return false;
            }
            var jsonResult = JSON.stringify(productsToOrder);

            var productAdjustmentValues = document.getElementById('ContentPlaceHolder1_hdProductAdjustmentCheckedValues').value;

            if (savedProductAdjustments === false && productAdjustmentValues.length > 0) {
                __doPostBack('ContentPlaceHolder1_upChildProduct', 'AddProductAdjustmentToCartSaveSpecification_' + jsonResult);
                return true;
            }

            return AddSidebarVariantsToCart(button, table, jsonResult);
        }

        /**
         * Voeg artikel varianten toe aan het winkelmandje o.b.v. de waarden in de aangeduide tabel.
         * @param {JQuery}    button                        Button als jQuery object bijv: $(ctrl).
         * @param {string}    table                         ID van tabel in HTML structuur.
         * @param {string}    jsonResult                    JSON object van artikelen die toegevoegd moeten worden
         * @returns {boolean}                               True indien toevoegen uitgevoerd is, false als alle aantallen 0 waren. 
         */
        function AddSidebarVariantsToCart(button, table, jsonResult) {
            var productAdjustmentDebtorId = document.getElementById('ContentPlaceHolder1_hdProductAdjustmentDebtorSpecificationId').value;

            $.ajax({
                type: "POST",
                url: '/webfunctions/cart.ashx',
                async: true,
                data: {
                    ShowShopBuyPopup: false,
                    RequestType: 'AddVariantsToCart',
                    Variants: jsonResult,
                    ProductAdjustmentDebtorId: productAdjustmentDebtorId,
                    Lang: Lang
                },
                async: true,
                success: function (response) {
                    if (response.Success) {
                        if (NavigateToShoppingCartUrl()) {
                            window.location.href = ShoppingCartUrl;
                        }
                        else {
                            setTimeout(function () {
                                SetBuyButtonIcon(button, false); // Reset winkelmand icon
                                toggleAllmenu(); // Sluit sidebar menu

                                $(`#${table} input`).each(function () {
                                    $(this).val(0).trigger("change"); // Reset waarden naar 0
                                })

                            }, 600);
                            GetCartCount();
                        }
                    }
                },
                error: function (response) {
                    toastr.error("Er is iets fout gegaan bij het toevoegen aan de winkelmand. " + response.Message);
                    return false;
                },
            });
        }

        function SampleProductAlreadyInCartMessage() {
            toastr.error("Der Musterartikel wurde bereits in den Warenkorb gelegt.");
        }

        function UpdateDocumentsTabVisibility(showTab) {
            if (showTab === true) {
                $("#lblDocuments").removeClass("hide");
            }
            else {
                $("#lblDocuments").addClass("hide");
            }
        }

    </script>

                
            
</div>
        </div>

        

        <div class='wrapper100 wrapper100-subfooter'>
<div class='blockcontainer wrapper-content wrapper-subfooter'>
<div id="websiteblok41" class="box pcw100 pcs0010 tabw100 tabs0010 mobw100 mobs0010 pagecfg key-next-subfooter">
<div id="websitecontentblok85" class="box pcw100 pcs0010 tabw100 tabs0010 mobw100 mobs0010 element">
<div id="cmscontentblok306" class="box pcw33 pcs0010 tabw33 tabs0010 mobw33 mobs0010 cmscfg">

<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star-half-alt"></i>
</div>
<div id="cmscontentblok307" class="box pcw66 pcs0010 tabw66 tabs0010 mobw66 mobs0010 cmscfg">

<h4><a href="https://www.valuedshops.de/webshop/Wollplatz-de_11766" style="text-decoration:none">Wir sind stolz auf unsere Bewertung von 8,8!</a></h4>
</div>
</div>
</div>
</div>
</div>
<div class='wrapper100 wrapper100-footer'>
<div class='blockcontainer wrapper-content wrapper-footer'>
<div id="websiteblok39" class="box pcw100 pcs0010 tabw100 tabs0010 mobw100 mobs0010 pagecfg key-next-footer">
<div id="websitecontentblok83" class="box pcw100 pcs0010 tabw100 tabs0010 mobw100 mobs0010 element">
<div id="cmscontentblok258" class="box pcw25 pcs0010 tabw100 tabs0010 mobw100 mobs0010 linkcfg">
<div class='contentbox linkinnercontentbox'>
<h4 >Populär</h4>
<ul>
<li><a href='/blog.html' title='Blog'><i class='fa fa-chevron-right'></i>Blog</a></li>
<li><a href='/videotutorials' title='Videotutorials'><i class='fa fa-chevron-right'></i>Videotutorials</a></li>
<li><a href='/wolle/haekelgarne' title='Häkelgarne'><i class='fa fa-chevron-right'></i>Häkelgarne</a></li>
<li><a href='/nadeln/haekelnadeln' title='Häkelnadeln'><i class='fa fa-chevron-right'></i>Häkelnadeln</a></li>
<li><a href='/haekelstar' title='Häkelstar'><i class='fa fa-chevron-right'></i>Häkelstar</a></li>
<li><a href='/anleitungen/strickanleitungen' title='Strickanleitungen'><i class='fa fa-chevron-right'></i>Strickanleitungen</a></li>
<li><a href='/kaffee-und-wolle' title='Kaffee & Wolle'><i class='fa fa-chevron-right'></i>Kaffee & Wolle</a></li>
<li><a href='/wolle' title='Strickwolle'><i class='fa fa-chevron-right'></i>Strickwolle</a></li>
</ul>
</div>
</div>
<div id="cmscontentblok268" class="box pcw25 pcs0020 tabw100 tabs0020 mobw100 mobs0020 linkcfg">
<div class='contentbox linkinnercontentbox'>
<h4 >Kundeninformation</h4>
<ul>
<li><a href='/allgemeine-geschaftsbedingungen' title='AGB'><i class='fa fa-chevron-right'></i>AGB</a></li>
<li><a href='/experience-center' title='Experience Center'><i class='fa fa-chevron-right'></i>Experience Center</a></li>
<li><a href='/geschenkgutschein-bedingungen' title='Geschenkgutscheinbedingungen'><i class='fa fa-chevron-right'></i>Geschenkgutscheinbedingungen</a></li>
<li><a href='/impressum' title='Impressum'><i class='fa fa-chevron-right'></i>Impressum</a></li>
<li><a href='/jobs' title='Jobs'><i class='fa fa-chevron-right'></i>Jobs</a></li>
<li><a href='/kontakt' title='Kontakt'><i class='fa fa-chevron-right'></i>Kontakt</a></li>
<li><a href='/kundendienst' title='Kundendienst'><i class='fa fa-chevron-right'></i>Kundendienst</a></li>
<li><a href='/reklamationen' title='Reklamationen'><i class='fa fa-chevron-right'></i>Reklamationen</a></li>
<li><a href='/i/cookies.html' title='Cookie-Richtlinie'><i class='fa fa-chevron-right'></i>Cookie-Richtlinie</a></li>
<li><a href='/i/datenschutz.html' title='Datenschutzerklärung'><i class='fa fa-chevron-right'></i>Datenschutzerklärung</a></li>
<li><a href='/m/84/retouraanvraag.aspx' title='Retouren'><i class='fa fa-chevron-right'></i>Retouren</a></li>
<li><a href='/versandkosten-und-lieferung' title='Versandkosten und Lieferung'><i class='fa fa-chevron-right'></i>Versandkosten und Lieferung</a></li>
<li><a href='/bezahlen' title='Zahlungsmöglichkeiten'><i class='fa fa-chevron-right'></i>Zahlungsmöglichkeiten</a></li>
</ul>
</div>
</div>
<div id="cmscontentblok266" class="box pcw25 pcs0030 tabw100 tabs0030 mobw100 mobs0030 cmscfg contact">
<article class='contentbox cmsinnercontentbox'>
<header><h4>Kontaktinformationen</h4></header>
<div class='cms-styling'>

<ul>
<li>Wollplatz.de 
</li><li>Doornepol 5 
</li><li>5301 LV Zaltbommel 
</li><li>Niederlande 
</li><li><u><a href="/kontakt"><u>Kontaktinformationen</u></a></u> </li></ul>
<h3>Anmeldung für Newsletter</h3><iframe style="display: none" name="iframe_form"></iframe>
<form class="newsletter_optin" method="post" name="Anmelden" action="https://publisher.copernica.com/" target="iframe_form" />
<div class="newsinnercontentbox"><input type="hidden" value=".p.w.formgenerate.outputform" name="px_process" /> <input type="hidden" value="subscribe" name="wizard" /> <input type="hidden" value="eabba8a9980bf15bd77040d26bf11db9d1131395cd2899467873040717f64f7b" name="check" /> <input type="hidden" value="12805" name="cdmaccount" /> <input type="hidden" value="dbfield10" name="match_key" /> <input type="hidden" value="2" name="database" /> <input type="hidden" value="10,34,36,49,50,51,54" name="dbfields" /> <input id="txtNieuwsbrief_Email" class="input-white news-inputtxt" name="dbfield10" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="E-Mail Adresse" /> <input type="hidden" value="DE" name="dbfield34" /> <input type="hidden" value="DE" name="dbfield36" /> <input type="hidden" value="1" name="dbfield49" /> <input type="hidden" value="1" name="dbfield50" /> <input type="hidden" value="0" name="dbfield51" /> <input type="hidden" value="website_footer_de" name="dbfield54" /> <input class="btn news-button" type="submit" value="Anmelden" /> </div></form>
<div class="whatever-styling-you-want"></div>
</div></article>
</div>
<div id="cmscontentblok267" class="box pcw25 pcs0040 tabw100 tabs0040 mobw100 mobs0040 cmscfg socialfooter">
<article class='contentbox cmsinnercontentbox'>
<header><h4>Folgen Sie uns</h4></header>
<div class='cms-styling'>

<a title="Facebook" href="https://www.facebook.com/wollplatz"><i class="fa  fa-facebook-square"></i></a><a title="Instagram" href="https://www.instagram.com/wollplatz/"><i class="fa  fa-instagram"></i></a><a title="Youtube" href="https://www.youtube.com/user/Wollplatz"><i class="fa  fa-youtube-square"></i></a><a title="Pinterest" href="https://nl.pinterest.com/wollplatz/"><i class="fa  fa-pinterest-square"></i></a><a href="https://www.valuedshops.de/webshop/Wollplatz-de_11766" rel="nofollow" target="_blank">
<div class="border"></div><img title="keurmerk" class="keurmerk" alt="keurmerk" src="https://content22.logic4server.nl/clientdata/Wolplein/images/webwinkelkeurmerk_4413763182010.png" width="90" height="35" /> </a>
</div></article>
</div>
</div>
</div>
</div>
</div>
<div class='wrapper100 wrapper100-copyfooter'>
<div class='blockcontainer wrapper-content wrapper-copyfooter'>
<div id="websiteblok38" class="box pcw100 pcs0010 tabw100 tabs0010 mobw100 mobs0010 pagecfg key-next-copy-footer">
<div id="websitecontentblok82" class="box pcw100 pcs0010 tabw100 tabs0010 mobw100 mobs0010 element">
<div id="cmscontentblok285" class="box pcw100 pcs0010 tabw100 tabs0010 mobw100 mobs0010 cmscfg">

© 2023 More2Make. All Rights Reserved.
</div>
</div>
</div>
</div>
</div>

    </div>

    
        <div class="shopnavholder">
            <div id="categories" class="navbox logic-scroll">
                <div class="shopnavheader">
                    <form name='search'
class='search navsearch'>
<input 
required='required' 
maxlength='150' 
id='searchSooqrMobile' 
type='text' 
class='input-txt' 
autocomplete='off' 
placeholder='Suchen nach...'/>
<button data-searchpage='suche.html' aria-label='Suchen nach...' class='button topbar-searchbutton searchbutton'><i class='fa fa-search'></i></button>
</form>

                    <span class="menu-close"><i class="fa fa-times"></i></span>
                </div>
                <nav>
                    <ul class="menu-topul">
                        <li class='navgroupinloggenmob'><a href='/login.html' rel='nofollow' title='Login ' class='navgroupclick'>Login<i class='fa fa-angle-right'></i></a></li>
                        <li><a href='/' title="Homepage" class="navgroupclick">Home <i class="fa fa-angle-right"></i></a></li>
                        <li><a href='https://www.wollplatz.de/wolle' class='navgroupclick withoutsub'>Wolle & Garne<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/nadeln' class='navgroupclick withoutsub'>Nadeln<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/pakete' class='navgroupclick withoutsub'>Pakete<i class='fa fa-angle-right'></i></a></li><li><span class='navgroupclick withsub'>Zubehör<i class='fa fa-angle-right'></i></span><ul class='navsub'><li><a href='https://www.wollplatz.de/zubehor/accessoires' class='navgroupclick withoutsub'>Accessoires<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/zubehor/kurzwaren' class='navgroupclick withoutsub'>Kurzwaren<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/aufbewahrung' class='navgroupclick withoutsub'>Aufbewahrung<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/zubehor/alle-zubehor' class='navgroupclick withoutsub'>Alle Zubehör<i class='fa fa-angle-right'></i></a></li></ul></li><li><a href='https://www.wollplatz.de/anleitungen' class='navgroupclick withoutsub'>Anleitungen<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/bucher-zeitschriften' class='navgroupclick withoutsub'>Bücher & Zeitschriften<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/sale-wollplatz' class='navgroupclick withoutsub'>Sale<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/amigurumi-wollplatz' class='navgroupclick withoutsub'>Amigurumi<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/geschenktipps' class='navgroupclick withoutsub'>Geschenktipps<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/geschenktipps-ab-50-euro' class='navgroupclick withoutsub'>Geschenktipps ab 50 euro<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/geschenktipps-bis-10-euro' class='navgroupclick withoutsub'>Geschenktipps bis 10 euro<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/geschenktipps-bis-25-euro' class='navgroupclick withoutsub'>Geschenktipps bis 25 euro<i class='fa fa-angle-right'></i></a></li><li><a href='https://www.wollplatz.de/geschenktipps-bis-50-euro' class='navgroupclick withoutsub'>Geschenktipps bis 50 euro<i class='fa fa-angle-right'></i></a></li>
                    </ul>
                    <ul class="menu-bottomul">
                        

                        <li class="merkenmenu"><a href='/marken.html'
                            title='Marken' class="navcmsclick withoutsub">Marken <i class="fa fa-angle-right"></i></a></li>

                        
                        <li class="winkelmandjemenu"><a href='/warenkorb.html'
                            title='Warenkorb' class="navcmsclick withoutsub">Warenkorb <i class="fa fa-angle-right"></i></a></li>
                        

                        <li class="verlanglijstmenu"><a href='/wunschliste.html'
                            title='Wunschliste' class="navcmsclick withoutsub">Wunschliste<i class="fa fa-angle-right"></i></a></li>
                        <li class="mijnaccountmenu"><span class='navcmsclick withsub'>Mein Account<i class='fa fa-angle-right'></i></span>
                            <ul class='navsub'>
                                <li><a href='/login.html' rel='nofollow' title='Login' class='navcmsclick withoutsub'>Login <i class='fa fa-angle-right'></i></a></li>
<li><a href='/registrieren.html' title='Registrieren' class='navcmsclick withoutsub'>Registrieren <i class='fa fa-angle-right'></i></a></li>
<li><a href='/login.html?mode=bestellijst' rel='nofollow' title='Bestel Liste' class='navcmsclick withoutsub'>Bestel Liste <i class='fa fa-angle-right'></i></a></li>
<li><a href='/login.html?mode=bestelhistorie' rel='nofollow' title='Bestellhistorie' class='navcmsclick withoutsub'>Bestellhistorie <i class='fa fa-angle-right'></i></a></li>
<li><a href='/login.html?mode=orders;facturen' rel='nofollow' title='Bestellungen & rechnungen' class='navcmsclick withoutsub'>Bestellungen & rechnungen <i class='fa fa-angle-right'></i></a></li>

                            </ul>
                        </li>
                        
                    </ul>
                </nav>
            </div>
        </div>
    

    
        <div class="hide shopuserholder">
            <div id="menuUserMenu" class="logic-scroll logbox">
            </div>
        </div>
    


    
        <div class="hide shopcartholder">
            <div class="logic-scroll cartbox">

                
                <h4 onclick="location.href='/warenkorb.html';">Warenkorb</h4>
                <span class="menu-close"><i class="fa fa-times"></i></span>
                <div id="innnershopcartholder">
                    <div class="filterloader" style="display: block">
                        <div class="spinner">
                            <div class="bounce1"></div>
                            <div class="bounce2"></div>
                            <div class="bounce3"></div>
                        </div>
                    </div>
                </div>
                <div>
                    
                </div>
            </div>
        </div>
    

    

    
        <div class="hide shopfilterholder">
            <div class="filterloader">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
            <div class="logic-scroll filterbox">
                <div class="filterbox-header">
                    <span class="clear-filters" onclick="ResetFilter();">Filter löschen</span>
                    <h4>Filter</h4>
                    <span class="menu-close"><i class="fa fa-times"></i></span>
                </div>
                <div id="innerproductlistfilterformobiel" class="innerproductlistfilter innerproductlistfilterformobiel"></div>

            </div>
        </div>
    



    


    <div class="buypopupholder"></div>

    <div class="shopcover"></div>
    <div class="backtotop-btn"><i class="fa fa-toggle-up"></i></div>
    <div class="bestellijstpopupholder"></div>

    

    <div id="gpHolder" class="generic-popup-holder hide">
        <span class="generic-popup-close" onclick="CloseGenericPopup();"><i class="fa fa-times"></i></span>
        <div id="gpHeader" class="generic-popup-header">
            <h4 id="gpTitle"></h4>
        </div>
        <div id="gpContent" class="generic-popup-content"></div>
        <div id="gpCta" class="generic-popup-cta">
            <div id="ctaCancel" onclick="CloseGenericPopup();">Stornieren</div>
            <div id="ctaConfirm">Bestätigen</div>
        </div>
    </div>

    

    

    
    <!--[if lte IE 9]><p class="oldbrowser">U gebruikt een verouderde browser. <a href="http://browsehappy.com/">Vernieuw uw browser</a> om deze website optimaal weer te geven.</p><![endif]-->

    
    
    

    <style>
.login-link-holder span {
    font-weight: bold;
}
</style><script>function InitJquery(){}</script>
    <script>(function() {
var SooqrLoggedin = false;
 var ws = document.createElement('script'); ws.type = 'text/javascript'; ws.async = true;
 ws.src = 'https://static.sooqr.com/custom/119572/snippet.js';
 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ws, s);
})();</script>


    <script>
var UseGA = false;
var UseGtagInsteadOfAnalytics = false;
var UseGoogleTagManagerEvent = true;
var UseLegacyImplementationInGtm = true;
var UseGa4ImplementationInGtm = false;
var UseFacebookPixelEvent = false;
var UseDatatricsPixelEvent = false;
var ShowDebtorWebshopProductListLabels = false;
var CurrencySymbol = '€';
var CurrencyIsoCode = 'EUR';
var WebshopCountryShortCode = 'de-de';
var VatToggleExcluding = 'Inkl. MwSt';
var VatToggleIncluding = 'Exkl. MwSt';
var ShowAdressUnknownWarning = true;
var UseGlobalSearchAsBaseSearcherInsteadOfSuggestionOrCategory = false;
var ShowProductInformationPopupOnExplodedViews = false;
var DisplayPriceZeroCentsAsDash = false;
var SlickRandomSliderUseCeilInsteadOfFloor = false;
var VatInclusiveLeading = true;
var DeviceOnServer = 'PC';
var ExecuteCartCount = false;
var Lang = 'de';
var ShoppingCartUrl = '';
var NavigateToShoppingCartConfig = '';
var JsVersion = '230914120951';
var EnablePushState = true;
</script>

    <script src='/js/all.min.js?v=230914120951' type='text/javascript' onload='InitJquery(); AfterInit();' async></script>

    

    
    <style>.fa { font-family: 'Font Awesome 5 Pro'; font-weight: 300 !important; }</style><link rel="preconnect" href="https://cdn.logic4.nl">
                <link rel="stylesheet" href="https://cdn.logic4.nl/FontAwesome/5/latest/css/all.min.css?v=07102023">
                <link rel="stylesheet" href="https://cdn.logic4.nl/FontAwesome/5/latest/css/brands.min.css?v=07102023">
                <link rel="stylesheet" href="https://cdn.logic4.nl/FontAwesome/5/latest/css/v4-shims.min.css?v=07102023">
                <link rel="stylesheet" href="https://cdn.logic4.nl/FontAwesome/5/custom_shims/custom-shims.min.css?v=07102023">
</body>
</html>
"""