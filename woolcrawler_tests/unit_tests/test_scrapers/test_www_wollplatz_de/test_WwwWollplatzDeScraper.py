from decimal import Decimal
import pytest
from unittest.mock import Mock, AsyncMock
from .dummy_website import DUMMY_WEBSITE
from woolcrawler_src.scraper.website_scrapers.www_wollplatz_de import WwwWollplatzDeScraper


class Session:
    async def request(self):
        pass


class WwwWollplatzDeParser(AsyncMock):
    pass


@pytest.fixture
def get_scraper():
    wool_case = Mock()
    wool_case.brand = 'Cool'
    wool_case.name = 'Wool'
    session = Session()
    resp_mock = AsyncMock()
    resp_mock.text = AsyncMock(return_value=DUMMY_WEBSITE)
    session.request = AsyncMock(return_value=resp_mock)
    scraper = WwwWollplatzDeScraper(wool_case, session)
    return scraper


@pytest.mark.asyncio
async def test_scrape(get_scraper):
    scraper = get_scraper
    assert scraper.url == 'https://www.wollplatz.de/wolle/Cool/Cool-Wool'
    # await scraper.fetch_html()
    await scraper.scrape()
    assert scraper.session.request.called
    assert scraper.session.request.call_count == 1
    assert scraper.session.request.return_value.raise_for_status.called
    assert scraper.session.request.return_value.text.called
    assert scraper.wool_case.availability == 'Lieferbar'
    assert scraper.wool_case.price == Decimal('3.10')
    assert scraper.wool_case.needle_strength == '4 mm'
    assert scraper.wool_case.composition == '100% Acryl'
