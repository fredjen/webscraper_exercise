# Created by fredjen at 06.10.2023
Feature: Woolcrawler
  The woolcrawler is the main and only feature of this script. It takes all wool entries from the database, scrapes
  their current related data from the specified websites and saves the results back to the db.

  Scenario: Crawl
    Given woolentries exist in the database
    When the crawler is started
    Then the corresponding wooldata is scraped and saved to db