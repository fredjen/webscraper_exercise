import datetime
import random
import asyncio
from decimal import Decimal
import pytest
from pytest_bdd import scenario, given, when, then
from woolcrawler_tests.util_dbio import TestDBIO
from woolcrawler_src import WoolCrawler
from woolcrawler_src.utils import get_logger
from woolcrawler_src.dbio import DBIO


TEST_WOOL_CASE_NUM = 50


@pytest.fixture
def setup_db(tmp_path):
    test_dbio = TestDBIO(
        'sqlite://storage.sqlite',
        storage_path=tmp_path,
    )
    return test_dbio, tmp_path


class ScraperStub:
    def __init__(self, wool_case, session):
        self.wool_case = wool_case
        self.session = session

    @property
    def result(self):
        return self.wool_case

    async def scrape(self):
        self.wool_case.scraped_on = datetime.datetime.now()
        max_seconds = random.randint(1, 3)
        await asyncio.sleep(1, max_seconds)
        self.wool_case.availability = self.wool_case.brand
        self.wool_case.needle_strength = self.wool_case.name
        self.wool_case.composition = self.wool_case.website
        self.wool_case.price = Decimal(f'{str(len(self.wool_case.brand) + len(self.wool_case.name))}.00')
        return self.wool_case


class ScraperFactoryStub:
    @staticmethod
    def get_scraper(wool_case, session):
        return ScraperStub(wool_case, session)


@pytest.fixture
def setup_stubs():
    scraper_factory = ScraperFactoryStub()
    return scraper_factory


@scenario('crawl.feature', 'Crawl')
def test_crawl():
    pass


@given('woolentries exist in the database', target_fixture='setup_database')
def setup_database(setup_stubs, setup_db):
    scraper_factory = setup_stubs
    test_dbio, tmp_path = setup_db
    # ---
    dummy_wool_cases = [
        dict(brand=f'Ramba{i}', name=f'Zamba XL {i}', website='www.wollplatz.de')
        for i in range(1, TEST_WOOL_CASE_NUM + 1)
    ]
    for wool_case in dummy_wool_cases:
        test_dbio.insert_wool_case(**wool_case)
    # ---
    db_path = tmp_path
    db_path.mkdir(parents=True, exist_ok=True)
    dbio = DBIO('sqlite://storage.sqlite', storage_path=db_path)
    return dbio, scraper_factory, test_dbio


@when('the crawler is started', target_fixture='run_crawler')
def run_crawler(setup_database, tmp_path):
    dbio, scraper_factory, test_dbio = setup_database

    # create logger instances #
    log_path = tmp_path
    log_path.mkdir(parents=True, exist_ok=True)
    get_logger('coro_error', file_path=log_path)
    get_logger('db_error', file_path=log_path)
    general_error_logger = get_logger('general_error', file_path=log_path)
    # ----------------------- #

    pending_queue = dbio.get_wool_cases()
    today = datetime.date.today()
    crawl_date = dbio.get_crawl_date_id(today)
    woolcrawler = WoolCrawler(dbio, scraper_factory, pending_queue, crawl_date, async_limit=10, task_timeout=4)
    try:
        woolcrawler.run()
    except Exception as e:
        dbio.close()
        test_dbio.close()
        general_error_logger.exception(e)
        raise e

    dbio.close()

    return test_dbio


@then('the corresponding wooldata is scraped and saved to db')
def assert_data_in_db(run_crawler):
    test_dbio = run_crawler
    scrape_results = test_dbio.get_wool_data_points_with_wool_case_data()
    assert len(scrape_results) == TEST_WOOL_CASE_NUM
    for data_point in scrape_results:
        assert isinstance(data_point['crawl_date_id'], int)
        assert isinstance(data_point['scraped_on'], datetime.datetime)
        assert len(data_point['brand']) > 0
        assert len(data_point['name']) > 0
        assert len(data_point['website']) > 0
        assert data_point['brand'] == data_point['availability']
        assert data_point['name'] == data_point['needle_strength']
        assert data_point['website'] == data_point['composition']
        assert data_point['price'] == Decimal(f'{str(len(data_point["brand"]) + len(data_point["name"]))}.00')
    test_dbio.close()
