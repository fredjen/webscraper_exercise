import collections
from typing import Any, Optional


class Queue:
    """
    A simple Queue class.
    Dequeue does not raise an exception when there are no items left to return, but returns None instead.
    """
    def __init__(self) -> None:
        self._data = collections.deque()

    def enqueue(self, value: Any) -> None:
        self._data.append(value)

    def dequeue(self) -> Optional[Any]:
        try:
            return self._data.popleft()
        except IndexError:
            return None
