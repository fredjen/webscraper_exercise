import logging
import logging.handlers
import pathlib


_logger_instances = set()


def _normalize_path(name, file_path):
    if not (name or file_path):
        return pathlib.PurePath('./default.log')
    if not isinstance(file_path, pathlib.PurePath):
        file_path = pathlib.PurePath(file_path)
    if not file_path.suffix == '.log':
        file_name = name + '.log'
        file_path = file_path / file_name
    return file_path


def _normalize_path_for_debugging(name, file_path):
    file_path = _normalize_path(name, file_path)
    file_path = file_path.with_name(file_path.stem + '.debug' + file_path.suffix)
    return file_path


def _set_console_handler_parameters(logger, formatter):
    # create console handler
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(formatter)
    # only show debug messages in console by filtering everything else
    # noinspection PyTypeChecker
    console_handler.addFilter(_LevelFilter(logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL))
    logger.addHandler(console_handler)


def _set_file_handler_parameters(logger, formatter, file_path, file_rotation=True, max_kb=1024, backup_count=1):
    if file_rotation:
        # create rotating file handler
        max_bytes = max_kb*1024
        file_handler = logging.handlers.RotatingFileHandler(file_path, maxBytes=max_bytes, backupCount=backup_count)
    else:
        # create file handler
        file_handler = logging.FileHandler(file_path)

    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)


def get_logger(name='', file_path='', debugging=False, file_rotation=True, max_kb=1024, backup_count=1):
    """
    get_logger returns the already existing logger by name or instanciates a new one if it does not exist already.

    To create loggers, the logging module from the Python Standard Library is used, and the returned logger is a
    logger object from said library.
    If the logger instance with the specified name has already been created during the runtime of the program before,
    only the name argument must be specified.
    Without any arguments given the standard root logger will be created, writing the logfiles to the working directory.
    file_path must lead to a .log-textfile or a directory.  If the logfile does not already exist, the get_logger
    function will create it.

    :param name: str
    :param file_path: str | Path
    :param debugging: bool
    :param file_rotation: bool
    :param max_kb: int
    :param backup_count: int
    :return: Logger
    """
    # create logger
    logger = logging.getLogger(name)

    # return logger instance if instance already exists
    if name in _logger_instances:
        return logger
    else:
        _logger_instances.add(name)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    if debugging:
        logger.setLevel(logging.DEBUG)
        _set_console_handler_parameters(logger, formatter)
    else:
        logger.setLevel(logging.INFO)
    # noinspection PyTypeChecker
    file_path = _normalize_path(name, file_path) if not debugging else _normalize_path_for_debugging(name, file_path)
    _set_file_handler_parameters(logger, formatter, file_path, file_rotation, max_kb, backup_count)

    return logger


class _LevelFilter:
    def __init__(self, *args):
        self.level = args

    def filter(self, record):
        return record.levelno not in self.level
