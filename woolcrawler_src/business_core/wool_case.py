class WoolCase:
    """A simple business data object."""
    def __init__(self, woolcase_id: int, brand: str, name: str, website: str, **kwargs) -> None:
        self.id = woolcase_id
        self.brand = brand
        self.name = name
        self.website = website
        self.availability = kwargs.get('availability')
        self.price = kwargs.get('price')
        self.needle_strength = kwargs.get('needle_strength')
        self.composition = kwargs.get('composition')
        self.scraped_on = kwargs.get('scraped_on')
        self.crawl_date = kwargs.get('crawl_date_id')
