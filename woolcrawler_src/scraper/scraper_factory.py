from aiohttp import ClientSession
from woolcrawler_src.business_core import WoolCase
from woolcrawler_src.scraper.website_scrapers.abstract_scraper import Scraper
from .website_scrapers import WwwWollplatzDeScraper


SCRAPER_MAP = {
    'www.wollplatz.de': WwwWollplatzDeScraper,
}


class ScraperFactory:
    """
    The ScraperFactory receives a WoolCase instance and returns an instantiated instance of the corresponding
    Scraper object necessary to scrape the website of the wool case.
    """
    def __init__(self):
        pass

    @staticmethod
    def get_scraper(wool_case: WoolCase, session: ClientSession) -> Scraper:
        scraper = SCRAPER_MAP[wool_case.website]
        return scraper(wool_case, session)
