import datetime
import decimal
from .abstract_parser import Parser
from .abstract_scraper import Scraper
from bs4 import BeautifulSoup


class WwwWollplatzDeParser(Parser):
    """The parser for www.wollplatz.de."""
    def __init__(self, html: str) -> None:
        super().__init__(html)
        self.soup = BeautifulSoup(self.html, 'html.parser')

    def get_availability(self):
        availability_row = self.soup.find('tr', class_='pbuy-voorraad')
        availability = availability_row.find('span').text
        return availability

    def get_price(self):
        pricebox = self.soup.find('div', class_='buy-price').find('span', class_='product-price')
        price = pricebox['content']
        price = decimal.Decimal(price)
        return price

    def get_needle_strength(self):
        needle_strength_category = self.soup.find('td', string='Nadelstärke')
        needle_strength = needle_strength_category.find_next_sibling().text
        return needle_strength

    def get_composition(self):
        composition_category = self.soup.find('td', string='Zusammenstellung')
        composition = composition_category.find_next_sibling().text
        return composition

    def parse(self):
        self.results['availability'] = self.get_availability()
        self.results['price'] = self.get_price()
        self.results['needle_strength'] = self.get_needle_strength()
        self.results['composition'] = self.get_composition()


class WwwWollplatzDeScraper(Scraper):
    """The scraper for www.wollplatz.de."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.base_url = 'https://www.wollplatz.de/wolle/'
        self.url = self._make_url()
        self.html = ''
        self.parser = None

    def _make_url_woolname(self):
        return '-'.join(self.wool_case.name.split())

    def _make_url(self):
        wool_name = self._make_url_woolname()
        return f'{self.base_url}{self.wool_case.brand}/{self.wool_case.brand}-{wool_name}'

    async def fetch_html(self):
        resp = await self.session.request(method="GET", url=self.url)
        resp.raise_for_status()
        print("Got response [%s] for URL: %s", resp.status, self.url)
        self.html = await resp.text()

    def parse_html(self):
        self.parser = WwwWollplatzDeParser(self.html)
        self.parser.parse()

    def add_parse_results_to_woolcase(self):
        self.wool_case.availability = self.parser.results['availability']
        self.wool_case.price = self.parser.results['price']
        self.wool_case.needle_strength = self.parser.results['needle_strength']
        self.wool_case.composition = self.parser.results['composition']

    async def scrape(self):
        """
        This one is the main method to call.
        It tries to scrape the website for the given wool case, parses the results, adds the new information to the
        WoolCase object and returns the WoolCase object.
        If you don't save the returned wool case, you can still find it in WwwWollplatzDeScraper.wool_case.
        """
        self.wool_case.scraped_on = datetime.datetime.now()
        await self.fetch_html()
        self.parse_html()
        self.add_parse_results_to_woolcase()
        return self.wool_case
