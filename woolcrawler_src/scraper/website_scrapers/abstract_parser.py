from abc import ABC, abstractmethod


class Parser(ABC):
    """
    Abstract class for all parser objects.
    Create child classes from this when making new parser classes for new websites to scrape.

    It's intended use is to be used by the corresponding scraper object.
    """
    def __init__(self, html: str) -> None:
        self.html = html
        self.results = dict()

    @abstractmethod
    def get_availability(self):
        pass

    @abstractmethod
    def get_price(self):
        pass

    @abstractmethod
    def get_needle_strength(self):
        pass

    @abstractmethod
    def get_composition(self):
        pass

    def parse(self):
        self.results['availability'] = self.get_availability()
        self.results['price'] = self.get_price()
        self.results['needle_strength'] = self.get_needle_strength()
        self.results['composition'] = self.get_composition()
