from abc import ABC, abstractmethod
from aiohttp import ClientSession
from woolcrawler_src.business_core import WoolCase


class Scraper(ABC):
    """
    Abstract class for all scraper objects.
    Create child classes from this when making new scraper classes for new websites to scrape.
    """
    def __init__(self, wool_case: WoolCase, session: ClientSession) -> None:
        self.wool_case = wool_case
        self.session = session

    @abstractmethod
    async def scrape(self):
        pass

    @property
    def result(self):
        return self.wool_case

    @abstractmethod
    async def fetch_html(self):
        pass

    @abstractmethod
    def parse_html(self):
        pass

    @abstractmethod
    def add_parse_results_to_woolcase(self):
        pass
