import asyncio
from asyncio import Task
import datetime
from typing import Optional, Set
from aiohttp import ClientSession
from aiohttp.client_exceptions import ClientResponseError

from woolcrawler_src.dbio import DBIO
from woolcrawler_src.scraper import ScraperFactory
from woolcrawler_src.business_core import WoolCase

from .utils import Queue, get_logger


"""fix yelling at me error"""
"""fix from: https://pythonalgos.com/2022/01/09/runtimeerror-event-loop-is-closed-asyncio-fix/"""
from functools import wraps
from asyncio.proactor_events import _ProactorBasePipeTransport


def silence_event_loop_closed(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except RuntimeError as e:
            if str(e) != 'Event loop is closed':
                raise

    return wrapper


_ProactorBasePipeTransport.__del__ = silence_event_loop_closed(_ProactorBasePipeTransport.__del__)
"""fix yelling at me error end"""


class WoolCrawler:
    """
    The heart of the crawler logic.
    It receives instances of the db wrapper, the scraper factory, a queue with wool cases to srape, and a python
    date object specifying the current date.

    Use the parameter async_limit to limit how many scrape calls are being queued for asynchronous processing.
    Use the task_timeout parameter to define after how many secounds a single task is being timed out and discarded.

    Logs timed out tasks and connection errors as well as errors with database operations.
    Timed out tasks and connection errors will not disrupt the script.
    An error with a db operation will reraise the exception and halt the script if not handled somewhere else.
    """
    def __init__(self, dbio: DBIO, scraper_factory: ScraperFactory, pending_queue: Queue, crawl_date: int,
                 async_limit: int = 10, task_timeout: Optional[float] = None) -> None:
        """
        crawl_date means the date id from the db and therefore is an integer, not a date instance.
        The task_timout argument is in secounds.
        """
        self.dbio = dbio
        self.scraper_factory = scraper_factory
        self.pending_queue = pending_queue
        self.crawl_date = crawl_date
        self.limit = async_limit
        self.task_timeout = task_timeout
        self.results = Queue()

    async def scrape(self, wool_case: WoolCase, session: ClientSession) -> WoolCase:
        wool_case.crawl_date = self.crawl_date
        scraper = self.scraper_factory.get_scraper(wool_case, session)
        await scraper.scrape()
        return scraper.result

    def get_task(self, pending_wool_case: WoolCase, session: ClientSession) -> Task:
        wait_coro = asyncio.wait_for(
            self.scrape(pending_wool_case, session),
            timeout=self.task_timeout,  # seconds
        )
        return asyncio.create_task(wait_coro, name='woolcase with id ' + str(pending_wool_case.id))

    async def consume_done_tasks(self, done: Set[Task]) -> None:
        while done:
            done_task = done.pop()
            try:
                result_wool_case = await done_task
            except (asyncio.TimeoutError, ClientResponseError) as e:
                print(f'{done_task.get_name()} has thrown an error: {e}')
                # log error
                logger = get_logger('coro_error')
                logger.exception(e)
                continue
            self.results.enqueue(result_wool_case)
            print('woolcase ', result_wool_case.id)
            print(datetime.datetime.now())

    def book_results_to_db(self):
        try:
            self.dbio.bulk_insert_wool_data_points(self.results)
        except Exception as e:
            # log error
            logger = get_logger('db_error')
            logger.exception(e)
            raise e

    async def loop(self):
        pending = set()
        done = set()
        async with ClientSession() as session:
            while True:
                pending_queue_empty = False
                while len(pending) < self.limit and not pending_queue_empty:
                    pending_wool_case = self.pending_queue.dequeue()
                    if pending_wool_case is None:
                        pending_queue_empty = True
                    else:
                        task = self.get_task(pending_wool_case, session)
                        pending.add(task)

                if pending:
                    # done, pending = await asyncio.wait(pending)
                    # --- This is usually faster than without return_when=asyncio.FIRST_COMPLETED --- #
                    # --- but needs more performance testing: --------------------------------------- #
                    done, pending = await asyncio.wait(pending, return_when=asyncio.FIRST_COMPLETED)

                await self.consume_done_tasks(done)

                self.book_results_to_db()

                if pending_queue_empty and not pending:
                    break

        # good idea according to aiohttp documentation for letting sessions close before the loop shuts down
        await asyncio.sleep(0.5)

    def run(self):
        """
        This is the main method to call after instanciating the class.
        All other methods are rather internal logic.
        """
        asyncio.run(self.loop())
