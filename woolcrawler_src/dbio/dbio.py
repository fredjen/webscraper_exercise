from typing import Tuple, Optional
from datetime import date as Date
from .db_model import get_db
from woolcrawler_src.business_core import WoolCase
from woolcrawler_src.utils import Queue


class DBIO:
    """
    Wrapper for database functionality do decouple database library from code.
    """
    def __init__(self, storage, storage_path=None, autocommit=True, pool_size=0, migrate=True, fake_migrate=False,
                 check_reserved=('sqlite',)):
        """
        The storage argument as of now takes a pydal db setup string.
        """
        self._storage = storage
        self._storage_path = storage_path
        self._autocommit = autocommit
        self._db = get_db(self._storage,
                          folder=self._storage_path,
                          pool_size=pool_size,
                          migrate=migrate,
                          fake_migrate=fake_migrate,
                          check_reserved=check_reserved,
                          )

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

    # ------------------- #
    # commit and rollback #
    # ------------------- #

    def commit(self):
        """Call after db actions to commit to db."""
        self._db.commit()

    def rollback(self):
        """Call to rollback not yet commited db actions."""
        self._db.rollback()

    # ---------------- #
    # close connection #
    # ---------------- #

    def close(self):
        """Close all db connections from this DAL instance."""
        self._db.close()

    # --------------- #
    # db wool actions #
    # --------------- #

    def _check_if_exists(self, crawl_date: Date):
        db = self._db
        return db(db.crawl_date.crawl_date == crawl_date).count()

    def _create_crawl_date(self, crawl_date: Date):
        db = self._db
        crawl_date_id = db.crawl_date.insert(crawl_date=crawl_date)
        self.commit()
        return crawl_date_id

    def _get_crawl_date(self, crawl_date: Date):
        db = self._db
        return db(db.crawl_date.crawl_date == crawl_date).select().first()

    def get_crawl_date_id(self, crawl_date: Date) -> int:
        """
        Get db id of the given date.
        If the date does not exist in db already, create it and return the id of the new date entry.
        """
        if not self._check_if_exists(crawl_date):
            return self._create_crawl_date(crawl_date)
        else:
            return self._get_crawl_date(crawl_date).id

    def get_wool_cases(self, limit: Optional[Tuple[int, int]] = None) -> Queue:
        """
        Get a queue of all wool cases.
        Optionally limit, which db entries to query.
        """
        db = self._db
        query_kwargs = dict()
        if limit is not None:
            query_kwargs['limitby'] = limit
        wool_case_rows = db(db.wool_case).select(**query_kwargs)
        wool_cases = Queue()
        for wool_case_row in wool_case_rows:
            wool_case = WoolCase(
                wool_case_row.id,
                wool_case_row.brand,
                wool_case_row.name,
                wool_case_row.website,
            )
            wool_cases.enqueue(wool_case)
        return wool_cases

    def bulk_insert_wool_data_points(self, data_points: Queue) -> None:
        """Receive a queue of scraped woolcases and bulk insert them into the wool_data_point table."""
        db = self._db
        insert_list = list()
        while woolcase := data_points.dequeue():
            woolcase_data = dict(
                wool_case_id=woolcase.id,
                crawl_date_id=woolcase.crawl_date,
                availability=woolcase.availability,
                price=woolcase.price,
                needle_strength=woolcase.needle_strength,
                composition=woolcase.composition,
                scraped_on=woolcase.scraped_on,
            )
            insert_list.append(woolcase_data)
        db.wool_data_point.bulk_insert(insert_list)
        self.commit()
