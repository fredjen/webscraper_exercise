from pydal import DAL, Field


def get_db(storage, pool_size=0, db_codec='UTF-8', migrate=True, fake_migrate=False, check_reserved=('sqlite',),
           folder=None):
    """Setup the database model using pydal and return a DAL instance."""
    db = DAL(
        uri=storage,
        pool_size=pool_size,
        db_codec=db_codec,
        check_reserved=check_reserved,
        migrate=migrate,
        migrate_enabled=migrate,
        fake_migrate=fake_migrate,
        fake_migrate_all=fake_migrate,
        folder=folder,
    )
    db.define_table(
        'wool_case',
        Field('brand', 'string', notnull=True),
        Field('name', 'string', notnull=True),
        Field('website', 'string', notnull=True),
    )
    db.define_table(
        'crawl_date',
        Field('crawl_date', 'date', notnull=True, unique=True),
    )
    db.define_table(
        'wool_data_point',
        Field('wool_case_id', 'reference wool_case', notnull=True),
        Field('crawl_date_id', 'reference crawl_date', notnull=True),
        Field('availability', 'string', default=None),
        Field('price', 'decimal(4, 2)', default=None),
        Field('needle_strength', 'string', default=None),
        Field('composition', 'string', default=None),
        Field('scraped_on', 'datetime', notnull=True),
    )
    return db
