import time
import datetime
from pathlib import Path
from woolcrawler_src.utils import get_logger
from woolcrawler_src.woolcrawler import WoolCrawler
from woolcrawler_src.dbio import DBIO
from woolcrawler_src.scraper import ScraperFactory


def main():
    # create logger instances #
    log_path = Path(__file__, '../logs/')
    log_path.mkdir(parents=True, exist_ok=True)
    get_logger('coro_error', file_path=log_path)  # instanciate this once to define the logfile path
    get_logger('db_error', file_path=log_path)  # instanciate this once to define the logfile path
    general_error_logger = get_logger('general_error', file_path=log_path)
    # ----------------------- #
    db_path = Path(__file__, '../database/')
    db_path.mkdir(parents=True, exist_ok=True)
    with DBIO('sqlite://storage.sqlite', storage_path=db_path) as dbio:
        pending_queue = dbio.get_wool_cases()
        today = datetime.date.today()
        crawl_date = dbio.get_crawl_date_id(today)
        scraper_factory = ScraperFactory()
        woolcrawler = WoolCrawler(dbio, scraper_factory, pending_queue, crawl_date, async_limit=10, task_timeout=4)
        try:
            woolcrawler.run()
        except Exception as e:
            general_error_logger.exception(e)
            raise e


if __name__ == '__main__':
    s = time.perf_counter()
    main()
    elapsed = time.perf_counter() - s
    print(f"{__file__} executed in {elapsed:0.2f} seconds.")
