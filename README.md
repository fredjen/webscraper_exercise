What's this about?
===

Glad you asked. This is an exercise I did creating an asynchronous web srcaper.

Setup
===

1. Written in Python 3.10. (It's what I had installed...)
2. Pull the repository.
3. Run main.py with the working directory at the project root.  
This will create the database folder and the logs folder at project root level with an sqlite database in the database folde.
4. Add some relevant data to the **woolcase** database table.  
5. Run main.py with the working directory at the project root once more.  
Check the database table **wool_data_point** for the results.  
Also check the log files for possible errors.

Possible example wool cases:
<table>
<tr><th>brand</th><th>name</th><th>website</th></tr>
<tr><td>DMC</td><td>Natura XL</td><td>www.wollplatz.de</td></tr>
<tr><td>Drops</td><td>Safran</td><td>www.wollplatz.de</td></tr>
<tr><td>Drops</td><td>Baby Merino Mix</td><td>www.wollplatz.de</td></tr>
<tr><td>Hahn</td><td>Alpacca Speciale</td><td>www.wollplatz.de</td></tr>
<tr><td>Stylecraft</td><td>Special DK</td><td>www.wollplatz.de</td></tr>
</table>


What could be improved upon?
===

1. The website scraped here gives out the information without the need to run any JavaScript. This allows to use aiohttp for the requests.  
If the site was a more dynamic JS frontend framework based application, scraping it via i. e. Selenium would probably be necessary.  
Thus getting the asynchronous scraping to work with Selenium would be really nice.
2. The crawler is running in async, but on one core only at the moment. Combining the whole process with multiprocessing would be the next natural step.
